//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include <QtWidgets/QWidget>

class Piano : public QWidget {
    Q_OBJECT;

public:
    explicit Piano(unsigned octs, QWidget *parent = nullptr);

    QSize sizeHint() const override;
    void paintEvent(QPaintEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

signals:
    void onKeyPressed(unsigned key);
    void onKeyReleased(unsigned key);

private:
    unsigned octs_{};
    QVector<unsigned> keyval_;
    unsigned mousePressedKey_ = -1;
    QRectF keyRect(unsigned key) const;
    unsigned keyAtPos(QPointF pos) const;
};
