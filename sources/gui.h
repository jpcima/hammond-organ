//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "instr/synth.h"
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QSlider>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QToolTip>
#include <QtWidgets/QLabel>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtCore/QTimer>
#include <QtCore/QDebug>

void update_all_controls();
QWidget *create_window_contents();
QWidget *create_harmonic_editor();
QWidget *create_keyclick_editor();
QWidget *create_tone_editor();
QWidget *create_vibrato_editor();
QWidget *create_saturation_editor();
QWidget *create_rotary_editor();
QWidget *create_reverb_editor();
QWidget *create_output_editor();
QSlider *create_slider_int32(ParameterId pid);
QSlider *create_slider_float32(ParameterId pid);
QCheckBox *create_checkbox_int32(ParameterId pid);
QComboBox *create_combobox_int32(ParameterId pid);
QWidget *create_piano();

void install_tooltip(QWidget *ctl, ParameterId pid);

struct ControlDesc {
    QWidget *widget;
    ParameterId pid;
    void (*update)(QWidget *, ParameterId pid, const void *);
};
