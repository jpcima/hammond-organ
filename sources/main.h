//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include <cstdint>

extern int arg_resampler_quality;

void interrupt_signal_handler(int sig);

void setup_audio();
void shutdown_audio();

void setup_midi();
void shutdown_midi();

void audio_out_init_callback(double sample_rate, unsigned buffer_size);
void audio_out_process_callback(float **output, unsigned frame_count);
void midi_in_callback(double time, const uint8_t *msg, uint32_t msglen);

#ifdef GRAPHICAL_USER_INTERFACE
struct OrganSynth;
void uientry(OrganSynth *synth);
#endif
