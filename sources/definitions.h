//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once

static constexpr unsigned num_output_channels = 2;
static constexpr unsigned instrument_polyphony = 24;
static constexpr unsigned midi_channel_count = 16;

static constexpr unsigned instrument_note_min = 12;
static constexpr unsigned instrument_note_max = 108;
