//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "main.h"
#include "definitions.h"
#include "utility/logging.h"
#include "utility/scope_guard.h"
#include <jack/jack.h>
#include <memory>
#include <algorithm>
#include <stdexcept>
#include <sys/mman.h>

#undef LOG_TAG
#define LOG_TAG "Main::Jack"

namespace {

jack_client_t *client{};
std::unique_ptr<jack_port_t *[]> ports;

}  // namespace

int process_callback(unsigned nframes, void *);

void setup_audio()
{
    bool success = false;

    LOGI("Locking memory");
    if (mlockall(MCL_CURRENT | MCL_FUTURE) != 0)
        LOGW("Memory locking failed");

    scope(exit)
    {
        if (!success)
            shutdown_audio();
    };

    LOGI("Setting up audio output");
    client = jack_client_open(PROJECT_DISPLAY_NAME, JackNoStartServer, nullptr);
    if (!client)
        throw std::runtime_error("jack_client_open");

    ports.reset(new jack_port_t *[num_output_channels]);
    for (unsigned i = 0; i < num_output_channels; ++i) {
        char name[32];
        sprintf(name, "channel %u", i + 1);
        const char *type = JACK_DEFAULT_AUDIO_TYPE;
        int flags = JackPortIsOutput | JackPortIsTerminal;
        jack_port_t *port = jack_port_register(client, name, type, flags, 0);
        if (!port)
            throw std::runtime_error("jack_port_register");
        ports[i] = port;
    }

    jack_set_process_callback(client, &process_callback, nullptr);

    unsigned sample_rate = jack_get_sample_rate(client);
    unsigned buffer_size = jack_get_buffer_size(client);

    LOGI("Audio sample rate: %u", sample_rate);
    LOGI("Audio buffer size: %u", buffer_size);

    // now buffer size and sample rate are both set
    audio_out_init_callback(sample_rate, buffer_size);

    if (jack_activate(client) != 0)
        throw std::runtime_error("jack_activate");

    success = true;
}

void shutdown_audio()
{
    if (client) {
        jack_client_close(client);
        client = nullptr;
    }
}

int process_callback(unsigned frame_count, void *)
{
    float *outputs[num_output_channels];
    for (unsigned i = 0; i < num_output_channels; ++i)
        outputs[i] = (float *)jack_port_get_buffer(::ports[i], frame_count);

    audio_out_process_callback(outputs, frame_count);
    return 0;
}
