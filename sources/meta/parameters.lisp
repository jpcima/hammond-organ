;; This file defines the list of parameters of the system
;;
;; Each tuple accepts the following keyword parameters
;;  TYPE: the data type
;;  DEFAULT: the initial value
;;  VALUES: a range in the structured form (:range min max &key logarithmic)

(:magic "TONEWHEEL-ORGAN")
(:version 0)

(|name| :type string :default "Default")

(|h0| :type int :default 65535 :values (:range 0 65535)
      :description "16'")
(|h1| :type int :default 65535 :values (:range 0 65535)
      :description "5 1/3'")
(|h2| :type int :default 65535 :values (:range 0 65535)
      :description "8'")
(|h3| :type int :default 0 :values (:range 0 65535)
      :description "4'")
(|h4| :type int :default 0 :values (:range 0 65535)
      :description "2 2/3'")
(|h5| :type int :default 0 :values (:range 0 65535)
      :description "2'")
(|h6| :type int :default 0 :values (:range 0 65535)
      :description "1 3/5'")
(|h7| :type int :default 0 :values (:range 0 65535)
      :description "1 1/3'")
(|h8| :type int :default 0 :values (:range 0 65535)
      :description "1'")

(|tone_waveset| :type int :default 0 :values (:choices "Hammond" "Sawtooth" "Triangle" "Square")
      :description "Tone Waveform")

;; for CAPS saturator
#|
(|sat_enable| :type int :default 1 :values (:range 0 1)
      :description "Enable")
(|sat_mode| :type int :default 1 :values (:range 1 11)
      :description "Mode")
(|sat_gain| :type float :default 0 :values (:range -24 72)
      :description "Gain")
(|sat_bias| :type float :default 0 :values (:range 0 1)
      :description "Bias")
|#

;; for Guitarix Tubescreamer
(|sat_enable| :type int :default 1 :values (:range 0 1)
      :description "Enable")
(|sat_drive| :type float :default 0.5 :values (:range 0.0 1.0)
      :description "Drive")
(|sat_level| :type float :default -16.0 :values (:range -20.0 4.0)
      :description "Level")
(|sat_tone| :type float :default 400.0 :values (:range 100.0 1000.0)
      :description "Tone")

;; for CAPS reverb
(|rev_enable| :type int :default 1 :values (:range 0 1)
      :description "Enable")
(|rev_bandwidth| :type float :default 0.75 :values (:range 0 1)
      :description "Bandwidth")
(|rev_tail| :type float :default 0.5 :values (:range 0 1)
      :description "Tail")
(|rev_damping| :type float :default 0.25 :values (:range 0 1)
      :description "Damping")
(|rev_blend| :type float :default 0.25 :values (:range 0 1)
      :description "Blend")

;; vibrato
(|vib_enable| :type int :default 1 :values (:range 0 1)
      :description "Enable")
(|vib_modrate| :type float :default 7 :values (:range 1 10)
      :description "Modulation speed")
(|vib_depth| :type float :default 0.5 :values (:range 0 1)
      :description "Modulation depth")

;; key click
(|kc_depth| :type int :default 65535 :values (:range 0 65535)
      :description "Volume")
(|kc_harmonic| :type int :default 1 :values (:choices "4th" "5th")
      :description "Harmonic")

;; rotary speaker
(|rot_enable| :type int :default 0 :values (:range 0 1)
      :description "Enable")
(|rot_modfreq| :type float :default 3 :values (:range 1 7)
      :description "Modulation speed")

;; global
(|g_volume| :type float :default 0 :values (:range -20 20)
      :description "Master gain")
