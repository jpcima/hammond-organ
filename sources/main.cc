//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "main.h"
#include "definitions.h"
#include "instr/synth.h"
#include "utility/logging.h"
#include "utility/scope_guard.h"
#include <boost/lexical_cast/try_lexical_convert.hpp>
#include <getopt.h>
#include <memory>
#include <cstdlib>
#include <cstring>
#include <csignal>
#if defined(_WIN32)
#include <windows.h>
static void pause()
{
    Sleep(INFINITE);
}
#endif

#undef LOG_TAG
#define LOG_TAG "Main"

static OrganSynth *synth{};

int arg_resampler_quality = 6;

int main(int argc, char *argv[])
{
    LOGI("Program starting");

    bool valid_args = true;
    for (int c; valid_args && (c = getopt(argc, argv, "r:")) != -1;) {
        switch (c) {
        case 'r':
            if (!boost::conversion::try_lexical_convert(optarg, arg_resampler_quality) ||
                !(arg_resampler_quality >= 0 && arg_resampler_quality <= 10))
                valid_args = false;
            break;
        }
    }

    valid_args = valid_args && argc == optind;
    if (!valid_args) {
        return 1;
    }

#ifdef __unix__
    sigset_t int_sigset;
    const int int_sigs[] = {SIGINT, SIGTERM};
    sigemptyset(&int_sigset);
    for (int sig : int_sigs)
        sigaddset(&int_sigset, sig);
    pthread_sigmask(SIG_BLOCK, &int_sigset, nullptr);

    struct sigaction int_sighandler;
    std::memset(&int_sighandler, 0, sizeof(int_sighandler));
    int_sighandler.sa_handler = interrupt_signal_handler;
    int_sighandler.sa_mask = int_sigset;
    for (int sig : int_sigs)
        sigaction(sig, &int_sighandler, nullptr);
#endif  // __unix__

    /// Audio

    setup_audio();
    scope(exit)
    {
        shutdown_audio();
    };

    /// MIDI

    setup_midi();
    scope(exit)
    {
        shutdown_midi();
    };

        /// UI Loop

#ifdef __unix__
    pthread_sigmask(SIG_UNBLOCK, &int_sigset, nullptr);
#endif

    LOGI("Interface starting");
#if GRAPHICAL_USER_INTERFACE
    uientry(synth);
#else
    for (;;)
        pause();
#endif

#ifdef __unix__
    pthread_sigmask(SIG_BLOCK, &int_sigset, nullptr);
#endif

    ///

    LOGI("Cleaning up");

    return 0;
}

void interrupt_signal_handler(int sig)
{
    LOGI("Interrupted by signal %d", sig);
    shutdown_midi();
    shutdown_audio();
    std::_Exit(1);
}

void audio_out_init_callback(double sample_rate, unsigned buffer_size)
{
    OrganSynth *synth = ::synth = new OrganSynth;
    synth->init(sample_rate, buffer_size);
}

void audio_out_process_callback(float **output, unsigned frame_count)
{
    OrganSynth &synth = *::synth;
    synth.synthesize(output, frame_count);
}

void midi_in_callback(double time, const uint8_t *msg, uint32_t msglen)
{
    OrganSynth &synth = *::synth;
    synth.putmsg(MessageTag::Midi, msg, msglen);
}
