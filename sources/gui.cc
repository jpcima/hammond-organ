//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "gui.h"
#include "gui_piano.h"
#include <cstring>

static OrganSynth *synth;
static QVector<ControlDesc> controldescriptors;

static constexpr int ctl_reso = 1000;

static unsigned midi_channel = 0;
static unsigned kb_first_oct = 2;
static unsigned kb_num_oct = 6;
static unsigned kb_vel = 100;

struct DrawbarPreset {
    DrawbarPreset(const char *name, const char *value)
        : name(name)
        , value(value)
    {
    }
    const char *name = nullptr;
    const char *value = nullptr;
};

static const DrawbarPreset drawbar_preset[] = {
    {"Gospel", "88 8000 008"},
    {"Blues", "88 8800 000"},
    {"Blues", "88 5324 588"},
    {"Rod Argent (Argent)", "88 0000 000"},
    {"Brian Auger", "88 8110 000"},
    {"Tom Coster (Santana)", "88 8800 000"},
    {"Jesse Crawford Setting", "80 0800 000"},
    {"Keith Emerson (ELP)", "88 8000 000"},
    {"Joey DeFrancesco", "88 8400 080"},
    {"Joey DeFrancesco", "83 8000 000"},
    {"BookerT Jones", "88 8630 000"},
    {"Green Onions", "88 8800 000"},
    {"Green Onions", "80 8800 008"},
    {"JonLord", "88 8000 000"},
    {"Matthew Fisher (Procol Harum)", "80 0808 000"},
    {"A Whiter Shade of Pale", "68 8600 000"},
    {"A Whiter Shade of Pale", "00 4370 000"},
    {"Jimmy Smith", "88 8000 000"},
    {"Jimmy McGriff (gospel)", "86 8600 006"},
    {"Steve Winwood", "84 8848 448"},
    {"Steve Winwood", "88 8888 888"},
    {"Errol Garner Style", "80 0008 888"},
    {"Earl Grant", "88 8888 888"},
    {"Earl Grant", "88 8877 666"},
    {"Lenny Dee", "80 8080 808"},
    {"Lenny Dee", "88 8000 808"},
    {"Lenny Dee", "08 8000 888"},
    {"Lenny Dee", "00 8875 400"},
    {"Lenny Dee", "08 8800 808"},
    {"Groove Holmes", "88 8804 664"},
    {"Groove Holmes", "88 8000 008"},
    {"Ethel Smith", "80 8808 008"},
    {"Ethel Smith", "88 8800 008"},
    {"Ethel Smith", "80 8808 000"},
    {"Ken Griffin", "85 8855 557"},
    {"Ken Griffin", "80 8808 008"},
    {"Walter Wanderley", "00 8800 006"},
    {"Walter Wanderley", "00 8008 000"},
    {"Tony Banks (Genesis)", "33 6866 330"},
};

void uientry(OrganSynth *synth)
{
    ::synth = synth;

    int argc = 1;
    char argv0[] = PROJECT_DISPLAY_NAME;
    char *argv[] = {argv0, nullptr};

    QApplication app(argc, argv);
    QMainWindow *window = new QMainWindow;
    window->setCentralWidget(create_window_contents());

    QTimer *timer = new QTimer;
    QObject::connect(timer, &QTimer::timeout, timer,
                     []() { update_all_controls(); });
    timer->start(5000);

    window->show();
    app.exec();
}

void update_all_controls()
{
    unsigned nctl = controldescriptors.size();
    for (unsigned i = 0; i < nctl; ++i) {
        const ControlDesc &desc = controldescriptors[i];
        VoicePreset &vcp = synth->active_preset_;

        const void *value = vcp.ptr(desc.pid);
        desc.widget->blockSignals(true);
        desc.update(desc.widget, desc.pid, value);
        desc.widget->blockSignals(false);
    }
}

QWidget *create_window_contents()
{
    QWidget *contents = new QWidget;
    QVBoxLayout *vbox = new QVBoxLayout;
    QHBoxLayout *hbox = new QHBoxLayout;
    hbox->addWidget(create_harmonic_editor());
    hbox->addWidget(create_keyclick_editor());
    hbox->addWidget(create_tone_editor());
    hbox->addWidget(create_vibrato_editor());
    hbox->addWidget(create_saturation_editor());
    hbox->addWidget(create_rotary_editor());
    hbox->addWidget(create_reverb_editor());
    hbox->addWidget(create_output_editor());
    vbox->addLayout(hbox);
    hbox = new QHBoxLayout;

    QWidget *piano = create_piano();
    piano->setFocusPolicy(Qt::StrongFocus);
    QTimer::singleShot(0, [piano]() { piano->setFocus(); });
    hbox->addWidget(piano);

    vbox->addLayout(hbox);
    contents->setLayout(vbox);

    return contents;
}

QWidget *create_harmonic_editor()
{
    QGroupBox *frame = new QGroupBox;
    frame->setTitle("Harmonics");
    QVBoxLayout *vbox = new QVBoxLayout;
    QHBoxLayout *hbox = new QHBoxLayout;

    QSlider *harmonic_slider[9];
    for (int i = 0; i < 9; ++i) {
        int j = (i == 1) ? 2 : (i == 2) ? 1 : i;
        // hammond inverts drawbar 2 and 3
        QSlider *slider = create_slider_int32(ParameterId(P_h0 + j));
        hbox->addWidget(slider);
        slider->setInvertedAppearance(true);
        slider->setInvertedControls(true);
        harmonic_slider[i] = slider;
    }
    vbox->addLayout(hbox);

    QComboBox *cb = new QComboBox;
    cb->addItem("Drawbar Preset...");
    cb->insertSeparator(1);
    for (const DrawbarPreset &p : drawbar_preset) {
        QString label = QString(p.value) + " - " + QString(p.name);
        cb->addItem(label);
    }

    QObject::connect(cb, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
                     cb, [harmonic_slider](int index) {
                         if (index - 2 >= 0) {
                             const DrawbarPreset &p = drawbar_preset[index - 2];
                             QString db = p.value;
                             db.remove(' ');
                             db.resize(9);
                             int32_t def, min, max;
                             getrange_i32(P_h0, &def, &min, &max);
                             for (unsigned i = 0; i < 9; ++i) {
                                 int value = db[i].toLatin1() - '0';
                                 assert(value >= 0 && value <= 8);
                                 value = value * (max - min) / 8 + min;
                                 harmonic_slider[i]->setValue(value);
                             }
                         }
                     });


    vbox->addWidget(cb);

    frame->setLayout(vbox);
    return frame;
}

QWidget *create_keyclick_editor()
{
    QGroupBox *frame = new QGroupBox;
    frame->setTitle("Key click");
    QHBoxLayout *hbox = new QHBoxLayout;
    QSlider *val_depth = create_slider_int32(P_kc_depth);
    hbox->addWidget(val_depth);
    QComboBox *val_harmonic = create_combobox_int32(P_kc_harmonic);
    hbox->addWidget(val_harmonic);
    frame->setLayout(hbox);
    return frame;
}

QWidget *create_tone_editor()
{
    QGroupBox *frame = new QGroupBox;
    frame->setTitle("Tone");
    QHBoxLayout *hbox = new QHBoxLayout;
    QComboBox *val_waveset = create_combobox_int32(P_tone_waveset);
    hbox->addWidget(val_waveset);
    frame->setLayout(hbox);
    return frame;
}

QWidget *create_vibrato_editor()
{
    QGroupBox *frame = new QGroupBox;
    frame->setTitle("Vibrato");
    QHBoxLayout *hbox = new QHBoxLayout;
    QCheckBox *val_enable = create_checkbox_int32(P_vib_enable);
    hbox->addWidget(val_enable);
    QSlider *val_modrate = create_slider_float32(P_vib_modrate);
    hbox->addWidget(val_modrate);
    QSlider *val_depth = create_slider_float32(P_vib_depth);
    hbox->addWidget(val_depth);
    frame->setLayout(hbox);
    return frame;
}


QWidget *create_saturation_editor()
{
    QGroupBox *frame = new QGroupBox;
    frame->setTitle("Saturation");
    QHBoxLayout *hbox = new QHBoxLayout;
    QCheckBox *val_enable = create_checkbox_int32(P_sat_enable);
    hbox->addWidget(val_enable);
    // QComboBox *val_mode = create_combobox_int32(P_sat_mode);
    // hbox->addWidget(val_mode);
    // QSlider *val_gain = create_slider_float32(P_sat_gain);
    // hbox->addWidget(val_gain);
    // QSlider *val_bias = create_slider_float32(P_sat_bias);
    // hbox->addWidget(val_bias);
    QSlider *val_level = create_slider_float32(P_sat_level);
    hbox->addWidget(val_level);
    QSlider *val_drive = create_slider_float32(P_sat_drive);
    hbox->addWidget(val_drive);
    QSlider *val_tone = create_slider_float32(P_sat_tone);
    hbox->addWidget(val_tone);
    frame->setLayout(hbox);
    return frame;
}

QWidget *create_rotary_editor()
{
    QGroupBox *frame = new QGroupBox;
    frame->setTitle("Rotary Speaker");
    QHBoxLayout *hbox = new QHBoxLayout;
    QCheckBox *val_enable = create_checkbox_int32(P_rot_enable);
    hbox->addWidget(val_enable);
    QSlider *val_modfreq = create_slider_float32(P_rot_modfreq);
    hbox->addWidget(val_modfreq);
    frame->setLayout(hbox);
    return frame;
}

QWidget *create_reverb_editor()
{
    QGroupBox *frame = new QGroupBox;
    frame->setTitle("Reverb");
    QHBoxLayout *hbox = new QHBoxLayout;
    QCheckBox *val_enable = create_checkbox_int32(P_rev_enable);
    hbox->addWidget(val_enable);
    QSlider *val_bandwidth = create_slider_float32(P_rev_bandwidth);
    hbox->addWidget(val_bandwidth);
    QSlider *val_tail = create_slider_float32(P_rev_tail);
    hbox->addWidget(val_tail);
    QSlider *val_damping = create_slider_float32(P_rev_damping);
    hbox->addWidget(val_damping);
    QSlider *val_blend = create_slider_float32(P_rev_blend);
    hbox->addWidget(val_blend);
    frame->setLayout(hbox);
    return frame;
}

QWidget *create_output_editor()
{
    QGroupBox *frame = new QGroupBox;
    frame->setTitle("Output");
    QHBoxLayout *hbox = new QHBoxLayout;
    QSlider *val_volume = create_slider_float32(P_g_volume);
    hbox->addWidget(val_volume);
    frame->setLayout(hbox);
    return frame;
}

static void slider_update_int32(QWidget *ctl, ParameterId pid, const void *pdata)
{
    int32_t value;
    std::memcpy(&value, pdata, sizeof(value));
    static_cast<QAbstractSlider *>(ctl)->setValue(value);
}

QSlider *create_slider_int32(ParameterId pid)
{
    int32_t def, min, max;
    getrange_i32(pid, &def, &min, &max);

    QSlider *slider = new QSlider;
    slider->setRange(min, max);
    slider->setValue(def);

    slider->setSingleStep((max - min) / 30);
    slider->setPageStep((max - min) / 10);

    install_tooltip(slider, pid);

    QObject::connect(slider, &QSlider::valueChanged, slider, [pid](int value) {
        LM_SetParameter cmd;
        cmd.id = pid;
        cmd.type = PT_INT;
        int32_t value32 = value;
        std::memcpy(cmd.value, &value32, sizeof(value32));
        synth->putmsg(MessageTag::Local, (uint8_t *)&cmd,
                      sizeof(cmd));  // - sizeof(cmd.value) + sizeof(int32_t)
    });

    ControlDesc desc;
    desc.widget = slider;
    desc.pid = pid;
    desc.update = &slider_update_int32;
    controldescriptors.push_back(desc);

    return slider;
}

static void slider_update_float32(QWidget *ctl, ParameterId pid, const void *pdata)
{
    float def, min, max;
    bool log;
    getrange_f32(pid, &def, &min, &max, &log);
    // TODO log
    float value;
    std::memcpy(&value, pdata, sizeof(value));
    static_cast<QAbstractSlider *>(ctl)->setValue((value - min) / (max - min) * ctl_reso);
}

QSlider *create_slider_float32(ParameterId pid)
{
    float def, min, max;
    bool log;
    getrange_f32(pid, &def, &min, &max, &log);
    // TODO log

    QSlider *slider = new QSlider;
    slider->setRange(0, ctl_reso);
    slider->setValue((def - min) / (max - min) * ctl_reso);

    slider->setSingleStep(ctl_reso / 30);
    slider->setPageStep(ctl_reso / 10);

    install_tooltip(slider, pid);

    QObject::connect(slider, &QSlider::valueChanged, slider, [pid, min, max](int value) {
        LM_SetParameter cmd;
        cmd.id = pid;
        cmd.type = PT_FLOAT;
        float value32 = float(value) / ctl_reso * (max - min) + min;
        std::memcpy(cmd.value, &value32, sizeof(value32));
        synth->putmsg(MessageTag::Local, (uint8_t *)&cmd,
                      sizeof(cmd));  // - sizeof(cmd.value) + sizeof(int32_t)
    });

    ControlDesc desc;
    desc.widget = slider;
    desc.pid = pid;
    desc.update = slider_update_float32;
    controldescriptors.push_back(desc);

    return slider;
}

static void button_update_int32(QWidget *ctl, ParameterId pid, const void *pdata)
{
    int32_t def, min, max;
    getrange_i32(pid, &def, &min, &max);
    int32_t value;
    std::memcpy(&value, pdata, sizeof(value));
    static_cast<QAbstractButton *>(ctl)->setChecked(value == max);
}

QCheckBox *create_checkbox_int32(ParameterId pid)
{
    int32_t def, min, max;
    getrange_i32(pid, &def, &min, &max);

    QCheckBox *checkbox = new QCheckBox;
    checkbox->setChecked(def == max);

    install_tooltip(checkbox, pid);

    QObject::connect(checkbox, &QCheckBox::toggled, checkbox, [pid, min, max](bool checked) {
        LM_SetParameter cmd;
        cmd.id = pid;
        cmd.type = PT_INT;
        int32_t value32 = checked ? max : min;
        std::memcpy(cmd.value, &value32, sizeof(value32));
        synth->putmsg(MessageTag::Local, (uint8_t *)&cmd,
                      sizeof(cmd));  // - sizeof(cmd.value) + sizeof(int32_t)
    });

    ControlDesc desc;
    desc.widget = checkbox;
    desc.pid = pid;
    desc.update = &button_update_int32;
    controldescriptors.push_back(desc);

    return checkbox;
}

static void combobox_update_int32(QWidget *ctl, ParameterId pid, const void *pdata)
{
    int32_t def, min, max;
    getrange_i32(pid, &def, &min, &max);
    int32_t value;
    std::memcpy(&value, pdata, sizeof(value));
    static_cast<QComboBox *>(ctl)->setCurrentIndex(value - min);
}

QComboBox *create_combobox_int32(ParameterId pid)
{
    int32_t def, min, max;
    getrange_i32(pid, &def, &min, &max);

    const char *const *choices = nullptr;
    unsigned nchoices = getchoices_i32(pid, &choices);

    QComboBox *combobox = new QComboBox;
    if (nchoices > 0) {
        for (int ent = min; ent <= max; ++ent)
            combobox->addItem(choices[(uint32_t)(ent - min)], ent);
    }
    else {
        for (int ent = min; ent <= max; ++ent)
            combobox->addItem(QString::number(ent), ent);
    }
    combobox->setCurrentIndex(def - min);

    install_tooltip(combobox, pid);

    QObject::connect(combobox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
                     combobox, [pid, min, max](int index) {
                         LM_SetParameter cmd;
                         cmd.id = pid;
                         cmd.type = PT_INT;
                         int32_t value32 = index + min;
                         std::memcpy(cmd.value, &value32, sizeof(value32));
                         synth->putmsg(MessageTag::Local, (uint8_t *)&cmd,
                                       sizeof(cmd));  // - sizeof(cmd.value) + sizeof(int32_t)
                     });

    ControlDesc desc;
    desc.widget = combobox;
    desc.pid = pid;
    desc.update = &combobox_update_int32;
    controldescriptors.push_back(desc);

    return combobox;
}

QWidget *create_piano()
{
    Piano *piano = new Piano(kb_num_oct);
    QObject::connect(piano, &Piano::onKeyPressed, piano, [](unsigned key) {
        key += kb_first_oct * 12;
        if (key > 127)
            return;
        uint8_t msg[3];
        msg[0] = 0x90 | midi_channel;
        msg[1] = key;
        msg[2] = kb_vel;
        synth->putmsg(MessageTag::Midi, msg, 3);
    });

    QObject::connect(piano, &Piano::onKeyReleased, piano, [](unsigned key) {
        key += kb_first_oct * 12;
        if (key > 127)
            return;
        uint8_t msg[3];
        msg[0] = 0x80 | midi_channel;
        msg[1] = key;
        msg[2] = kb_vel;
        synth->putmsg(MessageTag::Midi, msg, 3);
    });

    return piano;
}

void install_tooltip(QWidget *ctl, ParameterId pid)
{
    class EventFilter : public QObject {
        ParameterId pid_;

    public:
        explicit EventFilter(ParameterId pid, QObject *parent = nullptr)
            : QObject(parent)
            , pid_(pid)
        {
        }
        bool eventFilter(QObject *obj, QEvent *event) override
        {
            if (event->type() == QEvent::Enter) {
                QWidget *w = qobject_cast<QWidget *>(obj);
                QString pdesc = parameter_description[pid_];
                QToolTip::showText(QCursor::pos(), pdesc, w);
            }
            return QObject::eventFilter(obj, event);
        }
    };

    ctl->installEventFilter(new EventFilter(pid, ctl));
}
