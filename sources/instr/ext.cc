//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#ifdef USE_EMBEDDED_CAPS_PLUGINS
#include "../../../thirdparty/caps-0.9.24/basics.h"
#include "../../../thirdparty/caps-0.9.24/Descriptor.h"
#include "../../../thirdparty/caps-0.9.24/Reverb.h"
#include "../../../thirdparty/caps-0.9.24/Saturate.h"
#endif

#include "instr/ext.h"
#include "instr/ext-ladspa.h"
#include "utility/logging.h"
#include <ltdl.h>
#include <stdexcept>

#define LOG_TAG "Ext"

#ifndef USE_EMBEDDED_CAPS_PLUGINS
static std::unique_ptr<ladspa_plugin_handle> plugin_caps;
#endif
const LADSPA_Descriptor *ext_caps_plate;
const LADSPA_Descriptor *ext_caps_platex2;
#ifndef EXPERIMENT_SATURATOR
const LADSPA_Descriptor *ext_caps_saturate;
#endif

void ext_init()
{
#ifdef USE_EMBEDDED_CAPS_PLUGINS
    static Descriptor<Plate> dc_plate(1779);
    static Descriptor<PlateX2> dc_platex2(1795);
    static Descriptor<Saturate> dc_saturate(1771);
    ext_caps_plate = &dc_plate;
    ext_caps_platex2 = &dc_platex2;
#ifndef EXPERIMENT_SATURATOR
    ext_caps_saturate = &dc_saturate;
#endif
#else
    plugin_caps.reset(new ladspa_plugin_handle("caps"));
    LOGI("using LADSPA plugin '%s'", plugin_caps->library_file_name().c_str());

    ext_caps_plate = plugin_caps->descriptor_by_label("Plate");
    ext_caps_platex2 = plugin_caps->descriptor_by_label("PlateX2");
#ifndef EXPERIMENT_SATURATOR
    ext_caps_saturate = plugin_caps->descriptor_by_label("Saturate");
#endif
#endif
}

///
static struct ltdl_initializer_object {
    ltdl_initializer_object();
    ~ltdl_initializer_object();
} ltdl_initializer;

///
ltdl_initializer_object::ltdl_initializer_object()
{
    if (lt_dlinit() != 0)
        throw std::runtime_error("lt_dlinit");
}

ltdl_initializer_object::~ltdl_initializer_object()
{
    lt_dlexit();
}
