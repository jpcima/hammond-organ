//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include <memory>
#include <cstdint>

class OrganSynth;

///
class OrganVoice {
public:
    void init(double sample_rate, unsigned buffer_size);

    void prepare();
    void trigger(unsigned vel);
    void release(unsigned vel);
    bool finished();

    void compute(unsigned frame_count);

    OrganSynth *synth_{};
    unsigned id_{};
    unsigned note_{};

    bool released_{};
    uint32_t attackelapsed_{};
    uint32_t releaseleft_{};
    uint32_t keyclickleft_{};

    std::unique_ptr<uint32_t[]> env_;

    bool keyclick_{};
    std::unique_ptr<uint32_t[]> keyclick_env_;
    uint32_t keyclick_harmonic_number_{};

    void compute_envelope(unsigned frame_count);
    void compute_keyclick_envelope(unsigned frame_count);
    void contribute_osc_gain(unsigned frame_count);
};
