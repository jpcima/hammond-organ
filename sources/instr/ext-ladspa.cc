//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "ext-ladspa.h"
#include "utility/logging.h"
#include <ltdl.h>
#include <boost/utility/string_ref.hpp>
#ifdef WIN32
#include <windows.h>
#include <libgen.h>
#endif
#include <cassert>

#define LOG_TAG "Ext::Ladspa"

static bool path_is_absolute(boost::string_ref path);
static std::list<std::string> get_ladspa_load_paths();
///

ladspa_plugin_handle::ladspa_plugin_handle(const std::string &name)
    : name_(name)
    , library_(nullptr, &lt_dlclose)
{
    load_library();
    load_functions();
    count_descriptors();
}

ladspa_plugin_handle::~ladspa_plugin_handle()
{
}

const std::string &ladspa_plugin_handle::library_file_name() const
{
    return libpath_;
}

size_t ladspa_plugin_handle::descriptor_count() const
{
    return count_;
}

const LADSPA_Descriptor *ladspa_plugin_handle::descriptor(size_t index) const
{
    if (index >= count_)
        throw std::out_of_range("ladspa_plugin_handle::descriptor");
    return descfn_(index);
}

const LADSPA_Descriptor *ladspa_plugin_handle::descriptor_by_label(const char *label) const
{
    for (size_t i = 0; i < count_; ++i) {
        const LADSPA_Descriptor *desc = descfn_(i);
        if (!strcmp(desc->Label, label))
            return desc;
    }
    throw std::out_of_range("ladspa_plugin_handle::descriptor_by_label");
}

void ladspa_plugin_handle::load_library()
{
    std::string libpath;
    if (name_[0] == '/') {
        libpath = name_;
        library_.reset(lt_dlopenext(libpath.c_str()));
    }
    else {
        for (const std::string &path : get_ladspa_load_paths()) {
            libpath = path + '/' + name_;
            library_.reset(lt_dlopenext(libpath.c_str()));
            if (library_ && !lt_dlsym(library_.get(), "ladspa_descriptor"))
                library_.reset();
            if (library_)
                break;
        }
    }
    if (!library_) {
        LOGE("cannot load LADSPA library '%s'", name_.c_str());
        throw std::runtime_error("ladspa_plugin_handle::ladspa_plugin_handle");
    }
    libpath_ = std::move(libpath);
}

void ladspa_plugin_handle::load_functions()
{
    descfn_ = (LADSPA_Descriptor_Function)lt_dlsym(library_.get(),
                                                   "ladspa_descriptor");
    if (!descfn_)
        throw std::runtime_error("ladspa_plugin_handle::load_functions");
}

void ladspa_plugin_handle::count_descriptors()
{
    size_t count = 0;
    while (descfn_(count++))
        ;
    count_ = count;
}

bool path_is_absolute(boost::string_ref path)
{
#ifdef WIN32
    return path.size() >= 2 && path[1] == ':' &&
           ((path[0] >= 'a' && path[0] <= 'z') || (path[0] >= 'A' && path[0] <= 'Z'));
#else
    return !path.empty() && path.front() == '/';
#endif
}

std::list<std::string> get_ladspa_load_paths()
{
    std::list<std::string> result;

    boost::string_ref paths;
    if (const char *env = std::getenv("LADSPA_PATH")) {
        paths = env;
    }
    else {
#ifdef __unix__
        result.push_back("/usr/local/lib/ladspa");
        result.push_back("/usr/lib/ladspa");
#endif  // __unix__
    }

#ifdef WIN32
    // also search the program directory
    HMODULE hmodule = GetModuleHandle(nullptr);
    std::unique_ptr<char[]> hpath(new char[PATH_MAX]());
    DWORD dwret = GetModuleFileNameA(hmodule, hpath.get(), PATH_MAX);
    if (dwret == 0 || dwret == PATH_MAX)
        throw std::runtime_error("cannot get the path of the current module");
    result.push_back(dirname(hpath.get()));
#endif

#ifdef WIN32
    const char separator = ';';
#else
    const char separator = ':';
#endif

    while (!paths.empty()) {
        size_t seppos = paths.find(separator);
        boost::string_ref path = paths.substr(0, seppos);
        if (path_is_absolute(path))
            result.emplace_back(path);
        paths = (seppos != paths.npos) ? paths.substr(seppos + 1) : boost::string_ref();
    }
    return result;
}

///
ladspa_plugin_instance::~ladspa_plugin_instance()
{
    cleanup();
}

void ladspa_plugin_instance::instantiate(const LADSPA_Descriptor *desc, unsigned sample_rate)
{
    cleanup();
    desc_ = desc;
    assert(desc);
    handle_ = desc->instantiate(desc, sample_rate);
    if (!handle_) {
        LOGE("cannot instanciate LADSPA object '%s'", desc->Label);
        throw std::runtime_error(
            "ladspa_plugin_instance::ladspa_plugin_instance");
    }
}

void ladspa_plugin_instance::cleanup()
{
    if (handle_)
        desc_->cleanup(handle_);
}

void ladspa_plugin_instance::activate()
{
    if (desc_->activate)
        desc_->activate(handle_);
}

void ladspa_plugin_instance::deactivate()
{
    if (desc_->deactivate)
        desc_->deactivate(handle_);
}

void ladspa_plugin_instance::connect_port(size_t port, LADSPA_Data *location)
{
    desc_->connect_port(handle_, port, location);
}

void ladspa_plugin_instance::run(size_t frame_count)
{
    desc_->run(handle_, frame_count);
}

bool ladspa_plugin_instance::supports_run_adding() const
{
    return desc_->run_adding;
}

void ladspa_plugin_instance::run_adding(size_t frame_count)
{
    desc_->run_adding(handle_, frame_count);
}

void ladspa_plugin_instance::set_run_adding_gain(LADSPA_Data gain)
{
    desc_->set_run_adding_gain(handle_, gain);
}
