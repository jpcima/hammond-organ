//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "instr/parameters.h"
#include <cstdint>

enum class MessageTag : uint8_t {
    Midi,
    Local,
};

struct BasicMessage {
    uint32_t tag{};
};

struct LM_SelMidiRecvChn : BasicMessage {
    LM_SelMidiRecvChn()
    {
        tag = tag_for_type;
    }
    static constexpr uint32_t tag_for_type = 'MRCh';
    int channel{};
};

struct LM_SetParameter : BasicMessage {
    LM_SetParameter()
    {
        tag = tag_for_type;
    }
    static constexpr uint32_t tag_for_type = 'SetP';
    ParameterId id{};
    ParameterType type{};
    char value[::parameter_type_max_sizeof]{};
};
