//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "definitions.h"
class OrganSynth;

typedef unsigned VoiceNumber;

class VoiceManager {
public:
    static constexpr VoiceNumber NoVoice = instrument_polyphony;

    explicit VoiceManager(OrganSynth &synth);

    bool active(VoiceNumber id) const;
    inline unsigned active_voice_count() const
    {
        return active_count_;
    }

    VoiceNumber trigger(unsigned note, unsigned vel);
    VoiceNumber release_note(unsigned note, unsigned vel);
    VoiceNumber release_voice(VoiceNumber optid, unsigned vel);
    VoiceNumber kill_note(unsigned note);
    VoiceNumber kill_voice(VoiceNumber optid);
    VoiceNumber find_note(unsigned note);
    VoiceNumber find_free();
    void shutdown_some();

private:
    OrganSynth *synth_{};
    bool active_[instrument_polyphony]{};
    unsigned active_count_ = 0;
};
