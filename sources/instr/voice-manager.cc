//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "instr/voice-manager.h"
#include "instr/voice.h"
#include "instr/synth.h"
#include "utility/logging.h"
#include <cassert>

#undef LOG_TAG
#define LOG_TAG "Poly"

static constexpr bool poly_debug = false;

VoiceManager::VoiceManager(OrganSynth &synth)
{
    synth_ = &synth;
}

VoiceNumber VoiceManager::trigger(unsigned note, unsigned vel)
{
    if (note < instrument_note_min || note > instrument_note_max)
        return NoVoice;

    VoiceNumber optid = this->find_note(note);
    bool retrig = optid != NoVoice;

    if (!retrig)
        optid = this->find_free();

    OrganSynth &synth = *synth_;
    if (optid != NoVoice) {
        OrganVoice &vc = synth.voice_[optid];
        if (!retrig) {
            active_[optid] = true;
            ++active_count_;
            vc.id_ = optid;
            vc.synth_ = &synth;
            vc.note_ = note;
            vc.prepare();
        }
        vc.trigger(vel);
        if (poly_debug)
            LOGD("%s voice: %u", retrig ? "retriggered" : "allocated", optid);
    }
    else {
        if (poly_debug)
            LOGD("no free voice to allocate");
    }

    return optid;
}

VoiceNumber VoiceManager::release_note(unsigned note, unsigned vel)
{
    VoiceNumber optid = this->find_note(note);
    return this->release_voice(optid, vel);
}

VoiceNumber VoiceManager::release_voice(VoiceNumber optid, unsigned vel)
{
    OrganSynth &synth = *synth_;
    if (optid != NoVoice) {
        OrganVoice &vc = synth.voice_[optid];
        vc.release(vel);
        if (poly_debug)
            LOGD("released voice: %u", optid);
    }
    return optid;
}

VoiceNumber VoiceManager::kill_note(unsigned note)
{
    VoiceNumber optid = this->find_note(note);
    return this->kill_voice(optid);
}

VoiceNumber VoiceManager::kill_voice(VoiceNumber optid)
{
    if (optid != NoVoice) {
        assert(optid < instrument_polyphony);
        active_[optid] = false;
        if (poly_debug)
            LOGD("killed voice: %u", optid);
    }
    return optid;
}

VoiceNumber VoiceManager::find_note(unsigned note)
{
    OrganSynth &synth = *synth_;
    for (VoiceNumber i = 0; i < instrument_polyphony; ++i) {
        const OrganVoice &vc = synth.voice_[i];
        if (active_[i] && vc.note_ == note)
            return i;
    }
    return NoVoice;
}

VoiceNumber VoiceManager::find_free()
{
    for (VoiceNumber i = 0; i < instrument_polyphony; ++i) {
        if (!active_[i])
            return i;
    }
    return NoVoice;
}

void VoiceManager::shutdown_some()
{
    OrganSynth &synth = *synth_;
    for (VoiceNumber i = 0; i < instrument_polyphony; ++i) {
        OrganVoice &vc = synth.voice_[i];
        if (active_[i] && vc.finished()) {
            if (poly_debug)
                LOGD("idle voice shut down: %u", i);
            active_[i] = false;
            --active_count_;
        }
    }
}

bool VoiceManager::active(VoiceNumber id) const
{
    assert(id < instrument_polyphony);
    return active_[id];
}
