//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "instr/organ.h"
#include "definitions.h"
#include "utility/logging.h"
#include "dsp/window.h"
#include "dsp/wave.h"
#include "dsp/keyfreq.h"
#include <climits>
#include <cmath>

#undef LOG_TAG
#define LOG_TAG "Synth::Organ"

int16_t wtable_sin[wtable_length + 1];
int16_t wtable_sinmod1[wtable_length + 1];
int16_t *waveset_saw[8];
int16_t *waveset_triangle[8];
int16_t *waveset_square[8];
uint32_t wtable_step[96];
uint16_t volume_table[volume_table_length];

static void generate_waveset(dsp::WaveId wave, int16_t *waveset[8],
                             double sample_rate, unsigned max_harmonics = UINT_MAX);

void organ_init(double sample_rate)
{
    // sine and bass complex sine
    for (unsigned i = 0; i < wtable_length + 1; ++i) {
        double r = double(i) / wtable_length;
        r = std::fmod(r, 1.0);
        double s = std::sin(2.0 * M_PI * r);
        wtable_sin[i] = s * INT16_MAX;
        double mod1 = s * dsp::window_tukey(i, wtable_length + 1, 0.5);
        wtable_sinmod1[i] = mod1 * INT16_MAX;
    }

    // other waves
    generate_waveset(dsp::WaveId::Saw, waveset_saw, sample_rate);
    generate_waveset(dsp::WaveId::Triangle, waveset_triangle, sample_rate);
    generate_waveset(dsp::WaveId::Square, waveset_square, sample_rate);

    // phase increments
    for (unsigned i = 0; i < 96; ++i) {
        unsigned note = i + instrument_note_min;
        double freq = dsp::key2freq<double>(note);
        double step = freq * wtable_length / sample_rate;
        wtable_step[i] = step * (1 << 16);
    }

    // volume table
    for (unsigned i = 0; i < volume_table_length; ++i) {
        constexpr double dbmin = -20.0;
        constexpr double dbstep = dbmin / volume_table_length;
        double db = 0.0f + dbstep * i;
        double vol = std::pow(10.0, db / 20.0);
        unsigned index = volume_table_length - i - 1;
        volume_table[index] = vol * UINT16_MAX;
    }
    volume_table[0] = 0;
}

void generate_waveset(dsp::WaveId wave, int16_t *waveset[8], double sample_rate, unsigned max_harmonics)
{
    for (unsigned octnum = 0; octnum < 8; ++octnum) {
        int16_t *table = new int16_t[wtable_length + 1];  // XXX never freed
        table[wtable_length] = 0;

        float octmaxfreq = dsp::key2freq<double>(instrument_note_min + octnum * 12 + 11);
        float octmaxratio = sample_rate * 0.5f / octmaxfreq;

        constexpr unsigned fft_size = 64 * 1024;

        std::unique_ptr<float[]> table_float(new float[wtable_length]);
        unsigned h = generate_wavetable(wave, max_harmonics, octmaxratio,
                                        table_float.get(), wtable_length, fft_size);

        bool clipped = false;
        for (unsigned i = 0; i < wtable_length; ++i) {
            int sample = 0.8f * table_float[i] * INT16_MAX;
            int clip = sample;
            clip = std::max(clip, INT16_MIN);
            clip = std::min(clip, INT16_MAX);
            clipped = clipped || clip != sample;
            table[i] = clip;
        }

        if (clipped)
            LOGW("Clipping occured during waveset generation");

        LOGI("Generated IDFT wave %s octave %u harmonics %u",
             dsp::wave_id_name[(unsigned)wave], octnum, h);
        waveset[octnum] = table;
    }
}

uint16_t keyclick_env[keyclick_env_length + 2] = {
    0,     6537,  12081, 16894, 21147, 24956, 28406, 31558, 34460, 37149, 39653,
    41997, 44200, 46278, 48243, 50109, 51884, 53577, 55195, 56744, 58231, 59659,
    61034, 62358, 63637, 64872, 65525, 65502, 65480, 65458, 65436, 65415, 65393,
    65371, 65349, 65328, 65306, 65284, 65263, 65241, 65220, 65198, 65177, 65156,
    65134, 65113, 65092, 65071, 65050, 65029, 65008, 64987, 64966, 64945, 64924,
    64904, 64883, 64862, 64842, 64821, 64801, 64780, 64760, 64739, 64719, 64699,
    64678, 64658, 64638, 64618, 64598, 64578, 64558, 64538, 64518, 64498, 64478,
    64458, 64439, 64419, 64399, 64380, 64360, 64340, 64321, 64301, 64282, 64263,
    64243, 64224, 64205, 64186, 64166, 64147, 64128, 64109, 64090, 64071, 64052,
    64033, 64014, 63995, 63977, 63958, 63939, 63921, 63902, 63883, 63865, 63846,
    63828, 63809, 63791, 63772, 63754, 63736, 63717, 63699, 63681, 63663, 63645,
    63627, 63609, 63591, 63573, 63555, 63537, 63519, 63501, 63483, 63465, 63448,
    63430, 63412, 63395, 63377, 63359, 63342, 63324, 63307, 63290, 63272, 63255,
    63237, 63220, 63203, 63186, 63168, 63151, 63134, 63117, 63100, 63083, 63066,
    63049, 63032, 63015, 62998, 62981, 62964, 62948, 62931, 62914, 62897, 62881,
    62864, 62848, 62831, 62814, 62798, 62781, 62765, 62749, 62732, 62716, 62699,
    62683, 62667, 62651, 62634, 62618, 62602, 62586, 62570, 62554, 62538, 62522,
    62506, 62490, 62474, 62458, 62442, 62426, 62411, 62395, 62379, 62363, 62348,
    62332, 62316, 62301, 62285, 62269, 62254, 62238, 62223, 62208, 62192, 62177,
    62161, 62146, 62131, 62115, 62100, 62085, 62070, 62054, 62039, 62024, 62009,
    61994, 61979, 61964, 61949, 61934, 61919, 61904, 61889, 61874, 61859, 61845,
    61830, 61815, 61800, 61786, 61771, 61756, 61742, 61727, 61712, 61698, 61683,
    61669, 61654, 61640, 61625, 61611, 61596, 61582, 61568, 61553, 61539, 61525,
    61510, 61496, 61482, 61468, 61454, 61439, 61425, 61411, 61397, 61383, 61369,
    61355, 61341, 61327, 61313, 61299, 61285, 61271, 61258, 61244, 61230, 61216,
    61202, 61189, 61175, 61161, 61148, 61134, 61120, 61107, 61093, 61080, 61066,
    61052, 61039, 61025, 61012, 60999, 60985, 60972, 60958, 60945, 60932, 60918,
    60905, 60892, 60879, 60865, 60852, 60839, 60826, 60813, 60799, 60786, 60773,
    60760, 60747, 60734, 60721, 60708, 60695, 60682, 60669, 60656, 60643, 60631,
    60618, 60605, 60592, 60579, 60567, 60554, 60541, 60528, 60516, 60503, 60490,
    60478, 60465, 60452, 60440, 60427, 60415, 60402, 60390, 60377, 60365, 60352,
    60340, 60327, 60315, 60303, 60290, 60278, 60266, 60253, 60241, 60229, 60217,
    60204, 60192, 60180, 60168, 60156, 60143, 60131, 60119, 60107, 60095, 60083,
    60071, 60059, 60047, 60035, 60023, 60011, 59999, 59987, 59975, 59963, 59952,
    59940, 59928, 59916, 59904, 59892, 59881, 59869, 59857, 59846, 59834, 59822,
    59810, 59799, 59787, 59776, 59764, 59752, 59741, 59729, 59718, 59706, 59695,
    59683, 59672, 59660, 59649, 59637, 59626, 59615, 59603, 59592, 59581, 59569,
    59558, 59547, 59535, 59524, 59513, 59502, 59490, 59479, 59468, 59457, 59446,
    59435, 59423, 59412, 59401, 59390, 59379, 59368, 59357, 59346, 59335, 59324,
    59313, 59302, 59291, 59280, 59269, 59258, 59247, 59237, 59226, 59215, 59204,
    59193, 59182, 59172, 59161, 59150, 59139, 59129, 59118, 59107, 59097, 59086,
    59075, 59065, 58630, 56626, 54656, 52722, 50822, 48957, 47127, 45332, 43572,
    41846, 40156, 38500, 36879, 35293, 33742, 32225, 30744, 29297, 27886, 26509,
    25167, 23860, 22587, 21350, 20147, 18980, 17847, 16749, 15685, 14657, 13664,
    12705, 11781, 10893, 10039, 9219,  8435,  7686,  6971,  6291,  5646,  5036,
    4461,  3921,  3416,  2945,  2509,  2108,  1742,  1411,  1115,  854,   627,
    435,   278,   156,   69,    17,    0,     0,     0,
};
