//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "instr/voice.h"
#include "instr/synth.h"
#include "instr/organ.h"

void OrganVoice::init(double sample_rate, unsigned buffer_size)
{
    env_.reset(new uint32_t[buffer_size]());
    keyclick_env_.reset(new uint32_t[buffer_size]());
}

void OrganVoice::prepare()
{
    OrganSynth &synth = *synth_;
    const uint32_t attackframes = synth.attackframes_;

    released_ = true;
    attackelapsed_ = attackframes;
    releaseleft_ = 0;
}

void OrganVoice::trigger(unsigned vel)
{
    OrganSynth &synth = *synth_;
    const VoicePreset &vcp = synth.active_preset_;
    const uint32_t attackframes = synth.attackframes_;
    const unsigned keyclickframes = synth.keyclickframes_;

    if (released_) {
        attackelapsed_ = std::min(releaseleft_, attackframes);
        releaseleft_ = 0;
        released_ = false;
        if (synth.vcm_.active_voice_count() == 1) {
            keyclick_ = true;
            keyclick_harmonic_number_ = harmonic_number[vcp.parameter.kc_harmonic + 3];
            // TODO something about the potential discontinuity here?
            // if (keyclickleft_ == 0)
            keyclickleft_ = keyclickframes;
        }
    }
}

void OrganVoice::release(unsigned vel)
{
    OrganSynth &synth = *synth_;
    const uint32_t attackframes = synth.attackframes_;
    const uint32_t releaseframes = synth.releaseframes_;

    if (!released_) {
        uint32_t attackleft = attackframes - attackelapsed_;
        releaseleft_ = (attackleft < releaseframes) ? (releaseframes - attackleft) : 0u;
        attackelapsed_ = attackframes;
        released_ = true;
    }
}

bool OrganVoice::finished()
{
    return released_ && releaseleft_ == 0;
}

void OrganVoice::compute(unsigned frame_count)
{
    OrganSynth &synth = *synth_;
    const uint32_t attackframes = synth.attackframes_;

    compute_envelope(frame_count);
    compute_keyclick_envelope(frame_count);
    contribute_osc_gain(frame_count);

    if (released_) {
        releaseleft_ -= std::min(releaseleft_, frame_count);
    }
    else {
        attackelapsed_ = std::min(attackelapsed_ + frame_count, attackframes);
    }
    keyclickleft_ -= std::min(keyclickleft_, frame_count);
}

void OrganVoice::compute_envelope(unsigned frame_count)
{
    OrganSynth &synth = *synth_;
    const uint32_t attackframes = synth.attackframes_;
    const uint32_t releaseframes = synth.releaseframes_;

    // envelope
    if (released_) {
        for (unsigned i = 0; i < frame_count; ++i) {
            unsigned n = (i >= releaseleft_) ? 0 : (releaseleft_ - i);
            env_[i] = (n << 16u) / releaseframes;
        }
    }
    else {
        for (unsigned i = 0; i < frame_count; ++i) {
            uint32_t n = std::min(attackframes, attackelapsed_ + i);
            env_[i] = (n << 16u) / attackframes;
        }
    }
}

void OrganVoice::compute_keyclick_envelope(unsigned frame_count)
{
    if (!keyclick_)
        return;

    OrganSynth &synth = *synth_;
    const VoicePreset &vcp = synth.active_preset_;
    uint16_t kc_depth = vcp.parameter.kc_depth;
    uint16_t kc_vol = volume_table[kc_depth >> 6];
    const unsigned keyclickframes = synth.keyclickframes_;

    for (unsigned i = 0; i < frame_count; ++i) {
        uint32_t keyclickleft = (i > keyclickleft_) ? 0 : (keyclickleft_ - i);
        unsigned frac = ((keyclickframes - keyclickleft) << 16) / keyclickframes;
        unsigned fracindex = frac * keyclick_env_length;
        unsigned index = fracindex >> 16;
        unsigned mu = fracindex & ((1 << 16) - 1);
        int y1 = keyclick_env[index];
        int y2 = keyclick_env[index + 1];
        unsigned kc = y1 + (y2 - y1) * int(mu) / (1 << 16);
        kc = (kc * kc_vol) >> 16;
        keyclick_env_[i] = kc;
    }
}

void OrganVoice::contribute_osc_gain(unsigned frame_count)
{
    OrganSynth &synth = *synth_;
    const VoicePreset &vcp = synth.active_preset_;

    auto set_gain = [&](uint32_t oscnum, uint16_t hgain, uint32_t *env) {
        if (oscnum >= 96)
            return;
        uint16_t maxg = 0;
        for (unsigned i = 0; i < frame_count; ++i) {
            uint16_t g = (hgain * env[i]) >> 16;
            unsigned index = oscnum * frame_count + i;
            maxg = synth.oscgain_[index] = std::max(g, synth.oscgain_[index]);
        }
        synth.oscmaxgain_[oscnum] = std::max(synth.oscmaxgain_[oscnum], maxg);
    };

    uint32_t firstoscnum = note_ - instrument_note_min;
    for (unsigned h = 0; h < 9; ++h) {
        uint32_t oscnum = firstoscnum + harmonic_number[h];
        uint16_t hgain = (&vcp.parameter.h0)[h];
        hgain = volume_table[hgain >> 6];
        set_gain(oscnum, hgain, env_.get());
    }

    if (keyclick_) {
        uint32_t oscnum = firstoscnum + keyclick_harmonic_number_;
        set_gain(oscnum, UINT16_MAX, keyclick_env_.get());
    }
}
