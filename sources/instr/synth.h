//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "instr/ext-ladspa.h"
#include "instr/presets.h"
#include "instr/voice-manager.h"
#include "instr/msg.h"
#define VIB_FDELAY_ALLPASS 1
#ifdef VIB_FDELAY_ALLPASS
#include "parts/from-stk/delay-a.h"
#endif
//#define EXPERIMENT_SATURATOR 1
#ifdef EXPERIMENT_SATURATOR
#include "parts/saturator.h"
#else
namespace pluginlib { namespace ts9sim { class Dsp; } }
#endif
#include "parts/rotary-speaker.h"
#include "definitions.h"
#include <ring_buffer.h>
#include <mutex>
#include <memory>
#include <cstdint>

class OrganVoice;

///
class OrganSynth {
public:
    OrganSynth();
    ~OrganSynth();
    void init(double sample_rate, unsigned buffer_size);

    void synthesize(float *outputs[], unsigned frame_count);

    int recv_channel_ = 0;

    bool receiving_on_channel(unsigned ch) const;

    VoiceManager vcm_;
    int pitchbend_ = 0;
    std::unique_ptr<OrganVoice[]> voice_;
    VoicePreset active_preset_;
    VoicePreset reference_preset_;
    unsigned bank_number_{};
    unsigned program_number_{};

    float sample_interval_ = 0;

    uint32_t attackframes_ = 0;
    uint32_t releaseframes_ = 0;
    unsigned keyclickframes_ = 0;
    unsigned vibdelayframes_ = 0;

    uint32_t oscphase_[96]{};
    std::unique_ptr<uint16_t[]> oscgain_;
    uint16_t oscmaxgain_[96]{};
    std::unique_ptr<int[]> oscmix_;

    std::unique_ptr<float[]> temp_;

#ifndef EXPERIMENT_SATURATOR
    //ladspa_plugin_instance saturator_;
    std::unique_ptr<pluginlib::ts9sim::Dsp> saturator_;
#else
    Saturator saturator_;
#endif

#ifndef VIB_FDELAY_ALLPASS
    std::unique_ptr<float[]> vibdelayline_;
    unsigned vibdelaylinesize_{};
    unsigned vibdelayindex_{};
#else
    std::unique_ptr<DelayA> vibdelayline_;
#endif
    float vibmodphase_{};

    RotarySpeaker rotary_;

    ladspa_plugin_instance reverb_;
    ladspa_plugin_instance stereo_reverb_;

    float *apply_vibrato(float *in, unsigned frame_count);
    float *apply_saturation(float *in, unsigned frame_count);
    float *apply_rotary(float *in, unsigned frame_count);
    float *apply_stereo_rotary(float *in, unsigned frame_count);
    float *apply_reverb(float *in, unsigned frame_count);
    float *apply_stereo_reverb(float *in, unsigned frame_count);

    bool putmsg(MessageTag msgtag, const uint8_t *data, uint32_t length);
    bool getmsg(MessageTag *pmsgtag, uint8_t *pdata, uint32_t *plength);

    Ring_Buffer rbuf_{128 * 1024};
    std::mutex rbuf_writers_mutex_;
    static constexpr uint32_t msg_maxlen = 8192;

    void dispatchlocal(const uint8_t *msgdata, uint32_t msglen);
    void dispatchmidi(const uint8_t *msg, uint32_t len);
};
