//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "instr/presets.h"
#include <iostream>
#include <algorithm>
#include <cstring>
#include <cassert>

// generic access

void VoicePreset::get_parameter(ParameterId id, ParameterType pt, void *dst)
{
    assert(pt == ::parameter_type[id]);
    std::memcpy(dst, this->cptr(id), ::parameter_type_sizeof[pt]);
}

void VoicePreset::set_parameter(ParameterId id, ParameterType pt, const void *src)
{
    assert(pt == ::parameter_type[id]);
    void *dst = this->ptr(id);
    if (pt == PT_STRING) {
        const char *srcstr = static_cast<const char *>(src);
        char *dststr = static_cast<char *>(dst);
        size_t srcsize = std::strlen(srcstr);
        unsigned dstsize = ::parameter_type_sizeof[pt];
        unsigned i = 0;
        for (; i < srcsize; ++i)
            dststr[i] = srcstr[i];
        for (; i < dstsize; ++i)
            dststr[i] = 0;
    }
    else {
        std::memcpy(dst, src, ::parameter_type_sizeof[pt]);
    }
}

// getter

int32_t VoicePreset::get_i32(ParameterId id)
{
    assert(::parameter_type[id] == PT_INT);
    return *static_cast<const int32_t *>(this->cptr(id));
}

float VoicePreset::get_f32(ParameterId id)
{
    assert(::parameter_type[id] == PT_FLOAT);
    return *static_cast<const float *>(this->cptr(id));
}

boost::string_ref VoicePreset::get_string(ParameterId id)
{
    const char *ptr = static_cast<const char *>(this->cptr(id));
    ParameterType pt = ::parameter_type[id];
    assert(pt == PT_STRING);
    boost::string_ref sv(ptr, ::parameter_type_sizeof[pt]);
    while (sv.size() && !sv.back())
        sv.remove_suffix(1);
    return sv;
}

// setter

void VoicePreset::set_i32(ParameterId id, int32_t val)
{
    assert(::parameter_type[id] == PT_INT);
    *static_cast<int32_t *>(this->ptr(id)) = val;
}

void VoicePreset::set_f32(ParameterId id, float val)
{
    assert(::parameter_type[id] == PT_FLOAT);
    *static_cast<float *>(this->ptr(id)) = val;
}

void VoicePreset::set_string(ParameterId id, boost::string_ref val)
{
    char *dst = static_cast<char *>(this->ptr(id));
    ParameterType pt = ::parameter_type[id];
    assert(pt == PT_STRING);
    unsigned dstsize = ::parameter_type_sizeof[pt];
    unsigned srcsize = val.size();
    assert(srcsize <= dstsize);
    unsigned i = 0;
    for (; i < srcsize; ++i)
        dst[i] = val[i];
    for (; i < dstsize; ++i)
        dst[i] = 0;
}

/// pointer to parameter data
void *VoicePreset::ptr(ParameterId id)
{
    assert(unsigned(id) < ::parameter_count);
    unsigned offset = ::parameter_offset[id];
    return (char *)&this->parameter + offset;
}

const void *VoicePreset::cptr(ParameterId id) const
{
    return const_cast<VoicePreset *>(this)->ptr(id);
}

// input/output
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
static const bool little_endian_host = true;
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
static const bool little_endian_host = false;
#else
#error unsupported host endianness
#endif

bool VoicePreset::save(std::ostream &out) const
{
    constexpr unsigned magiclen = sizeof(parameter_file_magic);
    const char *magic = parameter_file_magic;
    out.write(magic, magiclen);

    uint32_t versionbuf = parameter_file_version;
    if (little_endian_host)
        std::reverse((char *)&versionbuf, (char *)&versionbuf + sizeof(versionbuf));
    out.write((char *)&versionbuf, sizeof(versionbuf));

    for (unsigned i = 0; i < ::parameter_count; ++i) {
        ParameterId id = ParameterId(i);
        ParameterType type = ::parameter_type[i];

        unsigned size = ::parameter_type_sizeof[type];
        bool byteswap = ::parameter_type_byteswapped[type];

        char valuebuf[::parameter_type_max_sizeof];
        std::memcpy(valuebuf, this->cptr(id), size);

        if (byteswap ^ little_endian_host)
            std::reverse(valuebuf, valuebuf + size);

        out.write(valuebuf, size);
    }

    out.flush();
    return out.good();
}

bool VoicePreset::load(std::istream &in)
{
    constexpr unsigned magiclen = sizeof(parameter_file_magic);
    char magic[magiclen]{};
    in.read(magic, magiclen);
    if (in.fail() || in.gcount() != magiclen ||
        !std::equal(magic, magic + magiclen, parameter_file_magic))
        return false;

    uint32_t version{};
    in.read((char *)&version, sizeof(version));
    if (little_endian_host)
        std::reverse((char *)&version, (char *)&version + sizeof(version));
    if (in.fail() || in.gcount() != sizeof(version) || version != parameter_file_version)
        return false;

    for (unsigned i = 0; i < ::parameter_count; ++i) {
        ParameterId id = ParameterId(i);
        ParameterType type = ::parameter_type[i];

        unsigned size = ::parameter_type_sizeof[type];
        bool byteswap = ::parameter_type_byteswapped[type];

        char *dst = reinterpret_cast<char *>(this->ptr(id));
        in.read(dst, size);

        if (byteswap ^ little_endian_host)
            std::reverse(dst, dst + size);
    }

    return in.good();
}
