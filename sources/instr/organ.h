//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include <cstdint>

static constexpr unsigned harmonic_number[9] = {
    0, 12, 19, 24, 31, 36, 40, 43, 48,
};

static constexpr unsigned wtable_length = 1024;

extern int16_t wtable_sin[wtable_length + 1];
extern int16_t wtable_sinmod1[wtable_length + 1];

extern uint32_t wtable_step[96];

static constexpr int16_t *waveset_standard[8]{
    wtable_sinmod1, wtable_sinmod1, wtable_sin, wtable_sin,
    wtable_sin,     wtable_sin,     wtable_sin, wtable_sin,
};

extern int16_t *waveset_saw[8];
extern int16_t *waveset_triangle[8];
extern int16_t *waveset_square[8];

static constexpr unsigned volume_table_length = 1024;
extern uint16_t volume_table[volume_table_length];

static constexpr unsigned keyclick_env_length = 512;
extern uint16_t keyclick_env[keyclick_env_length + 2];

static constexpr float keyclick_duration = 0.15f;

void organ_init(double sample_rate);
