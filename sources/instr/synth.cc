//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "instr/synth.h"
#include "instr/voice.h"
#include "instr/organ.h"
#include "instr/ext.h"
#include "utility/logging.h"
#include "utility/cpu.h"
#include "definitions.h"
#include "dsp/trig.h"
#include <cmath>
#ifndef EXPERIMENT_SATURATOR
#include "generated/ts9sim.cc"
#endif

#undef LOG_TAG
#define LOG_TAG "Instrument::Synth"

static constexpr float attacktime = 0.005f;
static constexpr float releasetime = 0.05f;
static const float vibdelaytime = 0.001f;

static double gauss(double x, double sigma)
{
    return exp(-x * x / (2 * sigma * sigma)) / sqrt(2 * M_PI * sigma * sigma);
}

///
OrganSynth::OrganSynth()
    : vcm_(*this)
    , voice_(new OrganVoice[instrument_polyphony])
{
}

OrganSynth::~OrganSynth()
{
}

void OrganSynth::init(double sample_rate, unsigned buffer_size)
{
    for (unsigned p = 0; p < instrument_polyphony; ++p)
        voice_[p].init(sample_rate, buffer_size);

    sample_interval_ = 1.0 / sample_rate;

    organ_init(sample_rate);
    ext_init();
    attackframes_ = std::lround(attacktime * sample_rate);
    releaseframes_ = std::lround(releasetime * sample_rate);

    oscgain_.reset(new uint16_t[96 * buffer_size]());
    oscmix_.reset(new int[buffer_size]());

    temp_.reset(new float[2 * buffer_size]);

    keyclickframes_ = std::lround(keyclick_duration * sample_rate);

#ifndef EXPERIMENT_SATURATOR
    // saturator_.instantiate(ext_caps_saturate, sample_rate);
    // saturator_.activate();
    pluginlib::ts9sim::Dsp *saturator = new pluginlib::ts9sim::Dsp;
    saturator_.reset(saturator);
    saturator->set_samplerate(sample_rate, saturator);
#endif

    const unsigned vibdelayframes = std::lround(vibdelaytime * sample_rate);
    vibdelayframes_ = vibdelayframes;

#ifndef VIB_FDELAY_ALLPASS
    vibdelaylinesize_ = nextpow2(vibdelayframes + 1 /* for interpolation */);
    vibdelayline_.reset(new float[vibdelaylinesize_]());
#else
    vibdelayline_.reset(new DelayA(0.5f, vibdelayframes));
#endif

    rotary_.init(sample_rate, buffer_size);

    reverb_.instantiate(ext_caps_plate, sample_rate);
    reverb_.activate();

    stereo_reverb_.instantiate(ext_caps_platex2, sample_rate);
    stereo_reverb_.activate();
}

void OrganSynth::synthesize(float *outputs[], unsigned frame_count)
{
    utility::FPStateRestorer fpsr;
    utility::disable_denormals();

    MessageTag msgtag;
    uint8_t msgdata[msg_maxlen];
    for (uint32_t msglen; getmsg(&msgtag, msgdata, &msglen);) {
        switch (msgtag) {
        case MessageTag::Midi:
            dispatchmidi(msgdata, msglen);
            break;
        case MessageTag::Local:
            dispatchlocal(msgdata, msglen);
            break;
        }
    }

    const VoicePreset &vcp = this->active_preset_;
    VoiceManager &vcm = vcm_;

    std::fill(&oscgain_[0], &oscgain_[96 * frame_count], 0);
    std::fill(&oscmaxgain_[0], &oscmaxgain_[96], 0);

    for (unsigned i = 0; i < instrument_polyphony; ++i) {
        if (vcm.active(i)) {
            OrganVoice &vc = voice_[i];
            vc.compute(frame_count);
        }
    }

    int16_t const *const *wavesets[] = {
        waveset_standard,
        waveset_saw,
        waveset_triangle,
        waveset_square,
    };
    int16_t const *const *waveset = wavesets[vcp.parameter.tone_waveset];

    uint16_t *oscgain = oscgain_.get();
    uint16_t *oscmaxgain = oscmaxgain_;

    std::fill(&oscmix_[0], &oscmix_[frame_count], 0);
    for (unsigned oscnum = 0; oscnum < 96; ++oscnum) {
        uint32_t phase = 0;
        if (oscmaxgain[oscnum] > 0) {
            phase = oscphase_[oscnum];
            uint32_t step = wtable_step[oscnum];
            const int16_t *table = waveset[oscnum / 12];
            for (unsigned i = 0; i < frame_count; ++i) {
                unsigned g = oscgain[oscnum * frame_count + i];
                unsigned index = phase >> 16;
                unsigned mu = phase & ((1 << 16) - 1);
                int y1 = table[index];
                int y2 = table[index + 1];
                int s = y1 + (y2 - y1) * int(mu) / (1 << 16);
                oscmix_[i] += (s * int(g)) / (1 << 16);
                phase = (phase + step) & ((wtable_length << 16) - 1);
            }
        }
        oscphase_[oscnum] = phase;
    }

    float *output_dry = temp_.get();
    std::fill(output_dry, output_dry + frame_count, 0);

    for (unsigned i = 0; i < frame_count; ++i) {
        float out = oscmix_[i] * (1.0f / INT16_MAX);
        const float attenuate = vcp.parameter.sat_enable ? 1.0f : (1.0f / 16.0f);
        output_dry[i] += out * attenuate;
    }

    float *output_vib = apply_vibrato(output_dry, frame_count);
    float *output_sat = apply_saturation(output_vib, frame_count);
    // float *output_rot = apply_rotary(output_sat, frame_count);
    // float *output_rev_l = apply_reverb(output_rot, frame_count);
    float *output_rot = apply_stereo_rotary(output_sat, frame_count);
    float *output_rev_l = apply_stereo_reverb(output_rot, frame_count);
    float *output_rev_r = output_rev_l + frame_count;

    const float g_amp = std::pow(10.0f, vcp.parameter.g_volume / 20.0f);
    for (unsigned i = 0; i < frame_count; ++i) {
        float left = output_rev_l[i] * g_amp;
        float right = output_rev_r[i] * g_amp;
        outputs[0][i] = left;
        outputs[1][i] = right;
    }

    vcm.shutdown_some();
}

float *OrganSynth::apply_vibrato(float *in, unsigned frame_count)
{
    const VoicePreset &vcp = active_preset_;
    if (!vcp.parameter.vib_enable)
        return in;

    float *out = temp_.get();
    const float startphase = vibmodphase_;
    const float vibmodrate = vcp.parameter.vib_modrate * sample_interval_;
    const float phasestep = vibmodrate * float(2.0f * M_PI);
    const float vibdepth = vcp.parameter.vib_depth;
    const unsigned vibdelayframes = vibdelayframes_;

    dsp::FastSine<float> oscilmod(startphase, phasestep);
    for (unsigned i = 0; i < frame_count; ++i) {
        float mod = oscilmod.sin();
        oscilmod.advance();
        mod = (mod + 1.0f) / 2.0f;

#ifndef VIB_FDELAY_ALLPASS
        vibdelayline_[vibdelayindex_] = in[i];
        vibdelayindex_ = (vibdelayindex_ + 1) & (vibdelaylinesize_ - 1);
        //
        float index = vibdelayindex_ + mod * vibdelayframes;
        unsigned indexi = index;
        float mu = index - indexi;
        //
        float y1 = vibdelayline_[(indexi) & (vibdelaylinesize_ - 1)];
        float y2 = vibdelayline_[(indexi + 1) & (vibdelaylinesize_ - 1)];
        out[i] = y1 + mu * (y2 - y1);
#else
        constexpr float vibdelaymin = DelayA::minimum_delay();
        // mod *= 1.0f - (vibdelaymin / vibdelayframes);  //
        float vibdelay = mod * vibdelayframes;
        // vibdelay += vibdelaymin;  //
        vibdelay = (vibdelay < vibdelaymin) ? vibdelaymin : vibdelay;
        vibdelay = (vibdelay > vibdelayframes) ? vibdelayframes : vibdelay;
        vibdelayline_->delay(vibdelay);
        out[i] = vibdepth * (vibdelayline_->tick(in[i])) + (1.0f - vibdepth) * in[i];
#endif
    }

    float phase = startphase + frame_count * phasestep;
    int phasewrap = phase / float(2.0f * M_PI);
    vibmodphase_ = phase - (phasewrap * float(2.0f * M_PI));

    return out;
}

float *OrganSynth::apply_saturation(float *in, unsigned frame_count)
{
    const VoicePreset &vcp = active_preset_;
    if (!vcp.parameter.sat_enable)
        return in;

    float *out = temp_.get();
#ifndef EXPERIMENT_SATURATOR
    // float p_mode = vcp.parameter.sat_mode;
    // float p_gain = vcp.parameter.sat_gain;
    // float p_bias = vcp.parameter.sat_bias;
    // saturator_.connect_port(0, &p_mode);
    // saturator_.connect_port(1, &p_gain);
    // saturator_.connect_port(2, &p_bias);
    // saturator_.connect_port(3, in);
    // saturator_.connect_port(4, out);
    // saturator_.run(frame_count);
    pluginlib::ts9sim::Dsp &saturator = *saturator_;
    saturator.level(vcp.parameter.sat_level);
    saturator.drive(vcp.parameter.sat_drive);
    saturator.tone(vcp.parameter.sat_tone);
    saturator.mono_audio(frame_count, in, out, &saturator);
#else
    const float gain = std::pow(10, vcp.parameter.sat_gain / 20.0f);
    const float bias = vcp.parameter.sat_bias;
    for (unsigned i = 0; i < frame_count; ++i) {
        float ins = in[i];
        float outs = saturator_.tick(gain * (ins + bias));
        // TODO still some hard clipping here! because oversampling filter gain
        out[i] = outs;
    }
#endif
    return out;
}

float *OrganSynth::apply_rotary(float *in, unsigned frame_count)
{
    const VoicePreset &vcp = this->active_preset_;
    if (!vcp.parameter.rot_enable)
        return in;

    float *out = temp_.get();
    rotary_.modulator_frequency(vcp.parameter.rot_modfreq);
    rotary_.tick(in, out, frame_count);
    return out;
}

float *OrganSynth::apply_stereo_rotary(float *in, unsigned frame_count)
{
    float *out_l = temp_.get();
    float *out_r = out_l + frame_count;

    const VoicePreset &vcp = this->active_preset_;
    if (!vcp.parameter.rot_enable) {
        std::copy_n(in, frame_count, out_l);
        std::copy_n(in, frame_count, out_r);
    }
    else {
        rotary_.modulator_frequency(vcp.parameter.rot_modfreq);
        rotary_.tick(in, out_l, out_r, frame_count);
    }
    return out_l;
}

float *OrganSynth::apply_reverb(float *in, unsigned frame_count)
{
    float *out_l = temp_.get();
    float *out_r = out_l + frame_count;

    const VoicePreset &vcp = this->active_preset_;
    if (!vcp.parameter.rev_enable) {
        std::copy_n(in, frame_count, out_l);
        std::copy_n(in, frame_count, out_r);
    }
    else {
        float p_bandwidth = vcp.parameter.rev_bandwidth;
        float p_tail = vcp.parameter.rev_tail;
        float p_damping = vcp.parameter.rev_damping;
        float p_blend = vcp.parameter.rev_blend;
        reverb_.connect_port(0, &p_bandwidth);
        reverb_.connect_port(1, &p_tail);
        reverb_.connect_port(2, &p_damping);
        reverb_.connect_port(3, &p_blend);
        reverb_.connect_port(4, in);
        reverb_.connect_port(5, out_l);
        reverb_.connect_port(6, out_r);
        reverb_.run(frame_count);
    }
    return out_l;
}

float *OrganSynth::apply_stereo_reverb(float *in, unsigned frame_count)
{
    const VoicePreset &vcp = this->active_preset_;
    if (!vcp.parameter.rev_enable)
        return in;

    float *in_l = in;
    float *in_r = in + frame_count;

    float *out_l = temp_.get();
    float *out_r = out_l + frame_count;
    float p_bandwidth = vcp.parameter.rev_bandwidth;
    float p_tail = vcp.parameter.rev_tail;
    float p_damping = vcp.parameter.rev_damping;
    float p_blend = vcp.parameter.rev_blend;
    stereo_reverb_.connect_port(0, &p_bandwidth);
    stereo_reverb_.connect_port(1, &p_tail);
    stereo_reverb_.connect_port(2, &p_damping);
    stereo_reverb_.connect_port(3, &p_blend);
    stereo_reverb_.connect_port(4, in_l);
    stereo_reverb_.connect_port(5, in_r);
    stereo_reverb_.connect_port(6, out_l);
    stereo_reverb_.connect_port(7, out_r);
    stereo_reverb_.run(frame_count);
    return out_l;
}

bool OrganSynth::putmsg(MessageTag msgtag, const uint8_t *data, uint32_t length)
{
    if (length > msg_maxlen)
        return false;

    // in case of multiple writers
    std::lock_guard<std::mutex> writers_lock(rbuf_writers_mutex_);

    Ring_Buffer &rbuf = rbuf_;
    unsigned avail_write = rbuf.size_free();
    if (length + 1 + 4 > avail_write)
        return false;
    bool rbok = rbuf.put((char *)&msgtag, 1) && rbuf.put((char *)&length, 4) &&
                rbuf.put(data, length);
    assert(rbok);
    return true;
}

bool OrganSynth::getmsg(MessageTag *pmsgtag, uint8_t *pdata, uint32_t *plength)
{
    Ring_Buffer &rbuf = rbuf_;
    char msghdr[1 + 4];
    if (!rbuf.peek(msghdr, sizeof(msghdr)))
        return false;
    *pmsgtag = (MessageTag)msghdr[0];
    uint32_t length = *plength = *(uint32_t *)(msghdr + 1);
    if (rbuf.size_used() < 1 + 4 + length)
        return false;
    bool rbok = rbuf.discard(sizeof(msghdr)) && rbuf.get(pdata, length);
    assert(rbok);
    return true;
}

void OrganSynth::dispatchlocal(const uint8_t *msgdata, uint32_t msglen)
{
    BasicMessage *msghdr = (BasicMessage *)msgdata;

    switch (msghdr->tag) {
    case LM_SelMidiRecvChn::tag_for_type: {
        LM_SelMidiRecvChn *msg = static_cast<LM_SelMidiRecvChn *>(msghdr);
        recv_channel_ = msg->channel;
        break;
    }
    case LM_SetParameter::tag_for_type: {
        LM_SetParameter *msg = static_cast<LM_SetParameter *>(msghdr);
        active_preset_.set_parameter(msg->id, msg->type, msg->value);
        break;
    }
    }
}
