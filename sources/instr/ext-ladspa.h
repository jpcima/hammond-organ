//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include <ladspa.h>
#include <memory>
#include <string>
#include <list>

struct lt__handle;
//

class ladspa_plugin_handle {
    typedef std::unique_ptr<lt__handle, int (*)(lt__handle *)> unique_lt_handle;

    std::string name_;
    std::string libpath_;
    unique_lt_handle library_;
    LADSPA_Descriptor_Function descfn_{};
    size_t count_{};

public:
    explicit ladspa_plugin_handle(const std::string &name);
    ~ladspa_plugin_handle();

    const std::string &library_file_name() const;

    size_t descriptor_count() const;
    const LADSPA_Descriptor *descriptor(size_t index) const;
    const LADSPA_Descriptor *descriptor_by_label(const char *label) const;

private:
    void load_library();
    void load_functions();
    void count_descriptors();

    ladspa_plugin_handle(const ladspa_plugin_handle &) = delete;
    ladspa_plugin_handle &operator=(const ladspa_plugin_handle &) = delete;
};

class ladspa_plugin_instance {
public:
    inline ladspa_plugin_instance()
    {
    }
    ~ladspa_plugin_instance();

    void instantiate(const LADSPA_Descriptor *desc, unsigned sample_rate);
    void cleanup();

    void activate();
    void deactivate();

    void connect_port(size_t port, LADSPA_Data *location);

    void run(size_t frame_count);
    bool supports_run_adding() const;
    void run_adding(size_t frame_count);
    void set_run_adding_gain(LADSPA_Data gain);

private:
    LADSPA_Handle handle_{};
    const LADSPA_Descriptor *desc_{};

    ladspa_plugin_instance(const ladspa_plugin_instance &) = delete;
    ladspa_plugin_instance &operator=(const ladspa_plugin_instance &) = delete;
};
