//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "instr/parameters.h"
#include <boost/utility/string_ref.hpp>
#include <iosfwd>
#include <cstdint>

class VoicePreset {
public:
    ParameterBlock parameter = ::pb_default;

    // generic access
    void get_parameter(ParameterId id, ParameterType pt, void *dst);
    void set_parameter(ParameterId id, ParameterType pt, const void *src);

    // getter
    int32_t get_i32(ParameterId id);
    float get_f32(ParameterId id);
    boost::string_ref get_string(ParameterId id);

    // setter
    void set_i32(ParameterId id, int32_t val);
    void set_f32(ParameterId id, float val);
    void set_string(ParameterId id, boost::string_ref val);

    // pointer to parameter data
    void *ptr(ParameterId id);
    const void *cptr(ParameterId id) const;

    // input/output
    bool save(std::ostream &out) const;
    bool load(std::istream &in);
};
