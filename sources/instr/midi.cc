//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "instr/synth.h"
#include "utility/logging.h"

#undef LOG_TAG
#define LOG_TAG "MIDI"

static constexpr bool midi_debug = false;

bool OrganSynth::receiving_on_channel(unsigned ch) const
{
    int rch = recv_channel_;
    bool omni = rch == -1;
    return omni || ch == (unsigned)rch;
}

void OrganSynth::dispatchmidi(const uint8_t *msg, uint32_t len)
{
    if (len < 2)
        return;

    unsigned status = msg[0];
    unsigned histat = status >> 4;
    unsigned lostat = status & 0b1111;

    if (!receiving_on_channel(lostat))
        return;

    uint8_t d1 = msg[1], d2 = (len < 3) ? 0 : msg[2];
    switch (histat) {
    case 0b1100:
        if (midi_debug)
            LOGD("program change program=%u", d1);
        program_number_ = d1;
        break;
    case 0b1000:
    L_noteoff:
        if (midi_debug)
            LOGD("note off note=%u velocity=%u", d1, d2);
        vcm_.release_note(d1, d2);
        break;
    case 0b1001:
        if (d2 == 0)
            goto L_noteoff;
        if (midi_debug)
            LOGD("note on note=%u velocity=%u", d1, d2);
        vcm_.trigger(d1, d2);
        break;
    case 0b1010:
        if (midi_debug)
            LOGD("aftertouch note=%u velocity=%u", d1, d2);
        break;
    case 0b1011:
        if (midi_debug)
            LOGD("control change cc=%u value=%u", d1, d2);
        switch (d1) {
            // bank select
        case 0: {
            unsigned lsb = bank_number_ & ((1 << 7) - 1);
            bank_number_ = (d2 << 7) | lsb;
            break;
        }
        case 32: {
            unsigned msb = bank_number_ >> 7;
            bank_number_ = (msb << 7) | d2;
            break;
        }
            // channel mode message
        case 120:
            for (unsigned i = 0; i < instrument_polyphony; ++i)
                vcm_.kill_voice(i);
            break;
        case 121:
            /// TODO
            break;
        case 123:
            for (unsigned i = 0; i < instrument_polyphony; ++i)
                vcm_.release_voice(i, 0);
            break;
        }
        break;
    case 0b1110: {
        unsigned pb = (d2 << 7) | d1;
        if (midi_debug)
            LOGD("pitch bend value=%u", pb);
        pitchbend_ = int(pb) - 8192;
    }
    }
}
