//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "instr/synth.h"

typedef struct _LADSPA_Descriptor LADSPA_Descriptor;

extern const LADSPA_Descriptor *ext_caps_plate;
extern const LADSPA_Descriptor *ext_caps_platex2;
#ifndef EXPERIMENT_SATURATOR
extern const LADSPA_Descriptor *ext_caps_saturate;
#endif

void ext_init();
