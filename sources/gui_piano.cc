//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "gui_piano.h"
#include <QtGui/QPainter>
#include <QtGui/QPaintEvent>
#include <QtGui/QMouseEvent>
#include <QtCore/QDebug>

static const unsigned keyw = 24;
static const unsigned keyh = keyw * 4;
static constexpr float keyoffs[12] = {0,    0.6, 1,   1.8, 2,    3,
                                      3.55, 4,   4.7, 5,   5.85, 6};
static constexpr bool black[12] = {0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0};
static constexpr unsigned kboct = 2;

Piano::Piano(unsigned octs, QWidget *parent)
    : QWidget(parent)
    , octs_(octs)
{
    keyval_.resize(octs * 12);
}

QSize Piano::sizeHint() const
{
    const unsigned octs = octs_;
    return QSize((keyoffs[11] + 1.0f) * keyw * octs + 1, keyh + 1);
}

void Piano::paintEvent(QPaintEvent *event)
{
    QPainter p(this);

    const unsigned octs = octs_;

    for (unsigned key = 0; key < octs * 12; ++key) {
        if (!black[key % 12]) {
            QRectF rect = keyRect(key);
            QColor keycolor = Qt::white;
            if (keyval_[key])
                keycolor = Qt::gray;
            p.fillRect(rect, keycolor);
            p.setPen(Qt::black);
            p.drawRect(rect);
        }
    }

    for (unsigned key = 0; key < octs * 12; ++key) {
        if (black[key % 12]) {
            QRectF rect = keyRect(key);
            QColor keycolor = Qt::black;
            if (keyval_[key])
                keycolor = Qt::gray;
            p.fillRect(rect, keycolor);
            p.setPen(Qt::black);
            p.drawRect(rect);
        }
    }
}

void Piano::mousePressEvent(QMouseEvent *event)
{
    unsigned key = keyAtPos(event->pos());
    if (key != unsigned(-1)) {
        keyval_[key] = 1;
        mousePressedKey_ = key;
        emit onKeyPressed(key);
        this->update();
    }
    QWidget::mousePressEvent(event);
}

void Piano::mouseReleaseEvent(QMouseEvent *event)
{
    if (mousePressedKey_ != unsigned(-1)) {
        keyval_[mousePressedKey_] = 0;
        emit onKeyReleased(mousePressedKey_);
        mousePressedKey_ = -1;
        this->update();
    }
}

void Piano::mouseMoveEvent(QMouseEvent *event)
{
    if (mousePressedKey_ != unsigned(-1)) {
        unsigned key = keyAtPos(event->pos());
        if (mousePressedKey_ != key) {
            keyval_[mousePressedKey_] = 0;
            emit onKeyReleased(mousePressedKey_);
            // mousePressedKey_ = -1;
            if (key != unsigned(-1)) {
                keyval_[key] = 1;
                mousePressedKey_ = key;
                emit onKeyPressed(key);
            }
            this->update();
        }
    }
}

#if defined(Q_OS_DARWIN)
#elif defined(Q_OS_UNIX)
static constexpr int key_scancodes[] = {
    52, 39, 53, 40, 54, 55, 42, 56, 43, 57, 44, 58,  // bottom row oct1
    59, 46, 60, 47, 61,  // bottom row lower half oct2
    24, 11, 25, 12, 26, 13, 27,  // top row upper half oct2
    28, 15, 29, 16, 30, 31, 18, 32, 19, 33, 20, 34,  // top row oct3
};

static unsigned key_for_scancode(int code)
{
    auto beg = std::begin(key_scancodes), end = std::end(key_scancodes);
    auto it = std::find(beg, end, code);
    if (it == end)
        return -1;
    return std::distance(beg, it);
}
#endif

void Piano::keyPressEvent(QKeyEvent *event)
{
    if (event->isAutoRepeat())
        return;
    if (event->modifiers() != 0)
        return;

    unsigned key = -1;

#if defined(Q_OS_DARWIN)
#elif defined(Q_OS_UNIX)
    key = key_for_scancode(event->nativeScanCode());
#endif

    if (key != unsigned(-1)) {
        key += kboct * 12;
        if (key < octs_ * 12 && keyval_[key] == 0) {
            keyval_[key] = 1;
            emit onKeyPressed(key);
            this->update();
        }
    }
    QWidget::keyPressEvent(event);
}

void Piano::keyReleaseEvent(QKeyEvent *event)
{
    if (event->isAutoRepeat())
        return;

    unsigned key = -1;

#if defined(Q_OS_DARWIN)
#elif defined(Q_OS_UNIX)
    key = key_for_scancode(event->nativeScanCode());
#endif

    if (key != unsigned(-1)) {
        key += kboct * 12;
        if (key < octs_ * 12 && keyval_[key] != 0) {
            keyval_[key] = 0;
            emit onKeyReleased(key);
            this->update();
        }
    }
    QWidget::keyReleaseEvent(event);
}

QRectF Piano::keyRect(unsigned key) const
{
    unsigned oct = key / 12;
    unsigned note = key % 12;
    float octwidth = (keyoffs[11] + 1.0f) * keyw;
    float octx = octwidth * oct;
    float notex = octx + keyoffs[note] * keyw;
    float notew = black[note] ? (0.6f * keyw) : keyw;
    float noteh = black[note] ? (0.6f * keyh) : keyh;
    return QRectF(notex, 0.0f, notew, noteh);
}

unsigned Piano::keyAtPos(QPointF pos) const
{
    const unsigned octs = octs_;

    for (unsigned key = 0; key < octs * 12; ++key) {
        if (black[key % 12]) {
            if (keyRect(key).contains(pos))
                return key;
        }
    }

    for (unsigned key = 0; key < octs * 12; ++key) {
        if (!black[key % 12]) {
            if (keyRect(key).contains(pos))
                return key;
        }
    }

    return -1;
}
