#include <cmath>

namespace fftpack {

/* DECK RADF2 */
template <class R>
/* Subroutine */[[gnu::always_inline]] static inline void
radf2(int ido, int l1, const R *__restrict cc, R *__restrict ch, const R *__restrict wa1)
{
    /* System generated locals */
    int ch_dim1, ch_offset, cc_dim1, cc_dim2, cc_offset, i__1, i__2;

    /* Local variables */
    int i__, k, ic;
    R ti2, tr2;
    int idp2;

    /* ***BEGIN PROLOGUE  RADF2 */
    /* ***SUBSIDIARY */
    /* ***PURPOSE  Calculate the fast Fourier transform of subvectors of */
    /*            length two. */
    /* ***LIBRARY   SLATEC (FFTPACK) */
    /* ***TYPE      SINGLE PRECISION (RADF2-S) */
    /* ***AUTHOR  Swarztrauber, P. N., (NCAR) */
    /* ***ROUTINES CALLED  (NONE) */
    /* ***REVISION HISTORY  (YYMMDD) */
    /*   790601  DATE WRITTEN */
    /*   830401  Modified to use SLATEC library source file format. */
    /*   860115  Modified by Ron Boisvert to adhere to Fortran 77 by */
    /*           changing dummy array size declarations (1) to (*). */
    /*   881128  Modified by Dick Valent to meet prologue standards. */
    /*   890831  Modified array declarations.  (WRB) */
    /*   891214  Prologue converted to Version 4.0 format.  (BAB) */
    /*   900402  Added TYPE section.  (WRB) */
    /* ***END PROLOGUE  RADF2 */
    /* ***FIRST EXECUTABLE STATEMENT  RADF2 */
    /* Parameter adjustments */
    ch_dim1 = ido;
    ch_offset = 1 + ch_dim1 * 3;
    ch -= ch_offset;
    cc_dim1 = ido;
    cc_dim2 = l1;
    cc_offset = 1 + cc_dim1 * (1 + cc_dim2);
    cc -= cc_offset;
    --wa1;

    /* Function Body */
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        ch[((k << 1) + 1) * ch_dim1 + 1] =
            cc[(k + cc_dim2) * cc_dim1 + 1] + cc[(k + (cc_dim2 << 1)) * cc_dim1 + 1];
        ch[ido + ((k << 1) + 2) * ch_dim1] =
            cc[(k + cc_dim2) * cc_dim1 + 1] - cc[(k + (cc_dim2 << 1)) * cc_dim1 + 1];
        /* L101: */
    }
    if ((i__1 = ido - 2) < 0) {
        goto L107;
    }
    else if (i__1 == 0) {
        goto L105;
    }
    else {
        goto L102;
    }
L102:
    idp2 = ido + 2;
    if ((ido - 1) / 2 < l1) {
        goto L108;
    }
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        /* DIR$ IVDEP */
        i__2 = ido;
        for (i__ = 3; i__ <= i__2; i__ += 2) {
            ic = idp2 - i__;
            tr2 = wa1[i__ - 2] * cc[i__ - 1 + (k + (cc_dim2 << 1)) * cc_dim1] +
                  wa1[i__ - 1] * cc[i__ + (k + (cc_dim2 << 1)) * cc_dim1];
            ti2 = wa1[i__ - 2] * cc[i__ + (k + (cc_dim2 << 1)) * cc_dim1] -
                  wa1[i__ - 1] * cc[i__ - 1 + (k + (cc_dim2 << 1)) * cc_dim1];
            ch[i__ + ((k << 1) + 1) * ch_dim1] = cc[i__ + (k + cc_dim2) * cc_dim1] + ti2;
            ch[ic + ((k << 1) + 2) * ch_dim1] = ti2 - cc[i__ + (k + cc_dim2) * cc_dim1];
            ch[i__ - 1 + ((k << 1) + 1) * ch_dim1] =
                cc[i__ - 1 + (k + cc_dim2) * cc_dim1] + tr2;
            ch[ic - 1 + ((k << 1) + 2) * ch_dim1] =
                cc[i__ - 1 + (k + cc_dim2) * cc_dim1] - tr2;
            /* L103: */
        }
        /* L104: */
    }
    goto L111;
L108:
    i__1 = ido;
    for (i__ = 3; i__ <= i__1; i__ += 2) {
        ic = idp2 - i__;
        /* DIR$ IVDEP */
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            tr2 = wa1[i__ - 2] * cc[i__ - 1 + (k + (cc_dim2 << 1)) * cc_dim1] +
                  wa1[i__ - 1] * cc[i__ + (k + (cc_dim2 << 1)) * cc_dim1];
            ti2 = wa1[i__ - 2] * cc[i__ + (k + (cc_dim2 << 1)) * cc_dim1] -
                  wa1[i__ - 1] * cc[i__ - 1 + (k + (cc_dim2 << 1)) * cc_dim1];
            ch[i__ + ((k << 1) + 1) * ch_dim1] = cc[i__ + (k + cc_dim2) * cc_dim1] + ti2;
            ch[ic + ((k << 1) + 2) * ch_dim1] = ti2 - cc[i__ + (k + cc_dim2) * cc_dim1];
            ch[i__ - 1 + ((k << 1) + 1) * ch_dim1] =
                cc[i__ - 1 + (k + cc_dim2) * cc_dim1] + tr2;
            ch[ic - 1 + ((k << 1) + 2) * ch_dim1] =
                cc[i__ - 1 + (k + cc_dim2) * cc_dim1] - tr2;
            /* L109: */
        }
        /* L110: */
    }
L111:
    if (ido % 2 == 1) {
        return;
    }
L105:
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        ch[((k << 1) + 2) * ch_dim1 + 1] = -cc[ido + (k + (cc_dim2 << 1)) * cc_dim1];
        ch[ido + ((k << 1) + 1) * ch_dim1] = cc[ido + (k + cc_dim2) * cc_dim1];
        /* L106: */
    }
L107:
    return;
} /* radf2_ */

/* DECK RADF3 */
template <class R>
/* Subroutine */[[gnu::always_inline]] static inline void
radf3(int ido, int l1, const R *__restrict cc, R *__restrict ch,
      const R *__restrict wa1, const R *__restrict wa2)
{
    /* System generated locals */
    int ch_dim1, ch_offset, cc_dim1, cc_dim2, cc_offset, i__1, i__2;

    /* Local variables */
    int i__, k, ic;
    R ci2, di2, di3, cr2, dr2, dr3, ti2, ti3, tr2, tr3;
    int idp2;
    R taui, taur;

    /* ***BEGIN PROLOGUE  RADF3 */
    /* ***SUBSIDIARY */
    /* ***PURPOSE  Calculate the fast Fourier transform of subvectors of */
    /*            length three. */
    /* ***LIBRARY   SLATEC (FFTPACK) */
    /* ***TYPE      SINGLE PRECISION (RADF3-S) */
    /* ***AUTHOR  Swarztrauber, P. N., (NCAR) */
    /* ***ROUTINES CALLED  (NONE) */
    /* ***REVISION HISTORY  (YYMMDD) */
    /*   790601  DATE WRITTEN */
    /*   830401  Modified to use SLATEC library source file format. */
    /*   860115  Modified by Ron Boisvert to adhere to Fortran 77 by */
    /*           (a) changing dummy array size declarations (1) to (*), */
    /*           (b) changing definition of variable TAUI by using */
    /*               FORTRAN intrinsic function SQRT instead of a DATA */
    /*               statement. */
    /*   881128  Modified by Dick Valent to meet prologue standards. */
    /*   890831  Modified array declarations.  (WRB) */
    /*   891214  Prologue converted to Version 4.0 format.  (BAB) */
    /*   900402  Added TYPE section.  (WRB) */
    /* ***END PROLOGUE  RADF3 */
    /* ***FIRST EXECUTABLE STATEMENT  RADF3 */
    /* Parameter adjustments */
    ch_dim1 = ido;
    ch_offset = 1 + (ch_dim1 << 2);
    ch -= ch_offset;
    cc_dim1 = ido;
    cc_dim2 = l1;
    cc_offset = 1 + cc_dim1 * (1 + cc_dim2);
    cc -= cc_offset;
    --wa1;
    --wa2;

    /* Function Body */
    taur = R(-.5);
    taui = std::sqrt(R(3.)) * R(.5);
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        cr2 = cc[(k + (cc_dim2 << 1)) * cc_dim1 + 1] + cc[(k + cc_dim2 * 3) * cc_dim1 + 1];
        ch[(k * 3 + 1) * ch_dim1 + 1] = cc[(k + cc_dim2) * cc_dim1 + 1] + cr2;
        ch[(k * 3 + 3) * ch_dim1 + 1] =
            taui * (cc[(k + cc_dim2 * 3) * cc_dim1 + 1] -
                    cc[(k + (cc_dim2 << 1)) * cc_dim1 + 1]);
        ch[ido + (k * 3 + 2) * ch_dim1] = cc[(k + cc_dim2) * cc_dim1 + 1] + taur * cr2;
        /* L101: */
    }
    if (ido == 1) {
        return;
    }
    idp2 = ido + 2;
    if ((ido - 1) / 2 < l1) {
        goto L104;
    }
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        /* DIR$ IVDEP */
        i__2 = ido;
        for (i__ = 3; i__ <= i__2; i__ += 2) {
            ic = idp2 - i__;
            dr2 = wa1[i__ - 2] * cc[i__ - 1 + (k + (cc_dim2 << 1)) * cc_dim1] +
                  wa1[i__ - 1] * cc[i__ + (k + (cc_dim2 << 1)) * cc_dim1];
            di2 = wa1[i__ - 2] * cc[i__ + (k + (cc_dim2 << 1)) * cc_dim1] -
                  wa1[i__ - 1] * cc[i__ - 1 + (k + (cc_dim2 << 1)) * cc_dim1];
            dr3 = wa2[i__ - 2] * cc[i__ - 1 + (k + cc_dim2 * 3) * cc_dim1] +
                  wa2[i__ - 1] * cc[i__ + (k + cc_dim2 * 3) * cc_dim1];
            di3 = wa2[i__ - 2] * cc[i__ + (k + cc_dim2 * 3) * cc_dim1] -
                  wa2[i__ - 1] * cc[i__ - 1 + (k + cc_dim2 * 3) * cc_dim1];
            cr2 = dr2 + dr3;
            ci2 = di2 + di3;
            ch[i__ - 1 + (k * 3 + 1) * ch_dim1] =
                cc[i__ - 1 + (k + cc_dim2) * cc_dim1] + cr2;
            ch[i__ + (k * 3 + 1) * ch_dim1] = cc[i__ + (k + cc_dim2) * cc_dim1] + ci2;
            tr2 = cc[i__ - 1 + (k + cc_dim2) * cc_dim1] + taur * cr2;
            ti2 = cc[i__ + (k + cc_dim2) * cc_dim1] + taur * ci2;
            tr3 = taui * (di2 - di3);
            ti3 = taui * (dr3 - dr2);
            ch[i__ - 1 + (k * 3 + 3) * ch_dim1] = tr2 + tr3;
            ch[ic - 1 + (k * 3 + 2) * ch_dim1] = tr2 - tr3;
            ch[i__ + (k * 3 + 3) * ch_dim1] = ti2 + ti3;
            ch[ic + (k * 3 + 2) * ch_dim1] = ti3 - ti2;
            /* L102: */
        }
        /* L103: */
    }
    return;
L104:
    i__1 = ido;
    for (i__ = 3; i__ <= i__1; i__ += 2) {
        ic = idp2 - i__;
        /* DIR$ IVDEP */
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            dr2 = wa1[i__ - 2] * cc[i__ - 1 + (k + (cc_dim2 << 1)) * cc_dim1] +
                  wa1[i__ - 1] * cc[i__ + (k + (cc_dim2 << 1)) * cc_dim1];
            di2 = wa1[i__ - 2] * cc[i__ + (k + (cc_dim2 << 1)) * cc_dim1] -
                  wa1[i__ - 1] * cc[i__ - 1 + (k + (cc_dim2 << 1)) * cc_dim1];
            dr3 = wa2[i__ - 2] * cc[i__ - 1 + (k + cc_dim2 * 3) * cc_dim1] +
                  wa2[i__ - 1] * cc[i__ + (k + cc_dim2 * 3) * cc_dim1];
            di3 = wa2[i__ - 2] * cc[i__ + (k + cc_dim2 * 3) * cc_dim1] -
                  wa2[i__ - 1] * cc[i__ - 1 + (k + cc_dim2 * 3) * cc_dim1];
            cr2 = dr2 + dr3;
            ci2 = di2 + di3;
            ch[i__ - 1 + (k * 3 + 1) * ch_dim1] =
                cc[i__ - 1 + (k + cc_dim2) * cc_dim1] + cr2;
            ch[i__ + (k * 3 + 1) * ch_dim1] = cc[i__ + (k + cc_dim2) * cc_dim1] + ci2;
            tr2 = cc[i__ - 1 + (k + cc_dim2) * cc_dim1] + taur * cr2;
            ti2 = cc[i__ + (k + cc_dim2) * cc_dim1] + taur * ci2;
            tr3 = taui * (di2 - di3);
            ti3 = taui * (dr3 - dr2);
            ch[i__ - 1 + (k * 3 + 3) * ch_dim1] = tr2 + tr3;
            ch[ic - 1 + (k * 3 + 2) * ch_dim1] = tr2 - tr3;
            ch[i__ + (k * 3 + 3) * ch_dim1] = ti2 + ti3;
            ch[ic + (k * 3 + 2) * ch_dim1] = ti3 - ti2;
            /* L105: */
        }
        /* L106: */
    }
    return;
} /* radf3_ */

/* DECK RADF4 */
template <class R>
/* Subroutine */[[gnu::always_inline]] static inline void
radf4(int ido, int l1, const R *__restrict cc, R *__restrict ch,
      const R *__restrict wa1, const R *__restrict wa2, const R *__restrict wa3)
{
    /* System generated locals */
    int cc_dim1, cc_dim2, cc_offset, ch_dim1, ch_offset, i__1, i__2;

    /* Local variables */
    int i__, k, ic;
    R ci2, ci3, ci4, cr2, cr3, cr4, ti1, ti2, ti3, ti4, tr1, tr2, tr3, tr4;
    int idp2;
    R hsqt2;

    /* ***BEGIN PROLOGUE  RADF4 */
    /* ***SUBSIDIARY */
    /* ***PURPOSE  Calculate the fast Fourier transform of subvectors of */
    /*            length four. */
    /* ***LIBRARY   SLATEC (FFTPACK) */
    /* ***TYPE      SINGLE PRECISION (RADF4-S) */
    /* ***AUTHOR  Swarztrauber, P. N., (NCAR) */
    /* ***ROUTINES CALLED  (NONE) */
    /* ***REVISION HISTORY  (YYMMDD) */
    /*   790601  DATE WRITTEN */
    /*   830401  Modified to use SLATEC library source file format. */
    /*   860115  Modified by Ron Boisvert to adhere to Fortran 77 by */
    /*           (a) changing dummy array size declarations (1) to (*). */
    /*           (b) changing definition of variable HSQT2 by using */
    /*               FORTRAN intrinsic function SQRT instead of a DATA */
    /*               statement. */
    /*   881128  Modified by Dick Valent to meet prologue standards. */
    /*   890831  Modified array declarations.  (WRB) */
    /*   891214  Prologue converted to Version 4.0 format.  (BAB) */
    /*   900402  Added TYPE section.  (WRB) */
    /* ***END PROLOGUE  RADF4 */
    /* ***FIRST EXECUTABLE STATEMENT  RADF4 */
    /* Parameter adjustments */
    ch_dim1 = ido;
    ch_offset = 1 + ch_dim1 * 5;
    ch -= ch_offset;
    cc_dim1 = ido;
    cc_dim2 = l1;
    cc_offset = 1 + cc_dim1 * (1 + cc_dim2);
    cc -= cc_offset;
    --wa1;
    --wa2;
    --wa3;

    /* Function Body */
    hsqt2 = std::sqrt(R(2.)) * R(.5);
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        tr1 = cc[(k + (cc_dim2 << 1)) * cc_dim1 + 1] +
              cc[(k + (cc_dim2 << 2)) * cc_dim1 + 1];
        tr2 = cc[(k + cc_dim2) * cc_dim1 + 1] + cc[(k + cc_dim2 * 3) * cc_dim1 + 1];
        ch[((k << 2) + 1) * ch_dim1 + 1] = tr1 + tr2;
        ch[ido + ((k << 2) + 4) * ch_dim1] = tr2 - tr1;
        ch[ido + ((k << 2) + 2) * ch_dim1] =
            cc[(k + cc_dim2) * cc_dim1 + 1] - cc[(k + cc_dim2 * 3) * cc_dim1 + 1];
        ch[((k << 2) + 3) * ch_dim1 + 1] = cc[(k + (cc_dim2 << 2)) * cc_dim1 + 1] -
                                           cc[(k + (cc_dim2 << 1)) * cc_dim1 + 1];
        /* L101: */
    }
    if ((i__1 = ido - 2) < 0) {
        goto L107;
    }
    else if (i__1 == 0) {
        goto L105;
    }
    else {
        goto L102;
    }
L102:
    idp2 = ido + 2;
    if ((ido - 1) / 2 < l1) {
        goto L111;
    }
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        /* DIR$ IVDEP */
        i__2 = ido;
        for (i__ = 3; i__ <= i__2; i__ += 2) {
            ic = idp2 - i__;
            cr2 = wa1[i__ - 2] * cc[i__ - 1 + (k + (cc_dim2 << 1)) * cc_dim1] +
                  wa1[i__ - 1] * cc[i__ + (k + (cc_dim2 << 1)) * cc_dim1];
            ci2 = wa1[i__ - 2] * cc[i__ + (k + (cc_dim2 << 1)) * cc_dim1] -
                  wa1[i__ - 1] * cc[i__ - 1 + (k + (cc_dim2 << 1)) * cc_dim1];
            cr3 = wa2[i__ - 2] * cc[i__ - 1 + (k + cc_dim2 * 3) * cc_dim1] +
                  wa2[i__ - 1] * cc[i__ + (k + cc_dim2 * 3) * cc_dim1];
            ci3 = wa2[i__ - 2] * cc[i__ + (k + cc_dim2 * 3) * cc_dim1] -
                  wa2[i__ - 1] * cc[i__ - 1 + (k + cc_dim2 * 3) * cc_dim1];
            cr4 = wa3[i__ - 2] * cc[i__ - 1 + (k + (cc_dim2 << 2)) * cc_dim1] +
                  wa3[i__ - 1] * cc[i__ + (k + (cc_dim2 << 2)) * cc_dim1];
            ci4 = wa3[i__ - 2] * cc[i__ + (k + (cc_dim2 << 2)) * cc_dim1] -
                  wa3[i__ - 1] * cc[i__ - 1 + (k + (cc_dim2 << 2)) * cc_dim1];
            tr1 = cr2 + cr4;
            tr4 = cr4 - cr2;
            ti1 = ci2 + ci4;
            ti4 = ci2 - ci4;
            ti2 = cc[i__ + (k + cc_dim2) * cc_dim1] + ci3;
            ti3 = cc[i__ + (k + cc_dim2) * cc_dim1] - ci3;
            tr2 = cc[i__ - 1 + (k + cc_dim2) * cc_dim1] + cr3;
            tr3 = cc[i__ - 1 + (k + cc_dim2) * cc_dim1] - cr3;
            ch[i__ - 1 + ((k << 2) + 1) * ch_dim1] = tr1 + tr2;
            ch[ic - 1 + ((k << 2) + 4) * ch_dim1] = tr2 - tr1;
            ch[i__ + ((k << 2) + 1) * ch_dim1] = ti1 + ti2;
            ch[ic + ((k << 2) + 4) * ch_dim1] = ti1 - ti2;
            ch[i__ - 1 + ((k << 2) + 3) * ch_dim1] = ti4 + tr3;
            ch[ic - 1 + ((k << 2) + 2) * ch_dim1] = tr3 - ti4;
            ch[i__ + ((k << 2) + 3) * ch_dim1] = tr4 + ti3;
            ch[ic + ((k << 2) + 2) * ch_dim1] = tr4 - ti3;
            /* L103: */
        }
        /* L104: */
    }
    goto L110;
L111:
    i__1 = ido;
    for (i__ = 3; i__ <= i__1; i__ += 2) {
        ic = idp2 - i__;
        /* DIR$ IVDEP */
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            cr2 = wa1[i__ - 2] * cc[i__ - 1 + (k + (cc_dim2 << 1)) * cc_dim1] +
                  wa1[i__ - 1] * cc[i__ + (k + (cc_dim2 << 1)) * cc_dim1];
            ci2 = wa1[i__ - 2] * cc[i__ + (k + (cc_dim2 << 1)) * cc_dim1] -
                  wa1[i__ - 1] * cc[i__ - 1 + (k + (cc_dim2 << 1)) * cc_dim1];
            cr3 = wa2[i__ - 2] * cc[i__ - 1 + (k + cc_dim2 * 3) * cc_dim1] +
                  wa2[i__ - 1] * cc[i__ + (k + cc_dim2 * 3) * cc_dim1];
            ci3 = wa2[i__ - 2] * cc[i__ + (k + cc_dim2 * 3) * cc_dim1] -
                  wa2[i__ - 1] * cc[i__ - 1 + (k + cc_dim2 * 3) * cc_dim1];
            cr4 = wa3[i__ - 2] * cc[i__ - 1 + (k + (cc_dim2 << 2)) * cc_dim1] +
                  wa3[i__ - 1] * cc[i__ + (k + (cc_dim2 << 2)) * cc_dim1];
            ci4 = wa3[i__ - 2] * cc[i__ + (k + (cc_dim2 << 2)) * cc_dim1] -
                  wa3[i__ - 1] * cc[i__ - 1 + (k + (cc_dim2 << 2)) * cc_dim1];
            tr1 = cr2 + cr4;
            tr4 = cr4 - cr2;
            ti1 = ci2 + ci4;
            ti4 = ci2 - ci4;
            ti2 = cc[i__ + (k + cc_dim2) * cc_dim1] + ci3;
            ti3 = cc[i__ + (k + cc_dim2) * cc_dim1] - ci3;
            tr2 = cc[i__ - 1 + (k + cc_dim2) * cc_dim1] + cr3;
            tr3 = cc[i__ - 1 + (k + cc_dim2) * cc_dim1] - cr3;
            ch[i__ - 1 + ((k << 2) + 1) * ch_dim1] = tr1 + tr2;
            ch[ic - 1 + ((k << 2) + 4) * ch_dim1] = tr2 - tr1;
            ch[i__ + ((k << 2) + 1) * ch_dim1] = ti1 + ti2;
            ch[ic + ((k << 2) + 4) * ch_dim1] = ti1 - ti2;
            ch[i__ - 1 + ((k << 2) + 3) * ch_dim1] = ti4 + tr3;
            ch[ic - 1 + ((k << 2) + 2) * ch_dim1] = tr3 - ti4;
            ch[i__ + ((k << 2) + 3) * ch_dim1] = tr4 + ti3;
            ch[ic + ((k << 2) + 2) * ch_dim1] = tr4 - ti3;
            /* L108: */
        }
        /* L109: */
    }
L110:
    if (ido % 2 == 1) {
        return;
    }
L105:
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        ti1 = -hsqt2 * (cc[ido + (k + (cc_dim2 << 1)) * cc_dim1] +
                        cc[ido + (k + (cc_dim2 << 2)) * cc_dim1]);
        tr1 = hsqt2 * (cc[ido + (k + (cc_dim2 << 1)) * cc_dim1] -
                       cc[ido + (k + (cc_dim2 << 2)) * cc_dim1]);
        ch[ido + ((k << 2) + 1) * ch_dim1] = tr1 + cc[ido + (k + cc_dim2) * cc_dim1];
        ch[ido + ((k << 2) + 3) * ch_dim1] = cc[ido + (k + cc_dim2) * cc_dim1] - tr1;
        ch[((k << 2) + 2) * ch_dim1 + 1] = ti1 - cc[ido + (k + cc_dim2 * 3) * cc_dim1];
        ch[((k << 2) + 4) * ch_dim1 + 1] = ti1 + cc[ido + (k + cc_dim2 * 3) * cc_dim1];
        /* L106: */
    }
L107:
    return;
} /* radf4_ */

/* DECK RADF5 */
template <class R>
/* Subroutine */[[gnu::always_inline]] static inline void
radf5(int ido, int l1, const R *__restrict cc, R *__restrict ch, const R *__restrict wa1,
      const R *__restrict wa2, const R *__restrict wa3, const R *__restrict wa4)
{
    /* System generated locals */
    int cc_dim1, cc_dim2, cc_offset, ch_dim1, ch_offset, i__1, i__2;

    /* Local variables */
    int i__, k, ic;
    R pi, ci2, di2, ci4, ci5, di3, di4, di5, ci3, cr2, cr3, dr2, dr3, dr4, dr5,
        cr5, cr4, ti2, ti3, ti5, ti4, tr2, tr3, tr4, tr5, ti11, ti12, tr11, tr12;
    int idp2;

    /* ***BEGIN PROLOGUE  RADF5 */
    /* ***SUBSIDIARY */
    /* ***PURPOSE  Calculate the fast Fourier transform of subvectors of */
    /*            length five. */
    /* ***LIBRARY   SLATEC (FFTPACK) */
    /* ***TYPE      SINGLE PRECISION (RADF5-S) */
    /* ***AUTHOR  Swarztrauber, P. N., (NCAR) */
    /* ***ROUTINES CALLED  (NONE) */
    /* ***REVISION HISTORY  (YYMMDD) */
    /*   790601  DATE WRITTEN */
    /*   830401  Modified to use SLATEC library source file format. */
    /*   860115  Modified by Ron Boisvert to adhere to Fortran 77 by */
    /*           (a) changing dummy array size declarations (1) to (*), */
    /*           (b) changing definition of variables PI, TI11, TI12, */
    /*               TR11, TR12 by using FORTRAN intrinsic functions ATAN */
    /*               and SIN instead of DATA statements. */
    /*   881128  Modified by Dick Valent to meet prologue standards. */
    /*   890831  Modified array declarations.  (WRB) */
    /*   891214  Prologue converted to Version 4.0 format.  (BAB) */
    /*   900402  Added TYPE section.  (WRB) */
    /* ***END PROLOGUE  RADF5 */
    /* ***FIRST EXECUTABLE STATEMENT  RADF5 */
    /* Parameter adjustments */
    ch_dim1 = ido;
    ch_offset = 1 + ch_dim1 * 6;
    ch -= ch_offset;
    cc_dim1 = ido;
    cc_dim2 = l1;
    cc_offset = 1 + cc_dim1 * (1 + cc_dim2);
    cc -= cc_offset;
    --wa1;
    --wa2;
    --wa3;
    --wa4;

    /* Function Body */
    pi = R(3.14159265358979323846L);
    tr11 = std::sin(pi * R(.1));
    ti11 = std::sin(pi * R(.4));
    tr12 = -std::sin(pi * R(.3));
    ti12 = std::sin(pi * R(.2));
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        cr2 = cc[(k + cc_dim2 * 5) * cc_dim1 + 1] + cc[(k + (cc_dim2 << 1)) * cc_dim1 + 1];
        ci5 = cc[(k + cc_dim2 * 5) * cc_dim1 + 1] - cc[(k + (cc_dim2 << 1)) * cc_dim1 + 1];
        cr3 = cc[(k + (cc_dim2 << 2)) * cc_dim1 + 1] + cc[(k + cc_dim2 * 3) * cc_dim1 + 1];
        ci4 = cc[(k + (cc_dim2 << 2)) * cc_dim1 + 1] - cc[(k + cc_dim2 * 3) * cc_dim1 + 1];
        ch[(k * 5 + 1) * ch_dim1 + 1] = cc[(k + cc_dim2) * cc_dim1 + 1] + cr2 + cr3;
        ch[ido + (k * 5 + 2) * ch_dim1] =
            cc[(k + cc_dim2) * cc_dim1 + 1] + tr11 * cr2 + tr12 * cr3;
        ch[(k * 5 + 3) * ch_dim1 + 1] = ti11 * ci5 + ti12 * ci4;
        ch[ido + (k * 5 + 4) * ch_dim1] =
            cc[(k + cc_dim2) * cc_dim1 + 1] + tr12 * cr2 + tr11 * cr3;
        ch[(k * 5 + 5) * ch_dim1 + 1] = ti12 * ci5 - ti11 * ci4;
        /* L101: */
    }
    if (ido == 1) {
        return;
    }
    idp2 = ido + 2;
    if ((ido - 1) / 2 < l1) {
        goto L104;
    }
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        /* DIR$ IVDEP */
        i__2 = ido;
        for (i__ = 3; i__ <= i__2; i__ += 2) {
            ic = idp2 - i__;
            dr2 = wa1[i__ - 2] * cc[i__ - 1 + (k + (cc_dim2 << 1)) * cc_dim1] +
                  wa1[i__ - 1] * cc[i__ + (k + (cc_dim2 << 1)) * cc_dim1];
            di2 = wa1[i__ - 2] * cc[i__ + (k + (cc_dim2 << 1)) * cc_dim1] -
                  wa1[i__ - 1] * cc[i__ - 1 + (k + (cc_dim2 << 1)) * cc_dim1];
            dr3 = wa2[i__ - 2] * cc[i__ - 1 + (k + cc_dim2 * 3) * cc_dim1] +
                  wa2[i__ - 1] * cc[i__ + (k + cc_dim2 * 3) * cc_dim1];
            di3 = wa2[i__ - 2] * cc[i__ + (k + cc_dim2 * 3) * cc_dim1] -
                  wa2[i__ - 1] * cc[i__ - 1 + (k + cc_dim2 * 3) * cc_dim1];
            dr4 = wa3[i__ - 2] * cc[i__ - 1 + (k + (cc_dim2 << 2)) * cc_dim1] +
                  wa3[i__ - 1] * cc[i__ + (k + (cc_dim2 << 2)) * cc_dim1];
            di4 = wa3[i__ - 2] * cc[i__ + (k + (cc_dim2 << 2)) * cc_dim1] -
                  wa3[i__ - 1] * cc[i__ - 1 + (k + (cc_dim2 << 2)) * cc_dim1];
            dr5 = wa4[i__ - 2] * cc[i__ - 1 + (k + cc_dim2 * 5) * cc_dim1] +
                  wa4[i__ - 1] * cc[i__ + (k + cc_dim2 * 5) * cc_dim1];
            di5 = wa4[i__ - 2] * cc[i__ + (k + cc_dim2 * 5) * cc_dim1] -
                  wa4[i__ - 1] * cc[i__ - 1 + (k + cc_dim2 * 5) * cc_dim1];
            cr2 = dr2 + dr5;
            ci5 = dr5 - dr2;
            cr5 = di2 - di5;
            ci2 = di2 + di5;
            cr3 = dr3 + dr4;
            ci4 = dr4 - dr3;
            cr4 = di3 - di4;
            ci3 = di3 + di4;
            ch[i__ - 1 + (k * 5 + 1) * ch_dim1] =
                cc[i__ - 1 + (k + cc_dim2) * cc_dim1] + cr2 + cr3;
            ch[i__ + (k * 5 + 1) * ch_dim1] = cc[i__ + (k + cc_dim2) * cc_dim1] + ci2 + ci3;
            tr2 = cc[i__ - 1 + (k + cc_dim2) * cc_dim1] + tr11 * cr2 + tr12 * cr3;
            ti2 = cc[i__ + (k + cc_dim2) * cc_dim1] + tr11 * ci2 + tr12 * ci3;
            tr3 = cc[i__ - 1 + (k + cc_dim2) * cc_dim1] + tr12 * cr2 + tr11 * cr3;
            ti3 = cc[i__ + (k + cc_dim2) * cc_dim1] + tr12 * ci2 + tr11 * ci3;
            tr5 = ti11 * cr5 + ti12 * cr4;
            ti5 = ti11 * ci5 + ti12 * ci4;
            tr4 = ti12 * cr5 - ti11 * cr4;
            ti4 = ti12 * ci5 - ti11 * ci4;
            ch[i__ - 1 + (k * 5 + 3) * ch_dim1] = tr2 + tr5;
            ch[ic - 1 + (k * 5 + 2) * ch_dim1] = tr2 - tr5;
            ch[i__ + (k * 5 + 3) * ch_dim1] = ti2 + ti5;
            ch[ic + (k * 5 + 2) * ch_dim1] = ti5 - ti2;
            ch[i__ - 1 + (k * 5 + 5) * ch_dim1] = tr3 + tr4;
            ch[ic - 1 + (k * 5 + 4) * ch_dim1] = tr3 - tr4;
            ch[i__ + (k * 5 + 5) * ch_dim1] = ti3 + ti4;
            ch[ic + (k * 5 + 4) * ch_dim1] = ti4 - ti3;
            /* L102: */
        }
        /* L103: */
    }
    return;
L104:
    i__1 = ido;
    for (i__ = 3; i__ <= i__1; i__ += 2) {
        ic = idp2 - i__;
        /* DIR$ IVDEP */
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            dr2 = wa1[i__ - 2] * cc[i__ - 1 + (k + (cc_dim2 << 1)) * cc_dim1] +
                  wa1[i__ - 1] * cc[i__ + (k + (cc_dim2 << 1)) * cc_dim1];
            di2 = wa1[i__ - 2] * cc[i__ + (k + (cc_dim2 << 1)) * cc_dim1] -
                  wa1[i__ - 1] * cc[i__ - 1 + (k + (cc_dim2 << 1)) * cc_dim1];
            dr3 = wa2[i__ - 2] * cc[i__ - 1 + (k + cc_dim2 * 3) * cc_dim1] +
                  wa2[i__ - 1] * cc[i__ + (k + cc_dim2 * 3) * cc_dim1];
            di3 = wa2[i__ - 2] * cc[i__ + (k + cc_dim2 * 3) * cc_dim1] -
                  wa2[i__ - 1] * cc[i__ - 1 + (k + cc_dim2 * 3) * cc_dim1];
            dr4 = wa3[i__ - 2] * cc[i__ - 1 + (k + (cc_dim2 << 2)) * cc_dim1] +
                  wa3[i__ - 1] * cc[i__ + (k + (cc_dim2 << 2)) * cc_dim1];
            di4 = wa3[i__ - 2] * cc[i__ + (k + (cc_dim2 << 2)) * cc_dim1] -
                  wa3[i__ - 1] * cc[i__ - 1 + (k + (cc_dim2 << 2)) * cc_dim1];
            dr5 = wa4[i__ - 2] * cc[i__ - 1 + (k + cc_dim2 * 5) * cc_dim1] +
                  wa4[i__ - 1] * cc[i__ + (k + cc_dim2 * 5) * cc_dim1];
            di5 = wa4[i__ - 2] * cc[i__ + (k + cc_dim2 * 5) * cc_dim1] -
                  wa4[i__ - 1] * cc[i__ - 1 + (k + cc_dim2 * 5) * cc_dim1];
            cr2 = dr2 + dr5;
            ci5 = dr5 - dr2;
            cr5 = di2 - di5;
            ci2 = di2 + di5;
            cr3 = dr3 + dr4;
            ci4 = dr4 - dr3;
            cr4 = di3 - di4;
            ci3 = di3 + di4;
            ch[i__ - 1 + (k * 5 + 1) * ch_dim1] =
                cc[i__ - 1 + (k + cc_dim2) * cc_dim1] + cr2 + cr3;
            ch[i__ + (k * 5 + 1) * ch_dim1] = cc[i__ + (k + cc_dim2) * cc_dim1] + ci2 + ci3;
            tr2 = cc[i__ - 1 + (k + cc_dim2) * cc_dim1] + tr11 * cr2 + tr12 * cr3;
            ti2 = cc[i__ + (k + cc_dim2) * cc_dim1] + tr11 * ci2 + tr12 * ci3;
            tr3 = cc[i__ - 1 + (k + cc_dim2) * cc_dim1] + tr12 * cr2 + tr11 * cr3;
            ti3 = cc[i__ + (k + cc_dim2) * cc_dim1] + tr12 * ci2 + tr11 * ci3;
            tr5 = ti11 * cr5 + ti12 * cr4;
            ti5 = ti11 * ci5 + ti12 * ci4;
            tr4 = ti12 * cr5 - ti11 * cr4;
            ti4 = ti12 * ci5 - ti11 * ci4;
            ch[i__ - 1 + (k * 5 + 3) * ch_dim1] = tr2 + tr5;
            ch[ic - 1 + (k * 5 + 2) * ch_dim1] = tr2 - tr5;
            ch[i__ + (k * 5 + 3) * ch_dim1] = ti2 + ti5;
            ch[ic + (k * 5 + 2) * ch_dim1] = ti5 - ti2;
            ch[i__ - 1 + (k * 5 + 5) * ch_dim1] = tr3 + tr4;
            ch[ic - 1 + (k * 5 + 4) * ch_dim1] = tr3 - tr4;
            ch[i__ + (k * 5 + 5) * ch_dim1] = ti3 + ti4;
            ch[ic + (k * 5 + 4) * ch_dim1] = ti4 - ti3;
            /* L105: */
        }
        /* L106: */
    }
    return;
} /* radf5_ */

/* DECK RADFG */
template <class R>
/* Subroutine */[[gnu::always_inline]] static inline void
radfg(int ido, int ip, int l1, int idl1, R *__restrict cc, R *__restrict c1,
      R *__restrict c2, R *__restrict ch, R *__restrict ch2, const R *__restrict wa)
{
    /* System generated locals */
    int ch_dim1, ch_dim2, ch_offset, cc_dim1, cc_dim2, cc_offset, c1_dim1, c1_dim2,
        c1_offset, c2_dim1, c2_offset, ch2_dim1, ch2_offset, i__1, i__2, i__3;

    /* Local variables */
    int i__, j, k, l, j2, ic, jc, lc, ik, is;
    R dc2, ai1, ai2, ar1, ar2, ds2;
    int nbd;
    R dcp, arg, dsp, tpi, ar1h, ar2h;
    int idp2, ipp2, idij, ipph;

    /* ***BEGIN PROLOGUE  RADFG */
    /* ***SUBSIDIARY */
    /* ***PURPOSE  Calculate the fast Fourier transform of subvectors of */
    /*            arbitrary length. */
    /* ***LIBRARY   SLATEC (FFTPACK) */
    /* ***TYPE      SINGLE PRECISION (RADFG-S) */
    /* ***AUTHOR  Swarztrauber, P. N., (NCAR) */
    /* ***ROUTINES CALLED  (NONE) */
    /* ***REVISION HISTORY  (YYMMDD) */
    /*   790601  DATE WRITTEN */
    /*   830401  Modified to use SLATEC library source file format. */
    /*   860115  Modified by Ron Boisvert to adhere to Fortran 77 by */
    /*           (a) changing dummy array size declarations (1) to (*), */
    /*           (b) changing references to intrinsic function FLOAT */
    /*               to REAL, and */
    /*           (c) changing definition of variable TPI by using */
    /*               FORTRAN intrinsic function ATAN instead of a DATA */
    /*               statement. */
    /*   881128  Modified by Dick Valent to meet prologue standards. */
    /*   890531  Changed all specific intrinsics to generic.  (WRB) */
    /*   890831  Modified array declarations.  (WRB) */
    /*   891214  Prologue converted to Version 4.0 format.  (BAB) */
    /*   900402  Added TYPE section.  (WRB) */
    /* ***END PROLOGUE  RADFG */
    /* ***FIRST EXECUTABLE STATEMENT  RADFG */
    /* Parameter adjustments */
    cc_dim1 = ido;
    cc_dim2 = ip;
    cc_offset = 1 + cc_dim1 * (1 + cc_dim2);
    cc -= cc_offset;
    ch_dim1 = ido;
    ch_dim2 = l1;
    ch_offset = 1 + ch_dim1 * (1 + ch_dim2);
    ch -= ch_offset;
    c1_dim1 = ido;
    c1_dim2 = l1;
    c1_offset = 1 + c1_dim1 * (1 + c1_dim2);
    c1 -= c1_offset;
    ch2_dim1 = idl1;
    ch2_offset = 1 + ch2_dim1;
    ch2 -= ch2_offset;
    c2_dim1 = idl1;
    c2_offset = 1 + c2_dim1;
    c2 -= c2_offset;
    --wa;

    /* Function Body */
    tpi = R(6.28318530717958647693L);
    arg = tpi / ip;
    dcp = std::cos(arg);
    dsp = std::sin(arg);
    ipph = (ip + 1) / 2;
    ipp2 = ip + 2;
    idp2 = ido + 2;
    nbd = (ido - 1) / 2;
    if (ido == 1) {
        goto L119;
    }
    i__1 = idl1;
    for (ik = 1; ik <= i__1; ++ik) {
        ch2[ik + ch2_dim1] = c2[ik + c2_dim1];
        /* L101: */
    }
    i__1 = ip;
    for (j = 2; j <= i__1; ++j) {
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            ch[(k + j * ch_dim2) * ch_dim1 + 1] = c1[(k + j * c1_dim2) * c1_dim1 + 1];
            /* L102: */
        }
        /* L103: */
    }
    if (nbd > l1) {
        goto L107;
    }
    is = -(ido);
    i__1 = ip;
    for (j = 2; j <= i__1; ++j) {
        is += ido;
        idij = is;
        i__2 = ido;
        for (i__ = 3; i__ <= i__2; i__ += 2) {
            idij += 2;
            i__3 = l1;
            for (k = 1; k <= i__3; ++k) {
                ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1] =
                    wa[idij - 1] * c1[i__ - 1 + (k + j * c1_dim2) * c1_dim1] +
                    wa[idij] * c1[i__ + (k + j * c1_dim2) * c1_dim1];
                ch[i__ + (k + j * ch_dim2) * ch_dim1] =
                    wa[idij - 1] * c1[i__ + (k + j * c1_dim2) * c1_dim1] -
                    wa[idij] * c1[i__ - 1 + (k + j * c1_dim2) * c1_dim1];
                /* L104: */
            }
            /* L105: */
        }
        /* L106: */
    }
    goto L111;
L107:
    is = -(ido);
    i__1 = ip;
    for (j = 2; j <= i__1; ++j) {
        is += ido;
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            idij = is;
            /* DIR$ IVDEP */
            i__3 = ido;
            for (i__ = 3; i__ <= i__3; i__ += 2) {
                idij += 2;
                ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1] =
                    wa[idij - 1] * c1[i__ - 1 + (k + j * c1_dim2) * c1_dim1] +
                    wa[idij] * c1[i__ + (k + j * c1_dim2) * c1_dim1];
                ch[i__ + (k + j * ch_dim2) * ch_dim1] =
                    wa[idij - 1] * c1[i__ + (k + j * c1_dim2) * c1_dim1] -
                    wa[idij] * c1[i__ - 1 + (k + j * c1_dim2) * c1_dim1];
                /* L108: */
            }
            /* L109: */
        }
        /* L110: */
    }
L111:
    if (nbd < l1) {
        goto L115;
    }
    i__1 = ipph;
    for (j = 2; j <= i__1; ++j) {
        jc = ipp2 - j;
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            /* DIR$ IVDEP */
            i__3 = ido;
            for (i__ = 3; i__ <= i__3; i__ += 2) {
                c1[i__ - 1 + (k + j * c1_dim2) * c1_dim1] =
                    ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1] +
                    ch[i__ - 1 + (k + jc * ch_dim2) * ch_dim1];
                c1[i__ - 1 + (k + jc * c1_dim2) * c1_dim1] =
                    ch[i__ + (k + j * ch_dim2) * ch_dim1] -
                    ch[i__ + (k + jc * ch_dim2) * ch_dim1];
                c1[i__ + (k + j * c1_dim2) * c1_dim1] =
                    ch[i__ + (k + j * ch_dim2) * ch_dim1] +
                    ch[i__ + (k + jc * ch_dim2) * ch_dim1];
                c1[i__ + (k + jc * c1_dim2) * c1_dim1] =
                    ch[i__ - 1 + (k + jc * ch_dim2) * ch_dim1] -
                    ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1];
                /* L112: */
            }
            /* L113: */
        }
        /* L114: */
    }
    goto L121;
L115:
    i__1 = ipph;
    for (j = 2; j <= i__1; ++j) {
        jc = ipp2 - j;
        i__2 = ido;
        for (i__ = 3; i__ <= i__2; i__ += 2) {
            i__3 = l1;
            for (k = 1; k <= i__3; ++k) {
                c1[i__ - 1 + (k + j * c1_dim2) * c1_dim1] =
                    ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1] +
                    ch[i__ - 1 + (k + jc * ch_dim2) * ch_dim1];
                c1[i__ - 1 + (k + jc * c1_dim2) * c1_dim1] =
                    ch[i__ + (k + j * ch_dim2) * ch_dim1] -
                    ch[i__ + (k + jc * ch_dim2) * ch_dim1];
                c1[i__ + (k + j * c1_dim2) * c1_dim1] =
                    ch[i__ + (k + j * ch_dim2) * ch_dim1] +
                    ch[i__ + (k + jc * ch_dim2) * ch_dim1];
                c1[i__ + (k + jc * c1_dim2) * c1_dim1] =
                    ch[i__ - 1 + (k + jc * ch_dim2) * ch_dim1] -
                    ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1];
                /* L116: */
            }
            /* L117: */
        }
        /* L118: */
    }
    goto L121;
L119:
    i__1 = idl1;
    for (ik = 1; ik <= i__1; ++ik) {
        c2[ik + c2_dim1] = ch2[ik + ch2_dim1];
        /* L120: */
    }
L121:
    i__1 = ipph;
    for (j = 2; j <= i__1; ++j) {
        jc = ipp2 - j;
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            c1[(k + j * c1_dim2) * c1_dim1 + 1] =
                ch[(k + j * ch_dim2) * ch_dim1 + 1] + ch[(k + jc * ch_dim2) * ch_dim1 + 1];
            c1[(k + jc * c1_dim2) * c1_dim1 + 1] =
                ch[(k + jc * ch_dim2) * ch_dim1 + 1] - ch[(k + j * ch_dim2) * ch_dim1 + 1];
            /* L122: */
        }
        /* L123: */
    }

    ar1 = R(1.);
    ai1 = R(0.);
    i__1 = ipph;
    for (l = 2; l <= i__1; ++l) {
        lc = ipp2 - l;
        ar1h = dcp * ar1 - dsp * ai1;
        ai1 = dcp * ai1 + dsp * ar1;
        ar1 = ar1h;
        i__2 = idl1;
        for (ik = 1; ik <= i__2; ++ik) {
            ch2[ik + l * ch2_dim1] = c2[ik + c2_dim1] + ar1 * c2[ik + (c2_dim1 << 1)];
            ch2[ik + lc * ch2_dim1] = ai1 * c2[ik + ip * c2_dim1];
            /* L124: */
        }
        dc2 = ar1;
        ds2 = ai1;
        ar2 = ar1;
        ai2 = ai1;
        i__2 = ipph;
        for (j = 3; j <= i__2; ++j) {
            jc = ipp2 - j;
            ar2h = dc2 * ar2 - ds2 * ai2;
            ai2 = dc2 * ai2 + ds2 * ar2;
            ar2 = ar2h;
            i__3 = idl1;
            for (ik = 1; ik <= i__3; ++ik) {
                ch2[ik + l * ch2_dim1] += ar2 * c2[ik + j * c2_dim1];
                ch2[ik + lc * ch2_dim1] += ai2 * c2[ik + jc * c2_dim1];
                /* L125: */
            }
            /* L126: */
        }
        /* L127: */
    }
    i__1 = ipph;
    for (j = 2; j <= i__1; ++j) {
        i__2 = idl1;
        for (ik = 1; ik <= i__2; ++ik) {
            ch2[ik + ch2_dim1] += c2[ik + j * c2_dim1];
            /* L128: */
        }
        /* L129: */
    }

    if (ido < l1) {
        goto L132;
    }
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        i__2 = ido;
        for (i__ = 1; i__ <= i__2; ++i__) {
            cc[i__ + (k * cc_dim2 + 1) * cc_dim1] = ch[i__ + (k + ch_dim2) * ch_dim1];
            /* L130: */
        }
        /* L131: */
    }
    goto L135;
L132:
    i__1 = ido;
    for (i__ = 1; i__ <= i__1; ++i__) {
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            cc[i__ + (k * cc_dim2 + 1) * cc_dim1] = ch[i__ + (k + ch_dim2) * ch_dim1];
            /* L133: */
        }
        /* L134: */
    }
L135:
    i__1 = ipph;
    for (j = 2; j <= i__1; ++j) {
        jc = ipp2 - j;
        j2 = j + j;
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            cc[ido + (j2 - 2 + k * cc_dim2) * cc_dim1] =
                ch[(k + j * ch_dim2) * ch_dim1 + 1];
            cc[(j2 - 1 + k * cc_dim2) * cc_dim1 + 1] =
                ch[(k + jc * ch_dim2) * ch_dim1 + 1];
            /* L136: */
        }
        /* L137: */
    }
    if (ido == 1) {
        return;
    }
    if (nbd < l1) {
        goto L141;
    }
    i__1 = ipph;
    for (j = 2; j <= i__1; ++j) {
        jc = ipp2 - j;
        j2 = j + j;
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            /* DIR$ IVDEP */
            i__3 = ido;
            for (i__ = 3; i__ <= i__3; i__ += 2) {
                ic = idp2 - i__;
                cc[i__ - 1 + (j2 - 1 + k * cc_dim2) * cc_dim1] =
                    ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1] +
                    ch[i__ - 1 + (k + jc * ch_dim2) * ch_dim1];
                cc[ic - 1 + (j2 - 2 + k * cc_dim2) * cc_dim1] =
                    ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1] -
                    ch[i__ - 1 + (k + jc * ch_dim2) * ch_dim1];
                cc[i__ + (j2 - 1 + k * cc_dim2) * cc_dim1] =
                    ch[i__ + (k + j * ch_dim2) * ch_dim1] +
                    ch[i__ + (k + jc * ch_dim2) * ch_dim1];
                cc[ic + (j2 - 2 + k * cc_dim2) * cc_dim1] =
                    ch[i__ + (k + jc * ch_dim2) * ch_dim1] -
                    ch[i__ + (k + j * ch_dim2) * ch_dim1];
                /* L138: */
            }
            /* L139: */
        }
        /* L140: */
    }
    return;
L141:
    i__1 = ipph;
    for (j = 2; j <= i__1; ++j) {
        jc = ipp2 - j;
        j2 = j + j;
        i__2 = ido;
        for (i__ = 3; i__ <= i__2; i__ += 2) {
            ic = idp2 - i__;
            i__3 = l1;
            for (k = 1; k <= i__3; ++k) {
                cc[i__ - 1 + (j2 - 1 + k * cc_dim2) * cc_dim1] =
                    ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1] +
                    ch[i__ - 1 + (k + jc * ch_dim2) * ch_dim1];
                cc[ic - 1 + (j2 - 2 + k * cc_dim2) * cc_dim1] =
                    ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1] -
                    ch[i__ - 1 + (k + jc * ch_dim2) * ch_dim1];
                cc[i__ + (j2 - 1 + k * cc_dim2) * cc_dim1] =
                    ch[i__ + (k + j * ch_dim2) * ch_dim1] +
                    ch[i__ + (k + jc * ch_dim2) * ch_dim1];
                cc[ic + (j2 - 2 + k * cc_dim2) * cc_dim1] =
                    ch[i__ + (k + jc * ch_dim2) * ch_dim1] -
                    ch[i__ + (k + j * ch_dim2) * ch_dim1];
                /* L142: */
            }
            /* L143: */
        }
        /* L144: */
    }
    return;
} /* radfg_ */

/* DECK RADB2 */
template <class R>
/* Subroutine */[[gnu::always_inline]] static inline void
radb2(int ido, int l1, const R *__restrict cc, R *__restrict ch, const R *__restrict wa1)
{
    /* System generated locals */
    int cc_dim1, cc_offset, ch_dim1, ch_dim2, ch_offset, i__1, i__2;

    /* Local variables */
    int i__, k, ic;
    R ti2, tr2;
    int idp2;

    /* ***BEGIN PROLOGUE  RADB2 */
    /* ***SUBSIDIARY */
    /* ***PURPOSE  Calculate the fast Fourier transform of subvectors of */
    /*            length two. */
    /* ***LIBRARY   SLATEC (FFTPACK) */
    /* ***TYPE      SINGLE PRECISION (RADB2-S) */
    /* ***AUTHOR  Swarztrauber, P. N., (NCAR) */
    /* ***ROUTINES CALLED  (NONE) */
    /* ***REVISION HISTORY  (YYMMDD) */
    /*   790601  DATE WRITTEN */
    /*   830401  Modified to use SLATEC library source file format. */
    /*   860115  Modified by Ron Boisvert to adhere to Fortran 77 by */
    /*           changing dummy array size declarations (1) to (*). */
    /*   881128  Modified by Dick Valent to meet prologue standards. */
    /*   890831  Modified array declarations.  (WRB) */
    /*   891214  Prologue converted to Version 4.0 format.  (BAB) */
    /*   900402  Added TYPE section.  (WRB) */
    /* ***END PROLOGUE  RADB2 */
    /* ***FIRST EXECUTABLE STATEMENT  RADB2 */
    /* Parameter adjustments */
    cc_dim1 = ido;
    cc_offset = 1 + cc_dim1 * 3;
    cc -= cc_offset;
    ch_dim1 = ido;
    ch_dim2 = l1;
    ch_offset = 1 + ch_dim1 * (1 + ch_dim2);
    ch -= ch_offset;
    --wa1;

    /* Function Body */
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        ch[(k + ch_dim2) * ch_dim1 + 1] =
            cc[((k << 1) + 1) * cc_dim1 + 1] + cc[ido + ((k << 1) + 2) * cc_dim1];
        ch[(k + (ch_dim2 << 1)) * ch_dim1 + 1] =
            cc[((k << 1) + 1) * cc_dim1 + 1] - cc[ido + ((k << 1) + 2) * cc_dim1];
        /* L101: */
    }
    if ((i__1 = ido - 2) < 0) {
        goto L107;
    }
    else if (i__1 == 0) {
        goto L105;
    }
    else {
        goto L102;
    }
L102:
    idp2 = ido + 2;
    if ((ido - 1) / 2 < l1) {
        goto L108;
    }
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        /* DIR$ IVDEP */
        i__2 = ido;
        for (i__ = 3; i__ <= i__2; i__ += 2) {
            ic = idp2 - i__;
            ch[i__ - 1 + (k + ch_dim2) * ch_dim1] =
                cc[i__ - 1 + ((k << 1) + 1) * cc_dim1] +
                cc[ic - 1 + ((k << 1) + 2) * cc_dim1];
            tr2 = cc[i__ - 1 + ((k << 1) + 1) * cc_dim1] -
                  cc[ic - 1 + ((k << 1) + 2) * cc_dim1];
            ch[i__ + (k + ch_dim2) * ch_dim1] =
                cc[i__ + ((k << 1) + 1) * cc_dim1] - cc[ic + ((k << 1) + 2) * cc_dim1];
            ti2 = cc[i__ + ((k << 1) + 1) * cc_dim1] + cc[ic + ((k << 1) + 2) * cc_dim1];
            ch[i__ - 1 + (k + (ch_dim2 << 1)) * ch_dim1] =
                wa1[i__ - 2] * tr2 - wa1[i__ - 1] * ti2;
            ch[i__ + (k + (ch_dim2 << 1)) * ch_dim1] =
                wa1[i__ - 2] * ti2 + wa1[i__ - 1] * tr2;
            /* L103: */
        }
        /* L104: */
    }
    goto L111;
L108:
    i__1 = ido;
    for (i__ = 3; i__ <= i__1; i__ += 2) {
        ic = idp2 - i__;
        /* DIR$ IVDEP */
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            ch[i__ - 1 + (k + ch_dim2) * ch_dim1] =
                cc[i__ - 1 + ((k << 1) + 1) * cc_dim1] +
                cc[ic - 1 + ((k << 1) + 2) * cc_dim1];
            tr2 = cc[i__ - 1 + ((k << 1) + 1) * cc_dim1] -
                  cc[ic - 1 + ((k << 1) + 2) * cc_dim1];
            ch[i__ + (k + ch_dim2) * ch_dim1] =
                cc[i__ + ((k << 1) + 1) * cc_dim1] - cc[ic + ((k << 1) + 2) * cc_dim1];
            ti2 = cc[i__ + ((k << 1) + 1) * cc_dim1] + cc[ic + ((k << 1) + 2) * cc_dim1];
            ch[i__ - 1 + (k + (ch_dim2 << 1)) * ch_dim1] =
                wa1[i__ - 2] * tr2 - wa1[i__ - 1] * ti2;
            ch[i__ + (k + (ch_dim2 << 1)) * ch_dim1] =
                wa1[i__ - 2] * ti2 + wa1[i__ - 1] * tr2;
            /* L109: */
        }
        /* L110: */
    }
L111:
    if (ido % 2 == 1) {
        return;
    }
L105:
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        ch[ido + (k + ch_dim2) * ch_dim1] =
            cc[ido + ((k << 1) + 1) * cc_dim1] + cc[ido + ((k << 1) + 1) * cc_dim1];
        ch[ido + (k + (ch_dim2 << 1)) * ch_dim1] =
            -(cc[((k << 1) + 2) * cc_dim1 + 1] + cc[((k << 1) + 2) * cc_dim1 + 1]);
        /* L106: */
    }
L107:
    return;
} /* radb2_ */

/* DECK RADB3 */
template <class R>
/* Subroutine */[[gnu::always_inline]] static inline void
radb3(int ido, int l1, const R *__restrict cc, R *__restrict ch,
      const R *__restrict wa1, const R *__restrict wa2)
{
    /* System generated locals */
    int cc_dim1, cc_offset, ch_dim1, ch_dim2, ch_offset, i__1, i__2;

    /* Local variables */
    int i__, k, ic;
    R ci2, ci3, di2, di3, cr2, cr3, dr2, dr3, ti2, tr2;
    int idp2;
    R taui, taur;

    /* ***BEGIN PROLOGUE  RADB3 */
    /* ***SUBSIDIARY */
    /* ***PURPOSE  Calculate the fast Fourier transform of subvectors of */
    /*            length three. */
    /* ***LIBRARY   SLATEC (FFTPACK) */
    /* ***TYPE      SINGLE PRECISION (RADB3-S) */
    /* ***AUTHOR  Swarztrauber, P. N., (NCAR) */
    /* ***ROUTINES CALLED  (NONE) */
    /* ***REVISION HISTORY  (YYMMDD) */
    /*   790601  DATE WRITTEN */
    /*   830401  Modified to use SLATEC library source file format. */
    /*   860115  Modified by Ron Boisvert to adhere to Fortran 77 by */
    /*           (a) changing dummy array size declarations (1) to (*), */
    /*           (b) changing definition of variable TAUI by using */
    /*               FORTRAN intrinsic function SQRT instead of a DATA */
    /*               statement. */
    /*   881128  Modified by Dick Valent to meet prologue standards. */
    /*   890831  Modified array declarations.  (WRB) */
    /*   891214  Prologue converted to Version 4.0 format.  (BAB) */
    /*   900402  Added TYPE section.  (WRB) */
    /* ***END PROLOGUE  RADB3 */
    /* ***FIRST EXECUTABLE STATEMENT  RADB3 */
    /* Parameter adjustments */
    cc_dim1 = ido;
    cc_offset = 1 + (cc_dim1 << 2);
    cc -= cc_offset;
    ch_dim1 = ido;
    ch_dim2 = l1;
    ch_offset = 1 + ch_dim1 * (1 + ch_dim2);
    ch -= ch_offset;
    --wa1;
    --wa2;

    /* Function Body */
    taur = R(-.5);
    taui = std::sqrt(R(3.)) * R(.5);
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        tr2 = cc[ido + (k * 3 + 2) * cc_dim1] + cc[ido + (k * 3 + 2) * cc_dim1];
        cr2 = cc[(k * 3 + 1) * cc_dim1 + 1] + taur * tr2;
        ch[(k + ch_dim2) * ch_dim1 + 1] = cc[(k * 3 + 1) * cc_dim1 + 1] + tr2;
        ci3 = taui * (cc[(k * 3 + 3) * cc_dim1 + 1] + cc[(k * 3 + 3) * cc_dim1 + 1]);
        ch[(k + (ch_dim2 << 1)) * ch_dim1 + 1] = cr2 - ci3;
        ch[(k + ch_dim2 * 3) * ch_dim1 + 1] = cr2 + ci3;
        /* L101: */
    }
    if (ido == 1) {
        return;
    }
    idp2 = ido + 2;
    if ((ido - 1) / 2 < l1) {
        goto L104;
    }
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        /* DIR$ IVDEP */
        i__2 = ido;
        for (i__ = 3; i__ <= i__2; i__ += 2) {
            ic = idp2 - i__;
            tr2 = cc[i__ - 1 + (k * 3 + 3) * cc_dim1] + cc[ic - 1 + (k * 3 + 2) * cc_dim1];
            cr2 = cc[i__ - 1 + (k * 3 + 1) * cc_dim1] + taur * tr2;
            ch[i__ - 1 + (k + ch_dim2) * ch_dim1] =
                cc[i__ - 1 + (k * 3 + 1) * cc_dim1] + tr2;
            ti2 = cc[i__ + (k * 3 + 3) * cc_dim1] - cc[ic + (k * 3 + 2) * cc_dim1];
            ci2 = cc[i__ + (k * 3 + 1) * cc_dim1] + taur * ti2;
            ch[i__ + (k + ch_dim2) * ch_dim1] = cc[i__ + (k * 3 + 1) * cc_dim1] + ti2;
            cr3 = taui * (cc[i__ - 1 + (k * 3 + 3) * cc_dim1] -
                          cc[ic - 1 + (k * 3 + 2) * cc_dim1]);
            ci3 = taui * (cc[i__ + (k * 3 + 3) * cc_dim1] + cc[ic + (k * 3 + 2) * cc_dim1]);
            dr2 = cr2 - ci3;
            dr3 = cr2 + ci3;
            di2 = ci2 + cr3;
            di3 = ci2 - cr3;
            ch[i__ - 1 + (k + (ch_dim2 << 1)) * ch_dim1] =
                wa1[i__ - 2] * dr2 - wa1[i__ - 1] * di2;
            ch[i__ + (k + (ch_dim2 << 1)) * ch_dim1] =
                wa1[i__ - 2] * di2 + wa1[i__ - 1] * dr2;
            ch[i__ - 1 + (k + ch_dim2 * 3) * ch_dim1] =
                wa2[i__ - 2] * dr3 - wa2[i__ - 1] * di3;
            ch[i__ + (k + ch_dim2 * 3) * ch_dim1] =
                wa2[i__ - 2] * di3 + wa2[i__ - 1] * dr3;
            /* L102: */
        }
        /* L103: */
    }
    return;
L104:
    i__1 = ido;
    for (i__ = 3; i__ <= i__1; i__ += 2) {
        ic = idp2 - i__;
        /* DIR$ IVDEP */
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            tr2 = cc[i__ - 1 + (k * 3 + 3) * cc_dim1] + cc[ic - 1 + (k * 3 + 2) * cc_dim1];
            cr2 = cc[i__ - 1 + (k * 3 + 1) * cc_dim1] + taur * tr2;
            ch[i__ - 1 + (k + ch_dim2) * ch_dim1] =
                cc[i__ - 1 + (k * 3 + 1) * cc_dim1] + tr2;
            ti2 = cc[i__ + (k * 3 + 3) * cc_dim1] - cc[ic + (k * 3 + 2) * cc_dim1];
            ci2 = cc[i__ + (k * 3 + 1) * cc_dim1] + taur * ti2;
            ch[i__ + (k + ch_dim2) * ch_dim1] = cc[i__ + (k * 3 + 1) * cc_dim1] + ti2;
            cr3 = taui * (cc[i__ - 1 + (k * 3 + 3) * cc_dim1] -
                          cc[ic - 1 + (k * 3 + 2) * cc_dim1]);
            ci3 = taui * (cc[i__ + (k * 3 + 3) * cc_dim1] + cc[ic + (k * 3 + 2) * cc_dim1]);
            dr2 = cr2 - ci3;
            dr3 = cr2 + ci3;
            di2 = ci2 + cr3;
            di3 = ci2 - cr3;
            ch[i__ - 1 + (k + (ch_dim2 << 1)) * ch_dim1] =
                wa1[i__ - 2] * dr2 - wa1[i__ - 1] * di2;
            ch[i__ + (k + (ch_dim2 << 1)) * ch_dim1] =
                wa1[i__ - 2] * di2 + wa1[i__ - 1] * dr2;
            ch[i__ - 1 + (k + ch_dim2 * 3) * ch_dim1] =
                wa2[i__ - 2] * dr3 - wa2[i__ - 1] * di3;
            ch[i__ + (k + ch_dim2 * 3) * ch_dim1] =
                wa2[i__ - 2] * di3 + wa2[i__ - 1] * dr3;
            /* L105: */
        }
        /* L106: */
    }
    return;
} /* radb3_ */

/* DECK RADB4 */
template <class R>
/* Subroutine */[[gnu::always_inline]] static inline void
radb4(int ido, int l1, const R *__restrict cc, R *__restrict ch,
      const R *__restrict wa1, const R *__restrict wa2, const R *__restrict wa3)
{
    /* System generated locals */
    int cc_dim1, cc_offset, ch_dim1, ch_dim2, ch_offset, i__1, i__2;

    /* Local variables */
    int i__, k, ic;
    R ci2, ci3, ci4, cr2, cr3, cr4, ti1, ti2, ti3, ti4, tr1, tr2, tr3, tr4;
    int idp2;
    R sqrt2;

    /* ***BEGIN PROLOGUE  RADB4 */
    /* ***SUBSIDIARY */
    /* ***PURPOSE  Calculate the fast Fourier transform of subvectors of */
    /*            length four. */
    /* ***LIBRARY   SLATEC (FFTPACK) */
    /* ***TYPE      SINGLE PRECISION (RADB4-S) */
    /* ***AUTHOR  Swarztrauber, P. N., (NCAR) */
    /* ***ROUTINES CALLED  (NONE) */
    /* ***REVISION HISTORY  (YYMMDD) */
    /*   790601  DATE WRITTEN */
    /*   830401  Modified to use SLATEC library source file format. */
    /*   860115  Modified by Ron Boisvert to adhere to Fortran 77 by */
    /*           (a) changing dummy array size declarations (1) to (*), */
    /*           (b) changing definition of variable SQRT2 by using */
    /*               FORTRAN intrinsic function SQRT instead of a DATA */
    /*               statement. */
    /*   881128  Modified by Dick Valent to meet prologue standards. */
    /*   890831  Modified array declarations.  (WRB) */
    /*   891214  Prologue converted to Version 4.0 format.  (BAB) */
    /*   900402  Added TYPE section.  (WRB) */
    /* ***END PROLOGUE  RADB4 */
    /* ***FIRST EXECUTABLE STATEMENT  RADB4 */
    /* Parameter adjustments */
    cc_dim1 = ido;
    cc_offset = 1 + cc_dim1 * 5;
    cc -= cc_offset;
    ch_dim1 = ido;
    ch_dim2 = l1;
    ch_offset = 1 + ch_dim1 * (1 + ch_dim2);
    ch -= ch_offset;
    --wa1;
    --wa2;
    --wa3;

    /* Function Body */
    sqrt2 = std::sqrt(R(2.));
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        tr1 = cc[((k << 2) + 1) * cc_dim1 + 1] - cc[ido + ((k << 2) + 4) * cc_dim1];
        tr2 = cc[((k << 2) + 1) * cc_dim1 + 1] + cc[ido + ((k << 2) + 4) * cc_dim1];
        tr3 = cc[ido + ((k << 2) + 2) * cc_dim1] + cc[ido + ((k << 2) + 2) * cc_dim1];
        tr4 = cc[((k << 2) + 3) * cc_dim1 + 1] + cc[((k << 2) + 3) * cc_dim1 + 1];
        ch[(k + ch_dim2) * ch_dim1 + 1] = tr2 + tr3;
        ch[(k + (ch_dim2 << 1)) * ch_dim1 + 1] = tr1 - tr4;
        ch[(k + ch_dim2 * 3) * ch_dim1 + 1] = tr2 - tr3;
        ch[(k + (ch_dim2 << 2)) * ch_dim1 + 1] = tr1 + tr4;
        /* L101: */
    }
    if ((i__1 = ido - 2) < 0) {
        goto L107;
    }
    else if (i__1 == 0) {
        goto L105;
    }
    else {
        goto L102;
    }
L102:
    idp2 = ido + 2;
    if ((ido - 1) / 2 < l1) {
        goto L108;
    }
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        /* DIR$ IVDEP */
        i__2 = ido;
        for (i__ = 3; i__ <= i__2; i__ += 2) {
            ic = idp2 - i__;
            ti1 = cc[i__ + ((k << 2) + 1) * cc_dim1] + cc[ic + ((k << 2) + 4) * cc_dim1];
            ti2 = cc[i__ + ((k << 2) + 1) * cc_dim1] - cc[ic + ((k << 2) + 4) * cc_dim1];
            ti3 = cc[i__ + ((k << 2) + 3) * cc_dim1] - cc[ic + ((k << 2) + 2) * cc_dim1];
            tr4 = cc[i__ + ((k << 2) + 3) * cc_dim1] + cc[ic + ((k << 2) + 2) * cc_dim1];
            tr1 = cc[i__ - 1 + ((k << 2) + 1) * cc_dim1] -
                  cc[ic - 1 + ((k << 2) + 4) * cc_dim1];
            tr2 = cc[i__ - 1 + ((k << 2) + 1) * cc_dim1] +
                  cc[ic - 1 + ((k << 2) + 4) * cc_dim1];
            ti4 = cc[i__ - 1 + ((k << 2) + 3) * cc_dim1] -
                  cc[ic - 1 + ((k << 2) + 2) * cc_dim1];
            tr3 = cc[i__ - 1 + ((k << 2) + 3) * cc_dim1] +
                  cc[ic - 1 + ((k << 2) + 2) * cc_dim1];
            ch[i__ - 1 + (k + ch_dim2) * ch_dim1] = tr2 + tr3;
            cr3 = tr2 - tr3;
            ch[i__ + (k + ch_dim2) * ch_dim1] = ti2 + ti3;
            ci3 = ti2 - ti3;
            cr2 = tr1 - tr4;
            cr4 = tr1 + tr4;
            ci2 = ti1 + ti4;
            ci4 = ti1 - ti4;
            ch[i__ - 1 + (k + (ch_dim2 << 1)) * ch_dim1] =
                wa1[i__ - 2] * cr2 - wa1[i__ - 1] * ci2;
            ch[i__ + (k + (ch_dim2 << 1)) * ch_dim1] =
                wa1[i__ - 2] * ci2 + wa1[i__ - 1] * cr2;
            ch[i__ - 1 + (k + ch_dim2 * 3) * ch_dim1] =
                wa2[i__ - 2] * cr3 - wa2[i__ - 1] * ci3;
            ch[i__ + (k + ch_dim2 * 3) * ch_dim1] =
                wa2[i__ - 2] * ci3 + wa2[i__ - 1] * cr3;
            ch[i__ - 1 + (k + (ch_dim2 << 2)) * ch_dim1] =
                wa3[i__ - 2] * cr4 - wa3[i__ - 1] * ci4;
            ch[i__ + (k + (ch_dim2 << 2)) * ch_dim1] =
                wa3[i__ - 2] * ci4 + wa3[i__ - 1] * cr4;
            /* L103: */
        }
        /* L104: */
    }
    goto L111;
L108:
    i__1 = ido;
    for (i__ = 3; i__ <= i__1; i__ += 2) {
        ic = idp2 - i__;
        /* DIR$ IVDEP */
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            ti1 = cc[i__ + ((k << 2) + 1) * cc_dim1] + cc[ic + ((k << 2) + 4) * cc_dim1];
            ti2 = cc[i__ + ((k << 2) + 1) * cc_dim1] - cc[ic + ((k << 2) + 4) * cc_dim1];
            ti3 = cc[i__ + ((k << 2) + 3) * cc_dim1] - cc[ic + ((k << 2) + 2) * cc_dim1];
            tr4 = cc[i__ + ((k << 2) + 3) * cc_dim1] + cc[ic + ((k << 2) + 2) * cc_dim1];
            tr1 = cc[i__ - 1 + ((k << 2) + 1) * cc_dim1] -
                  cc[ic - 1 + ((k << 2) + 4) * cc_dim1];
            tr2 = cc[i__ - 1 + ((k << 2) + 1) * cc_dim1] +
                  cc[ic - 1 + ((k << 2) + 4) * cc_dim1];
            ti4 = cc[i__ - 1 + ((k << 2) + 3) * cc_dim1] -
                  cc[ic - 1 + ((k << 2) + 2) * cc_dim1];
            tr3 = cc[i__ - 1 + ((k << 2) + 3) * cc_dim1] +
                  cc[ic - 1 + ((k << 2) + 2) * cc_dim1];
            ch[i__ - 1 + (k + ch_dim2) * ch_dim1] = tr2 + tr3;
            cr3 = tr2 - tr3;
            ch[i__ + (k + ch_dim2) * ch_dim1] = ti2 + ti3;
            ci3 = ti2 - ti3;
            cr2 = tr1 - tr4;
            cr4 = tr1 + tr4;
            ci2 = ti1 + ti4;
            ci4 = ti1 - ti4;
            ch[i__ - 1 + (k + (ch_dim2 << 1)) * ch_dim1] =
                wa1[i__ - 2] * cr2 - wa1[i__ - 1] * ci2;
            ch[i__ + (k + (ch_dim2 << 1)) * ch_dim1] =
                wa1[i__ - 2] * ci2 + wa1[i__ - 1] * cr2;
            ch[i__ - 1 + (k + ch_dim2 * 3) * ch_dim1] =
                wa2[i__ - 2] * cr3 - wa2[i__ - 1] * ci3;
            ch[i__ + (k + ch_dim2 * 3) * ch_dim1] =
                wa2[i__ - 2] * ci3 + wa2[i__ - 1] * cr3;
            ch[i__ - 1 + (k + (ch_dim2 << 2)) * ch_dim1] =
                wa3[i__ - 2] * cr4 - wa3[i__ - 1] * ci4;
            ch[i__ + (k + (ch_dim2 << 2)) * ch_dim1] =
                wa3[i__ - 2] * ci4 + wa3[i__ - 1] * cr4;
            /* L109: */
        }
        /* L110: */
    }
L111:
    if (ido % 2 == 1) {
        return;
    }
L105:
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        ti1 = cc[((k << 2) + 2) * cc_dim1 + 1] + cc[((k << 2) + 4) * cc_dim1 + 1];
        ti2 = cc[((k << 2) + 4) * cc_dim1 + 1] - cc[((k << 2) + 2) * cc_dim1 + 1];
        tr1 = cc[ido + ((k << 2) + 1) * cc_dim1] - cc[ido + ((k << 2) + 3) * cc_dim1];
        tr2 = cc[ido + ((k << 2) + 1) * cc_dim1] + cc[ido + ((k << 2) + 3) * cc_dim1];
        ch[ido + (k + ch_dim2) * ch_dim1] = tr2 + tr2;
        ch[ido + (k + (ch_dim2 << 1)) * ch_dim1] = sqrt2 * (tr1 - ti1);
        ch[ido + (k + ch_dim2 * 3) * ch_dim1] = ti2 + ti2;
        ch[ido + (k + (ch_dim2 << 2)) * ch_dim1] = -sqrt2 * (tr1 + ti1);
        /* L106: */
    }
L107:
    return;
} /* radb4_ */

/* DECK RADB5 */
template <class R>
/* Subroutine */[[gnu::always_inline]] static inline void
radb5(int ido, int l1, const R *__restrict cc, R *__restrict ch, const R *__restrict wa1,
      const R *__restrict wa2, const R *__restrict wa3, const R *__restrict wa4)
{
    /* System generated locals */
    int cc_dim1, cc_offset, ch_dim1, ch_dim2, ch_offset, i__1, i__2;

    /* Local variables */
    int i__, k, ic;
    R pi, ci2, ci3, ci4, ci5, di3, di4, di5, di2, cr2, cr3, cr5, cr4, ti2, ti3,
        ti4, ti5, dr3, dr4, dr5, dr2, tr2, tr3, tr4, tr5, ti11, ti12, tr11, tr12;
    int idp2;

    /* ***BEGIN PROLOGUE  RADB5 */
    /* ***SUBSIDIARY */
    /* ***PURPOSE  Calculate the fast Fourier transform of subvectors of */
    /*            length five. */
    /* ***LIBRARY   SLATEC (FFTPACK) */
    /* ***TYPE      SINGLE PRECISION (RADB5-S) */
    /* ***AUTHOR  Swarztrauber, P. N., (NCAR) */
    /* ***ROUTINES CALLED  (NONE) */
    /* ***REVISION HISTORY  (YYMMDD) */
    /*   790601  DATE WRITTEN */
    /*   830401  Modified to use SLATEC library source file format. */
    /*   860115  Modified by Ron Boisvert to adhere to Fortran 77 by */
    /*           (a) changing dummy array size declarations (1) to (*), */
    /*           (b) changing definition of variables PI, TI11, TI12, */
    /*               TR11, TR12 by using FORTRAN intrinsic functions ATAN */
    /*               and SIN instead of DATA statements. */
    /*   881128  Modified by Dick Valent to meet prologue standards. */
    /*   890831  Modified array declarations.  (WRB) */
    /*   891214  Prologue converted to Version 4.0 format.  (BAB) */
    /*   900402  Added TYPE section.  (WRB) */
    /* ***END PROLOGUE  RADB5 */
    /* ***FIRST EXECUTABLE STATEMENT  RADB5 */
    /* Parameter adjustments */
    cc_dim1 = ido;
    cc_offset = 1 + cc_dim1 * 6;
    cc -= cc_offset;
    ch_dim1 = ido;
    ch_dim2 = l1;
    ch_offset = 1 + ch_dim1 * (1 + ch_dim2);
    ch -= ch_offset;
    --wa1;
    --wa2;
    --wa3;
    --wa4;

    /* Function Body */
    pi = R(3.14159265358979323846L);
    tr11 = std::sin(pi * R(.1));
    ti11 = std::sin(pi * R(.4));
    tr12 = -std::sin(pi * R(.3));
    ti12 = std::sin(pi * R(.2));
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        ti5 = cc[(k * 5 + 3) * cc_dim1 + 1] + cc[(k * 5 + 3) * cc_dim1 + 1];
        ti4 = cc[(k * 5 + 5) * cc_dim1 + 1] + cc[(k * 5 + 5) * cc_dim1 + 1];
        tr2 = cc[ido + (k * 5 + 2) * cc_dim1] + cc[ido + (k * 5 + 2) * cc_dim1];
        tr3 = cc[ido + (k * 5 + 4) * cc_dim1] + cc[ido + (k * 5 + 4) * cc_dim1];
        ch[(k + ch_dim2) * ch_dim1 + 1] = cc[(k * 5 + 1) * cc_dim1 + 1] + tr2 + tr3;
        cr2 = cc[(k * 5 + 1) * cc_dim1 + 1] + tr11 * tr2 + tr12 * tr3;
        cr3 = cc[(k * 5 + 1) * cc_dim1 + 1] + tr12 * tr2 + tr11 * tr3;
        ci5 = ti11 * ti5 + ti12 * ti4;
        ci4 = ti12 * ti5 - ti11 * ti4;
        ch[(k + (ch_dim2 << 1)) * ch_dim1 + 1] = cr2 - ci5;
        ch[(k + ch_dim2 * 3) * ch_dim1 + 1] = cr3 - ci4;
        ch[(k + (ch_dim2 << 2)) * ch_dim1 + 1] = cr3 + ci4;
        ch[(k + ch_dim2 * 5) * ch_dim1 + 1] = cr2 + ci5;
        /* L101: */
    }
    if (ido == 1) {
        return;
    }
    idp2 = ido + 2;
    if ((ido - 1) / 2 < l1) {
        goto L104;
    }
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        /* DIR$ IVDEP */
        i__2 = ido;
        for (i__ = 3; i__ <= i__2; i__ += 2) {
            ic = idp2 - i__;
            ti5 = cc[i__ + (k * 5 + 3) * cc_dim1] + cc[ic + (k * 5 + 2) * cc_dim1];
            ti2 = cc[i__ + (k * 5 + 3) * cc_dim1] - cc[ic + (k * 5 + 2) * cc_dim1];
            ti4 = cc[i__ + (k * 5 + 5) * cc_dim1] + cc[ic + (k * 5 + 4) * cc_dim1];
            ti3 = cc[i__ + (k * 5 + 5) * cc_dim1] - cc[ic + (k * 5 + 4) * cc_dim1];
            tr5 = cc[i__ - 1 + (k * 5 + 3) * cc_dim1] - cc[ic - 1 + (k * 5 + 2) * cc_dim1];
            tr2 = cc[i__ - 1 + (k * 5 + 3) * cc_dim1] + cc[ic - 1 + (k * 5 + 2) * cc_dim1];
            tr4 = cc[i__ - 1 + (k * 5 + 5) * cc_dim1] - cc[ic - 1 + (k * 5 + 4) * cc_dim1];
            tr3 = cc[i__ - 1 + (k * 5 + 5) * cc_dim1] + cc[ic - 1 + (k * 5 + 4) * cc_dim1];
            ch[i__ - 1 + (k + ch_dim2) * ch_dim1] =
                cc[i__ - 1 + (k * 5 + 1) * cc_dim1] + tr2 + tr3;
            ch[i__ + (k + ch_dim2) * ch_dim1] = cc[i__ + (k * 5 + 1) * cc_dim1] + ti2 + ti3;
            cr2 = cc[i__ - 1 + (k * 5 + 1) * cc_dim1] + tr11 * tr2 + tr12 * tr3;
            ci2 = cc[i__ + (k * 5 + 1) * cc_dim1] + tr11 * ti2 + tr12 * ti3;
            cr3 = cc[i__ - 1 + (k * 5 + 1) * cc_dim1] + tr12 * tr2 + tr11 * tr3;
            ci3 = cc[i__ + (k * 5 + 1) * cc_dim1] + tr12 * ti2 + tr11 * ti3;
            cr5 = ti11 * tr5 + ti12 * tr4;
            ci5 = ti11 * ti5 + ti12 * ti4;
            cr4 = ti12 * tr5 - ti11 * tr4;
            ci4 = ti12 * ti5 - ti11 * ti4;
            dr3 = cr3 - ci4;
            dr4 = cr3 + ci4;
            di3 = ci3 + cr4;
            di4 = ci3 - cr4;
            dr5 = cr2 + ci5;
            dr2 = cr2 - ci5;
            di5 = ci2 - cr5;
            di2 = ci2 + cr5;
            ch[i__ - 1 + (k + (ch_dim2 << 1)) * ch_dim1] =
                wa1[i__ - 2] * dr2 - wa1[i__ - 1] * di2;
            ch[i__ + (k + (ch_dim2 << 1)) * ch_dim1] =
                wa1[i__ - 2] * di2 + wa1[i__ - 1] * dr2;
            ch[i__ - 1 + (k + ch_dim2 * 3) * ch_dim1] =
                wa2[i__ - 2] * dr3 - wa2[i__ - 1] * di3;
            ch[i__ + (k + ch_dim2 * 3) * ch_dim1] =
                wa2[i__ - 2] * di3 + wa2[i__ - 1] * dr3;
            ch[i__ - 1 + (k + (ch_dim2 << 2)) * ch_dim1] =
                wa3[i__ - 2] * dr4 - wa3[i__ - 1] * di4;
            ch[i__ + (k + (ch_dim2 << 2)) * ch_dim1] =
                wa3[i__ - 2] * di4 + wa3[i__ - 1] * dr4;
            ch[i__ - 1 + (k + ch_dim2 * 5) * ch_dim1] =
                wa4[i__ - 2] * dr5 - wa4[i__ - 1] * di5;
            ch[i__ + (k + ch_dim2 * 5) * ch_dim1] =
                wa4[i__ - 2] * di5 + wa4[i__ - 1] * dr5;
            /* L102: */
        }
        /* L103: */
    }
    return;
L104:
    i__1 = ido;
    for (i__ = 3; i__ <= i__1; i__ += 2) {
        ic = idp2 - i__;
        /* DIR$ IVDEP */
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            ti5 = cc[i__ + (k * 5 + 3) * cc_dim1] + cc[ic + (k * 5 + 2) * cc_dim1];
            ti2 = cc[i__ + (k * 5 + 3) * cc_dim1] - cc[ic + (k * 5 + 2) * cc_dim1];
            ti4 = cc[i__ + (k * 5 + 5) * cc_dim1] + cc[ic + (k * 5 + 4) * cc_dim1];
            ti3 = cc[i__ + (k * 5 + 5) * cc_dim1] - cc[ic + (k * 5 + 4) * cc_dim1];
            tr5 = cc[i__ - 1 + (k * 5 + 3) * cc_dim1] - cc[ic - 1 + (k * 5 + 2) * cc_dim1];
            tr2 = cc[i__ - 1 + (k * 5 + 3) * cc_dim1] + cc[ic - 1 + (k * 5 + 2) * cc_dim1];
            tr4 = cc[i__ - 1 + (k * 5 + 5) * cc_dim1] - cc[ic - 1 + (k * 5 + 4) * cc_dim1];
            tr3 = cc[i__ - 1 + (k * 5 + 5) * cc_dim1] + cc[ic - 1 + (k * 5 + 4) * cc_dim1];
            ch[i__ - 1 + (k + ch_dim2) * ch_dim1] =
                cc[i__ - 1 + (k * 5 + 1) * cc_dim1] + tr2 + tr3;
            ch[i__ + (k + ch_dim2) * ch_dim1] = cc[i__ + (k * 5 + 1) * cc_dim1] + ti2 + ti3;
            cr2 = cc[i__ - 1 + (k * 5 + 1) * cc_dim1] + tr11 * tr2 + tr12 * tr3;
            ci2 = cc[i__ + (k * 5 + 1) * cc_dim1] + tr11 * ti2 + tr12 * ti3;
            cr3 = cc[i__ - 1 + (k * 5 + 1) * cc_dim1] + tr12 * tr2 + tr11 * tr3;
            ci3 = cc[i__ + (k * 5 + 1) * cc_dim1] + tr12 * ti2 + tr11 * ti3;
            cr5 = ti11 * tr5 + ti12 * tr4;
            ci5 = ti11 * ti5 + ti12 * ti4;
            cr4 = ti12 * tr5 - ti11 * tr4;
            ci4 = ti12 * ti5 - ti11 * ti4;
            dr3 = cr3 - ci4;
            dr4 = cr3 + ci4;
            di3 = ci3 + cr4;
            di4 = ci3 - cr4;
            dr5 = cr2 + ci5;
            dr2 = cr2 - ci5;
            di5 = ci2 - cr5;
            di2 = ci2 + cr5;
            ch[i__ - 1 + (k + (ch_dim2 << 1)) * ch_dim1] =
                wa1[i__ - 2] * dr2 - wa1[i__ - 1] * di2;
            ch[i__ + (k + (ch_dim2 << 1)) * ch_dim1] =
                wa1[i__ - 2] * di2 + wa1[i__ - 1] * dr2;
            ch[i__ - 1 + (k + ch_dim2 * 3) * ch_dim1] =
                wa2[i__ - 2] * dr3 - wa2[i__ - 1] * di3;
            ch[i__ + (k + ch_dim2 * 3) * ch_dim1] =
                wa2[i__ - 2] * di3 + wa2[i__ - 1] * dr3;
            ch[i__ - 1 + (k + (ch_dim2 << 2)) * ch_dim1] =
                wa3[i__ - 2] * dr4 - wa3[i__ - 1] * di4;
            ch[i__ + (k + (ch_dim2 << 2)) * ch_dim1] =
                wa3[i__ - 2] * di4 + wa3[i__ - 1] * dr4;
            ch[i__ - 1 + (k + ch_dim2 * 5) * ch_dim1] =
                wa4[i__ - 2] * dr5 - wa4[i__ - 1] * di5;
            ch[i__ + (k + ch_dim2 * 5) * ch_dim1] =
                wa4[i__ - 2] * di5 + wa4[i__ - 1] * dr5;
            /* L105: */
        }
        /* L106: */
    }
    return;
} /* radb5_ */

/* DECK RADBG */
template <class R>
/* Subroutine */[[gnu::always_inline]] static inline void
radbg(int ido, int ip, int l1, int idl1, const R *__restrict cc, R *__restrict c1,
      R *__restrict c2, R *__restrict ch, R *__restrict ch2, const R *__restrict wa)
{
    /* System generated locals */
    int ch_dim1, ch_dim2, ch_offset, cc_dim1, cc_dim2, cc_offset, c1_dim1, c1_dim2,
        c1_offset, c2_dim1, c2_offset, ch2_dim1, ch2_offset, i__1, i__2, i__3;

    /* Local variables */
    int i__, j, k, l, j2, ic, jc, lc, ik, is;
    R dc2, ai1, ai2, ar1, ar2, ds2;
    int nbd;
    R dcp, arg, dsp, tpi, ar1h, ar2h;
    int idp2, ipp2, idij, ipph;

    /* ***BEGIN PROLOGUE  RADBG */
    /* ***SUBSIDIARY */
    /* ***PURPOSE  Calculate the fast Fourier transform of subvectors of */
    /*            arbitrary length. */
    /* ***LIBRARY   SLATEC (FFTPACK) */
    /* ***TYPE      SINGLE PRECISION (RADBG-S) */
    /* ***AUTHOR  Swarztrauber, P. N., (NCAR) */
    /* ***ROUTINES CALLED  (NONE) */
    /* ***REVISION HISTORY  (YYMMDD) */
    /*   790601  DATE WRITTEN */
    /*   830401  Modified to use SLATEC library source file format. */
    /*   860115  Modified by Ron Boisvert to adhere to Fortran 77 by */
    /*           (a) changing dummy array size declarations (1) to (*), */
    /*           (b) changing references to intrinsic function FLOAT */
    /*               to REAL, and */
    /*           (c) changing definition of variable TPI by using */
    /*               FORTRAN intrinsic function ATAN instead of a DATA */
    /*               statement. */
    /*   881128  Modified by Dick Valent to meet prologue standards. */
    /*   890531  Changed all specific intrinsics to generic.  (WRB) */
    /*   890831  Modified array declarations.  (WRB) */
    /*   891214  Prologue converted to Version 4.0 format.  (BAB) */
    /*   900402  Added TYPE section.  (WRB) */
    /* ***END PROLOGUE  RADBG */
    /* ***FIRST EXECUTABLE STATEMENT  RADBG */
    /* Parameter adjustments */
    cc_dim1 = ido;
    cc_dim2 = ip;
    cc_offset = 1 + cc_dim1 * (1 + cc_dim2);
    cc -= cc_offset;
    ch_dim1 = ido;
    ch_dim2 = l1;
    ch_offset = 1 + ch_dim1 * (1 + ch_dim2);
    ch -= ch_offset;
    c1_dim1 = ido;
    c1_dim2 = l1;
    c1_offset = 1 + c1_dim1 * (1 + c1_dim2);
    c1 -= c1_offset;
    ch2_dim1 = idl1;
    ch2_offset = 1 + ch2_dim1;
    ch2 -= ch2_offset;
    c2_dim1 = idl1;
    c2_offset = 1 + c2_dim1;
    c2 -= c2_offset;
    --wa;

    /* Function Body */
    tpi = R(6.28318530717958647693L);
    arg = tpi / ip;
    dcp = std::cos(arg);
    dsp = std::sin(arg);
    idp2 = ido + 2;
    nbd = (ido - 1) / 2;
    ipp2 = ip + 2;
    ipph = (ip + 1) / 2;
    if (ido < l1) {
        goto L103;
    }
    i__1 = l1;
    for (k = 1; k <= i__1; ++k) {
        i__2 = ido;
        for (i__ = 1; i__ <= i__2; ++i__) {
            ch[i__ + (k + ch_dim2) * ch_dim1] = cc[i__ + (k * cc_dim2 + 1) * cc_dim1];
            /* L101: */
        }
        /* L102: */
    }
    goto L106;
L103:
    i__1 = ido;
    for (i__ = 1; i__ <= i__1; ++i__) {
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            ch[i__ + (k + ch_dim2) * ch_dim1] = cc[i__ + (k * cc_dim2 + 1) * cc_dim1];
            /* L104: */
        }
        /* L105: */
    }
L106:
    i__1 = ipph;
    for (j = 2; j <= i__1; ++j) {
        jc = ipp2 - j;
        j2 = j + j;
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            ch[(k + j * ch_dim2) * ch_dim1 + 1] =
                cc[ido + (j2 - 2 + k * cc_dim2) * cc_dim1] +
                cc[ido + (j2 - 2 + k * cc_dim2) * cc_dim1];
            ch[(k + jc * ch_dim2) * ch_dim1 + 1] =
                cc[(j2 - 1 + k * cc_dim2) * cc_dim1 + 1] +
                cc[(j2 - 1 + k * cc_dim2) * cc_dim1 + 1];
            /* L107: */
        }
        /* L108: */
    }
    if (ido == 1) {
        goto L116;
    }
    if (nbd < l1) {
        goto L112;
    }
    i__1 = ipph;
    for (j = 2; j <= i__1; ++j) {
        jc = ipp2 - j;
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            /* DIR$ IVDEP */
            i__3 = ido;
            for (i__ = 3; i__ <= i__3; i__ += 2) {
                ic = idp2 - i__;
                ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1] =
                    cc[i__ - 1 + ((j << 1) - 1 + k * cc_dim2) * cc_dim1] +
                    cc[ic - 1 + ((j << 1) - 2 + k * cc_dim2) * cc_dim1];
                ch[i__ - 1 + (k + jc * ch_dim2) * ch_dim1] =
                    cc[i__ - 1 + ((j << 1) - 1 + k * cc_dim2) * cc_dim1] -
                    cc[ic - 1 + ((j << 1) - 2 + k * cc_dim2) * cc_dim1];
                ch[i__ + (k + j * ch_dim2) * ch_dim1] =
                    cc[i__ + ((j << 1) - 1 + k * cc_dim2) * cc_dim1] -
                    cc[ic + ((j << 1) - 2 + k * cc_dim2) * cc_dim1];
                ch[i__ + (k + jc * ch_dim2) * ch_dim1] =
                    cc[i__ + ((j << 1) - 1 + k * cc_dim2) * cc_dim1] +
                    cc[ic + ((j << 1) - 2 + k * cc_dim2) * cc_dim1];
                /* L109: */
            }
            /* L110: */
        }
        /* L111: */
    }
    goto L116;
L112:
    i__1 = ipph;
    for (j = 2; j <= i__1; ++j) {
        jc = ipp2 - j;
        /* DIR$ IVDEP */
        i__2 = ido;
        for (i__ = 3; i__ <= i__2; i__ += 2) {
            ic = idp2 - i__;
            i__3 = l1;
            for (k = 1; k <= i__3; ++k) {
                ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1] =
                    cc[i__ - 1 + ((j << 1) - 1 + k * cc_dim2) * cc_dim1] +
                    cc[ic - 1 + ((j << 1) - 2 + k * cc_dim2) * cc_dim1];
                ch[i__ - 1 + (k + jc * ch_dim2) * ch_dim1] =
                    cc[i__ - 1 + ((j << 1) - 1 + k * cc_dim2) * cc_dim1] -
                    cc[ic - 1 + ((j << 1) - 2 + k * cc_dim2) * cc_dim1];
                ch[i__ + (k + j * ch_dim2) * ch_dim1] =
                    cc[i__ + ((j << 1) - 1 + k * cc_dim2) * cc_dim1] -
                    cc[ic + ((j << 1) - 2 + k * cc_dim2) * cc_dim1];
                ch[i__ + (k + jc * ch_dim2) * ch_dim1] =
                    cc[i__ + ((j << 1) - 1 + k * cc_dim2) * cc_dim1] +
                    cc[ic + ((j << 1) - 2 + k * cc_dim2) * cc_dim1];
                /* L113: */
            }
            /* L114: */
        }
        /* L115: */
    }
L116:
    ar1 = R(1.);
    ai1 = R(0.);
    i__1 = ipph;
    for (l = 2; l <= i__1; ++l) {
        lc = ipp2 - l;
        ar1h = dcp * ar1 - dsp * ai1;
        ai1 = dcp * ai1 + dsp * ar1;
        ar1 = ar1h;
        i__2 = idl1;
        for (ik = 1; ik <= i__2; ++ik) {
            c2[ik + l * c2_dim1] = ch2[ik + ch2_dim1] + ar1 * ch2[ik + (ch2_dim1 << 1)];
            c2[ik + lc * c2_dim1] = ai1 * ch2[ik + ip * ch2_dim1];
            /* L117: */
        }
        dc2 = ar1;
        ds2 = ai1;
        ar2 = ar1;
        ai2 = ai1;
        i__2 = ipph;
        for (j = 3; j <= i__2; ++j) {
            jc = ipp2 - j;
            ar2h = dc2 * ar2 - ds2 * ai2;
            ai2 = dc2 * ai2 + ds2 * ar2;
            ar2 = ar2h;
            i__3 = idl1;
            for (ik = 1; ik <= i__3; ++ik) {
                c2[ik + l * c2_dim1] += ar2 * ch2[ik + j * ch2_dim1];
                c2[ik + lc * c2_dim1] += ai2 * ch2[ik + jc * ch2_dim1];
                /* L118: */
            }
            /* L119: */
        }
        /* L120: */
    }
    i__1 = ipph;
    for (j = 2; j <= i__1; ++j) {
        i__2 = idl1;
        for (ik = 1; ik <= i__2; ++ik) {
            ch2[ik + ch2_dim1] += ch2[ik + j * ch2_dim1];
            /* L121: */
        }
        /* L122: */
    }
    i__1 = ipph;
    for (j = 2; j <= i__1; ++j) {
        jc = ipp2 - j;
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            ch[(k + j * ch_dim2) * ch_dim1 + 1] =
                c1[(k + j * c1_dim2) * c1_dim1 + 1] - c1[(k + jc * c1_dim2) * c1_dim1 + 1];
            ch[(k + jc * ch_dim2) * ch_dim1 + 1] =
                c1[(k + j * c1_dim2) * c1_dim1 + 1] + c1[(k + jc * c1_dim2) * c1_dim1 + 1];
            /* L123: */
        }
        /* L124: */
    }
    if (ido == 1) {
        goto L132;
    }
    if (nbd < l1) {
        goto L128;
    }
    i__1 = ipph;
    for (j = 2; j <= i__1; ++j) {
        jc = ipp2 - j;
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            /* DIR$ IVDEP */
            i__3 = ido;
            for (i__ = 3; i__ <= i__3; i__ += 2) {
                ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1] =
                    c1[i__ - 1 + (k + j * c1_dim2) * c1_dim1] -
                    c1[i__ + (k + jc * c1_dim2) * c1_dim1];
                ch[i__ - 1 + (k + jc * ch_dim2) * ch_dim1] =
                    c1[i__ - 1 + (k + j * c1_dim2) * c1_dim1] +
                    c1[i__ + (k + jc * c1_dim2) * c1_dim1];
                ch[i__ + (k + j * ch_dim2) * ch_dim1] =
                    c1[i__ + (k + j * c1_dim2) * c1_dim1] +
                    c1[i__ - 1 + (k + jc * c1_dim2) * c1_dim1];
                ch[i__ + (k + jc * ch_dim2) * ch_dim1] =
                    c1[i__ + (k + j * c1_dim2) * c1_dim1] -
                    c1[i__ - 1 + (k + jc * c1_dim2) * c1_dim1];
                /* L125: */
            }
            /* L126: */
        }
        /* L127: */
    }
    goto L132;
L128:
    i__1 = ipph;
    for (j = 2; j <= i__1; ++j) {
        jc = ipp2 - j;
        i__2 = ido;
        for (i__ = 3; i__ <= i__2; i__ += 2) {
            i__3 = l1;
            for (k = 1; k <= i__3; ++k) {
                ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1] =
                    c1[i__ - 1 + (k + j * c1_dim2) * c1_dim1] -
                    c1[i__ + (k + jc * c1_dim2) * c1_dim1];
                ch[i__ - 1 + (k + jc * ch_dim2) * ch_dim1] =
                    c1[i__ - 1 + (k + j * c1_dim2) * c1_dim1] +
                    c1[i__ + (k + jc * c1_dim2) * c1_dim1];
                ch[i__ + (k + j * ch_dim2) * ch_dim1] =
                    c1[i__ + (k + j * c1_dim2) * c1_dim1] +
                    c1[i__ - 1 + (k + jc * c1_dim2) * c1_dim1];
                ch[i__ + (k + jc * ch_dim2) * ch_dim1] =
                    c1[i__ + (k + j * c1_dim2) * c1_dim1] -
                    c1[i__ - 1 + (k + jc * c1_dim2) * c1_dim1];
                /* L129: */
            }
            /* L130: */
        }
        /* L131: */
    }
L132:
    if (ido == 1) {
        return;
    }
    i__1 = idl1;
    for (ik = 1; ik <= i__1; ++ik) {
        c2[ik + c2_dim1] = ch2[ik + ch2_dim1];
        /* L133: */
    }
    i__1 = ip;
    for (j = 2; j <= i__1; ++j) {
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            c1[(k + j * c1_dim2) * c1_dim1 + 1] = ch[(k + j * ch_dim2) * ch_dim1 + 1];
            /* L134: */
        }
        /* L135: */
    }
    if (nbd > l1) {
        goto L139;
    }
    is = -(ido);
    i__1 = ip;
    for (j = 2; j <= i__1; ++j) {
        is += ido;
        idij = is;
        i__2 = ido;
        for (i__ = 3; i__ <= i__2; i__ += 2) {
            idij += 2;
            i__3 = l1;
            for (k = 1; k <= i__3; ++k) {
                c1[i__ - 1 + (k + j * c1_dim2) * c1_dim1] =
                    wa[idij - 1] * ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1] -
                    wa[idij] * ch[i__ + (k + j * ch_dim2) * ch_dim1];
                c1[i__ + (k + j * c1_dim2) * c1_dim1] =
                    wa[idij - 1] * ch[i__ + (k + j * ch_dim2) * ch_dim1] +
                    wa[idij] * ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1];
                /* L136: */
            }
            /* L137: */
        }
        /* L138: */
    }
    goto L143;
L139:
    is = -(ido);
    i__1 = ip;
    for (j = 2; j <= i__1; ++j) {
        is += ido;
        i__2 = l1;
        for (k = 1; k <= i__2; ++k) {
            idij = is;
            /* DIR$ IVDEP */
            i__3 = ido;
            for (i__ = 3; i__ <= i__3; i__ += 2) {
                idij += 2;
                c1[i__ - 1 + (k + j * c1_dim2) * c1_dim1] =
                    wa[idij - 1] * ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1] -
                    wa[idij] * ch[i__ + (k + j * ch_dim2) * ch_dim1];
                c1[i__ + (k + j * c1_dim2) * c1_dim1] =
                    wa[idij - 1] * ch[i__ + (k + j * ch_dim2) * ch_dim1] +
                    wa[idij] * ch[i__ - 1 + (k + j * ch_dim2) * ch_dim1];
                /* L140: */
            }
            /* L141: */
        }
        /* L142: */
    }
L143:
    return;
} /* radbg_ */

/* DECK RFFTI1 */
template <class R>
/* Subroutine */ void rffti1(int n, R *__restrict wa, int *__restrict ifac)
{
    /* Initialized data */

    int ntryh[4] = {4, 2, 3, 5};

    /* System generated locals */
    int i__1, i__2, i__3;

    /* Local variables */
    int i__, j, k1, l1, l2, ib;
    R fi;
    int ld, ii, nf, ip, nl, is, nq, nr;
    R arg;
    int ido, ipm;
    R tpi;
    int nfm1;
    R argh;
    int ntry;
    R argld;

    /* ***BEGIN PROLOGUE  RFFTI1 */
    /* ***PURPOSE  Initialize a real and an integer work array for RFFTF1 and */
    /*            RFFTB1. */
    /* ***LIBRARY   SLATEC (FFTPACK) */
    /* ***CATEGORY  J1A1 */
    /* ***TYPE      SINGLE PRECISION (RFFTI1-S, CFFTI1-C) */
    /* ***KEYWORDS  FFTPACK, FOURIER TRANSFORM */
    /* ***AUTHOR  Swarztrauber, P. N., (NCAR) */
    /* ***DESCRIPTION */

    /*   Subroutine RFFTI1 initializes the work arrays WA and IFAC which are */
    /*   used in both RFFTF1 and RFFTB1.  The prime factorization of N and a */
    /*   tabulation of the trigonometric functions are computed and stored in */
    /*   IFAC and WA, respectively. */

    /*   Input Argument */

    /*   N       the length of the sequence to be transformed. */

    /*   Output Arguments */

    /*   WA      a real work array which must be dimensioned at least N. */

    /*   IFAC    an integer work array which must be dimensioned at least 15. */

    /*   The same work arrays can be used for both RFFTF1 and RFFTB1 as long */
    /*   as N remains unchanged.  Different WA and IFAC arrays are required */
    /*   for different values of N.  The contents of WA and IFAC must not be */
    /*   changed between calls of RFFTF1 or RFFTB1. */

    /* ***REFERENCES  P. N. Swarztrauber, Vectorizing the FFTs, in Parallel */
    /*                 Computations (G. Rodrigue, ed.), Academic Press, */
    /*                 1982, pp. 51-83. */
    /* ***ROUTINES CALLED  (NONE) */
    /* ***REVISION HISTORY  (YYMMDD) */
    /*   790601  DATE WRITTEN */
    /*   830401  Modified to use SLATEC library source file format. */
    /*   860115  Modified by Ron Boisvert to adhere to Fortran 77 by */
    /*           (a) changing dummy array size declarations (1) to (*), */
    /*           (b) changing references to intrinsic function FLOAT */
    /*               to REAL, and */
    /*           (c) changing definition of variable TPI by using */
    /*               FORTRAN intrinsic functions instead of DATA */
    /*               statements. */
    /*   881128  Modified by Dick Valent to meet prologue standards. */
    /*   890531  Changed all specific intrinsics to generic.  (WRB) */
    /*   891214  Prologue converted to Version 4.0 format.  (BAB) */
    /*   900131  Routine changed from subsidiary to user-callable.  (WRB) */
    /*   920501  Reformatted the REFERENCES section.  (WRB) */
    /* ***END PROLOGUE  RFFTI1 */
    /* Parameter adjustments */
    --ifac;
    --wa;

    /* Function Body */
    /* ***FIRST EXECUTABLE STATEMENT  RFFTI1 */
    nl = n;
    nf = 0;
    j = 0;
L101:
    ++j;
    if (j - 4 <= 0) {
        goto L102;
    }
    else {
        goto L103;
    }
L102:
    ntry = ntryh[j - 1];
    goto L104;
L103:
    ntry += 2;
L104:
    nq = nl / ntry;
    nr = nl - ntry * nq;
    if (nr != 0) {
        goto L101;
    }
    else {
        goto L105;
    }
L105:
    ++nf;
    ifac[nf + 2] = ntry;
    nl = nq;
    if (ntry != 2) {
        goto L107;
    }
    if (nf == 1) {
        goto L107;
    }
    i__1 = nf;
    for (i__ = 2; i__ <= i__1; ++i__) {
        ib = nf - i__ + 2;
        ifac[ib + 2] = ifac[ib + 1];
        /* L106: */
    }
    ifac[3] = 2;
L107:
    if (nl != 1) {
        goto L104;
    }
    ifac[1] = n;
    ifac[2] = nf;
    tpi = R(6.28318530717958647693L);
    argh = tpi / n;
    is = 0;
    nfm1 = nf - 1;
    l1 = 1;
    if (nfm1 == 0) {
        return;
    }
    i__1 = nfm1;
    for (k1 = 1; k1 <= i__1; ++k1) {
        ip = ifac[k1 + 2];
        ld = 0;
        l2 = l1 * ip;
        ido = n / l2;
        ipm = ip - 1;
        i__2 = ipm;
        for (j = 1; j <= i__2; ++j) {
            ld += l1;
            i__ = is;
            argld = ld * argh;
            fi = R(0.);
            i__3 = ido;
            for (ii = 3; ii <= i__3; ii += 2) {
                i__ += 2;
                fi += R(1.);
                arg = fi * argld;
                wa[i__ - 1] = std::cos(arg);
                wa[i__] = std::sin(arg);
                /* L108: */
            }
            is += ido;
            /* L109: */
        }
        l1 = l2;
        /* L110: */
    }
    return;
} /* rffti1_ */

/* DECK RFFTF1 */
template <class R>
/* Subroutine */ void rfftf1(int n, R *__restrict c__, R *__restrict ch,
                             const R *__restrict wa, const int *__restrict ifac)
{
    /* System generated locals */
    int i__1;

    /* Local variables */
    int i__, k1, l1, l2, na, kh, nf, ip, iw, ix2, ix3, ix4, ido, idl1;

    /* ***BEGIN PROLOGUE  RFFTF1 */
    /* ***PURPOSE  Compute the forward transform of a real, periodic sequence. */
    /* ***LIBRARY   SLATEC (FFTPACK) */
    /* ***CATEGORY  J1A1 */
    /* ***TYPE      SINGLE PRECISION (RFFTF1-S, CFFTF1-C) */
    /* ***KEYWORDS  FFTPACK, FOURIER TRANSFORM */
    /* ***AUTHOR  Swarztrauber, P. N., (NCAR) */
    /* ***DESCRIPTION */

    /*   Subroutine RFFTF1 computes the Fourier coefficients of a real */
    /*   periodic sequence (Fourier analysis).  The transform is defined */
    /*   below at output parameter C. */

    /*   The arrays WA and IFAC which are used by subroutine RFFTB1 must be */
    /*   initialized by calling subroutine RFFTI1. */

    /*   Input Arguments */

    /*   N       the length of the array R to be transformed.  The method */
    /*           is most efficient when N is a product of small primes. */
    /*           N may change so long as different work arrays are provided. */

    /*   C       a real array of length N which contains the sequence */
    /*           to be transformed. */

    /*   CH      a real work array of length at least N. */

    /*   WA      a real work array which must be dimensioned at least N. */

    /*   IFAC    an integer work array which must be dimensioned at least 15. */

    /*           The WA and IFAC arrays must be initialized by calling */
    /*           subroutine RFFTI1, and different WA and IFAC arrays must be */
    /*           used for each different value of N.  This initialization */
    /*           does not have to be repeated so long as N remains unchanged. */
    /*           Thus subsequent transforms can be obtained faster than the */
    /*           first.  The same WA and IFAC arrays can be used by RFFTF1 */
    /*           and RFFTB1. */

    /*   Output Argument */

    /*   C       C(1) = the sum from I=1 to I=N of R(I) */

    /*           If N is even set L = N/2; if N is odd set L = (N+1)/2 */

    /*             then for K = 2,...,L */

    /*                C(2*K-2) = the sum from I = 1 to I = N of */

    /*                     C(I)*COS((K-1)*(I-1)*2*PI/N) */

    /*                C(2*K-1) = the sum from I = 1 to I = N of */

    /*                    -C(I)*SIN((K-1)*(I-1)*2*PI/N) */

    /*           If N is even */

    /*                C(N) = the sum from I = 1 to I = N of */

    /*                     (-1)**(I-1)*C(I) */

    /*   Notes:  This transform is unnormalized since a call of RFFTF1 */
    /*           followed by a call of RFFTB1 will multiply the input */
    /*           sequence by N. */

    /*           WA and IFAC contain initialization calculations which must */
    /*           not be destroyed between calls of subroutine RFFTF1 or */
    /*           RFFTB1. */

    /* ***REFERENCES  P. N. Swarztrauber, Vectorizing the FFTs, in Parallel */
    /*                 Computations (G. Rodrigue, ed.), Academic Press, */
    /*                 1982, pp. 51-83. */
    /* ***ROUTINES CALLED  RADF2, RADF3, RADF4, RADF5, RADFG */
    /* ***REVISION HISTORY  (YYMMDD) */
    /*   790601  DATE WRITTEN */
    /*   830401  Modified to use SLATEC library source file format. */
    /*   860115  Modified by Ron Boisvert to adhere to Fortran 77 by */
    /*           changing dummy array size declarations (1) to (*). */
    /*   881128  Modified by Dick Valent to meet prologue standards. */
    /*   891214  Prologue converted to Version 4.0 format.  (BAB) */
    /*   900131  Routine changed from subsidiary to user-callable.  (WRB) */
    /*   920501  Reformatted the REFERENCES section.  (WRB) */
    /* ***END PROLOGUE  RFFTF1 */
    /* ***FIRST EXECUTABLE STATEMENT  RFFTF1 */
    /* Parameter adjustments */
    --ifac;
    --wa;
    --ch;
    --c__;

    /* Function Body */
    nf = ifac[2];
    na = 1;
    l2 = n;
    iw = n;
    i__1 = nf;
    for (k1 = 1; k1 <= i__1; ++k1) {
        kh = nf - k1;
        ip = ifac[kh + 3];
        l1 = l2 / ip;
        ido = n / l2;
        idl1 = ido * l1;
        iw -= (ip - 1) * ido;
        na = 1 - na;
        if (ip != 4) {
            goto L102;
        }
        ix2 = iw + ido;
        ix3 = ix2 + ido;
        if (na != 0) {
            goto L101;
        }
        radf4(ido, l1, &c__[1], &ch[1], &wa[iw], &wa[ix2], &wa[ix3]);
        goto L110;
    L101:
        radf4(ido, l1, &ch[1], &c__[1], &wa[iw], &wa[ix2], &wa[ix3]);
        goto L110;
    L102:
        if (ip != 2) {
            goto L104;
        }
        if (na != 0) {
            goto L103;
        }
        radf2(ido, l1, &c__[1], &ch[1], &wa[iw]);
        goto L110;
    L103:
        radf2(ido, l1, &ch[1], &c__[1], &wa[iw]);
        goto L110;
    L104:
        if (ip != 3) {
            goto L106;
        }
        ix2 = iw + ido;
        if (na != 0) {
            goto L105;
        }
        radf3(ido, l1, &c__[1], &ch[1], &wa[iw], &wa[ix2]);
        goto L110;
    L105:
        radf3(ido, l1, &ch[1], &c__[1], &wa[iw], &wa[ix2]);
        goto L110;
    L106:
        if (ip != 5) {
            goto L108;
        }
        ix2 = iw + ido;
        ix3 = ix2 + ido;
        ix4 = ix3 + ido;
        if (na != 0) {
            goto L107;
        }
        radf5(ido, l1, &c__[1], &ch[1], &wa[iw], &wa[ix2], &wa[ix3], &wa[ix4]);
        goto L110;
    L107:
        radf5(ido, l1, &ch[1], &c__[1], &wa[iw], &wa[ix2], &wa[ix3], &wa[ix4]);
        goto L110;
    L108:
        if (ido == 1) {
            na = 1 - na;
        }
        if (na != 0) {
            goto L109;
        }
        radfg(ido, ip, l1, idl1, &c__[1], &c__[1], &c__[1], &ch[1], &ch[1], &wa[iw]);
        na = 1;
        goto L110;
    L109:
        radfg(ido, ip, l1, idl1, &ch[1], &ch[1], &ch[1], &c__[1], &c__[1], &wa[iw]);
        na = 0;
    L110:
        l2 = l1;
        /* L111: */
    }
    if (na == 1) {
        return;
    }
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
        c__[i__] = ch[i__];
        /* L112: */
    }
    return;
} /* rfftf1_ */

/* DECK RFFTB1 */
template <class R>
/* Subroutine */ void rfftb1(int n, R *__restrict c__, R *__restrict ch,
                             const R *__restrict wa, const int *__restrict ifac)
{
    /* System generated locals */
    int i__1;

    /* Local variables */
    int i__, k1, l1, l2, na, nf, ip, iw, ix2, ix3, ix4, ido, idl1;

    /* ***BEGIN PROLOGUE  RFFTB1 */
    /* ***PURPOSE  Compute the backward fast Fourier transform of a real */
    /*            coefficient array. */
    /* ***LIBRARY   SLATEC (FFTPACK) */
    /* ***CATEGORY  J1A1 */
    /* ***TYPE      SINGLE PRECISION (RFFTB1-S, CFFTB1-C) */
    /* ***KEYWORDS  FFTPACK, FOURIER TRANSFORM */
    /* ***AUTHOR  Swarztrauber, P. N., (NCAR) */
    /* ***DESCRIPTION */

    /*   Subroutine RFFTB1 computes the real periodic sequence from its */
    /*   Fourier coefficients (Fourier synthesis).  The transform is defined */
    /*   below at output parameter C. */

    /*   The arrays WA and IFAC which are used by subroutine RFFTB1 must be */
    /*   initialized by calling subroutine RFFTI1. */

    /*   Input Arguments */

    /*   N       the length of the array R to be transformed.  The method */
    /*           is most efficient when N is a product of small primes. */
    /*           N may change so long as different work arrays are provided. */

    /*   C       a real array of length N which contains the sequence */
    /*           to be transformed. */

    /*   CH      a real work array of length at least N. */

    /*   WA      a real work array which must be dimensioned at least N. */

    /*   IFAC    an integer work array which must be dimensioned at least 15. */

    /*           The WA and IFAC arrays must be initialized by calling */
    /*           subroutine RFFTI1, and different WA and IFAC arrays must be */
    /*           used for each different value of N.  This initialization */
    /*           does not have to be repeated so long as N remains unchanged. */
    /*           Thus subsequent transforms can be obtained faster than the */
    /*           first.  The same WA and IFAC arrays can be used by RFFTF1 */
    /*           and RFFTB1. */

    /*   Output Argument */

    /*   C       For N even and for I = 1,...,N */

    /*                C(I) = C(1)+(-1)**(I-1)*C(N) */

    /*                     plus the sum from K=2 to K=N/2 of */

    /*                      2.*C(2*K-2)*COS((K-1)*(I-1)*2*PI/N) */

    /*                     -2.*C(2*K-1)*SIN((K-1)*(I-1)*2*PI/N) */

    /*           For N odd and for I = 1,...,N */

    /*                C(I) = C(1) plus the sum from K=2 to K=(N+1)/2 of */

    /*                     2.*C(2*K-2)*COS((K-1)*(I-1)*2*PI/N) */

    /*                    -2.*C(2*K-1)*SIN((K-1)*(I-1)*2*PI/N) */

    /*   Notes:  This transform is unnormalized since a call of RFFTF1 */
    /*           followed by a call of RFFTB1 will multiply the input */
    /*           sequence by N. */

    /*           WA and IFAC contain initialization calculations which must */
    /*           not be destroyed between calls of subroutine RFFTF1 or */
    /*           RFFTB1. */

    /* ***REFERENCES  P. N. Swarztrauber, Vectorizing the FFTs, in Parallel */
    /*                 Computations (G. Rodrigue, ed.), Academic Press, */
    /*                 1982, pp. 51-83. */
    /* ***ROUTINES CALLED  RADB2, RADB3, RADB4, RADB5, RADBG */
    /* ***REVISION HISTORY  (YYMMDD) */
    /*   790601  DATE WRITTEN */
    /*   830401  Modified to use SLATEC library source file format. */
    /*   860115  Modified by Ron Boisvert to adhere to Fortran 77 by */
    /*           changing dummy array size declarations (1) to (*). */
    /*   881128  Modified by Dick Valent to meet prologue standards. */
    /*   891214  Prologue converted to Version 4.0 format.  (BAB) */
    /*   900131  Routine changed from subsidiary to user-callable.  (WRB) */
    /*   920501  Reformatted the REFERENCES section.  (WRB) */
    /* ***END PROLOGUE  RFFTB1 */
    /* ***FIRST EXECUTABLE STATEMENT  RFFTB1 */
    /* Parameter adjustments */
    --ifac;
    --wa;
    --ch;
    --c__;

    /* Function Body */
    nf = ifac[2];
    na = 0;
    l1 = 1;
    iw = 1;
    i__1 = nf;
    for (k1 = 1; k1 <= i__1; ++k1) {
        ip = ifac[k1 + 2];
        l2 = ip * l1;
        ido = n / l2;
        idl1 = ido * l1;
        if (ip != 4) {
            goto L103;
        }
        ix2 = iw + ido;
        ix3 = ix2 + ido;
        if (na != 0) {
            goto L101;
        }
        radb4(ido, l1, &c__[1], &ch[1], &wa[iw], &wa[ix2], &wa[ix3]);
        goto L102;
    L101:
        radb4(ido, l1, &ch[1], &c__[1], &wa[iw], &wa[ix2], &wa[ix3]);
    L102:
        na = 1 - na;
        goto L115;
    L103:
        if (ip != 2) {
            goto L106;
        }
        if (na != 0) {
            goto L104;
        }
        radb2(ido, l1, &c__[1], &ch[1], &wa[iw]);
        goto L105;
    L104:
        radb2(ido, l1, &ch[1], &c__[1], &wa[iw]);
    L105:
        na = 1 - na;
        goto L115;
    L106:
        if (ip != 3) {
            goto L109;
        }
        ix2 = iw + ido;
        if (na != 0) {
            goto L107;
        }
        radb3(ido, l1, &c__[1], &ch[1], &wa[iw], &wa[ix2]);
        goto L108;
    L107:
        radb3(ido, l1, &ch[1], &c__[1], &wa[iw], &wa[ix2]);
    L108:
        na = 1 - na;
        goto L115;
    L109:
        if (ip != 5) {
            goto L112;
        }
        ix2 = iw + ido;
        ix3 = ix2 + ido;
        ix4 = ix3 + ido;
        if (na != 0) {
            goto L110;
        }
        radb5(ido, l1, &c__[1], &ch[1], &wa[iw], &wa[ix2], &wa[ix3], &wa[ix4]);
        goto L111;
    L110:
        radb5(ido, l1, &ch[1], &c__[1], &wa[iw], &wa[ix2], &wa[ix3], &wa[ix4]);
    L111:
        na = 1 - na;
        goto L115;
    L112:
        if (na != 0) {
            goto L113;
        }
        radbg(ido, ip, l1, idl1, &c__[1], &c__[1], &c__[1], &ch[1], &ch[1], &wa[iw]);
        goto L114;
    L113:
        radbg(ido, ip, l1, idl1, &ch[1], &ch[1], &ch[1], &c__[1], &c__[1], &wa[iw]);
    L114:
        if (ido == 1) {
            na = 1 - na;
        }
    L115:
        l1 = l2;
        iw += (ip - 1) * ido;
        /* L116: */
    }
    if (na == 0) {
        return;
    }
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
        c__[i__] = ch[i__];
        /* L117: */
    }
    return;
} /* rfftb1_ */

#define IMPL_FFTPACK(T)                                                          \
    template void rffti1<T>(int n, T *wa, int *ifac);                            \
    template void rfftf1<T>(int n, T *c__, T *ch, const T *wa, const int *ifac); \
    template void rfftb1<T>(int n, T *c__, T *ch, const T *wa, const int *ifac);
IMPL_FFTPACK(float);
IMPL_FFTPACK(double);
IMPL_FFTPACK(long double);

}  // namespace fftpack
