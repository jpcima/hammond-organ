#pragma once

namespace fftpack {

template <class R> void rffti1(int n, R *wa, int *ifac);

template <class R>
void rfftf1(int n, R *c__, R *ch, const R *wa, const int *ifac);

template <class R>
void rfftb1(int n, R *c__, R *ch, const R *wa, const int *ifac);

}  // namespace fftpack
