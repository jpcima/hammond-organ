//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "trig.h"
#include <cmath>

namespace dsp {

template <class R>
FastSine<R>::FastSine(R phase, R step)
    : a_(std::cos(step))
    , b_(std::sin(step))
{
    s_ = std::sin(phase);
    c_ = std::cos(phase);
}

template <class R> void FastSine<R>::advance()
{
    const R s = s_, c = c_;
    s_ = b_ * c + a_ * s;
    c_ = a_ * c - b_ * s;
}

}  // namespace dsp
