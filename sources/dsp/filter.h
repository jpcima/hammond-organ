//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include <xsimd/xsimd.hpp>
#include <array>
#include <memory>

/* XXX: move it somewhere else */
namespace xsimd {
struct aligned_delete {
    void operator()(void *p)
    {
        xsimd::aligned_free(p);
    }
};
}  // namespace xsimd

namespace dsp {

//------------------------------------------------------------------------------
// FIR filter with compile-time length N
//------------------------------------------------------------------------------
template <unsigned N, class V = xsimd::simd_type<float>> struct FIR {
    static_assert(N >= 2, "invalid number of filter coefficients");

    typedef V vector_type;
    typedef typename V::value_type real_type;

    FIR();
    template <class Rc> void coefs(const Rc *coefs);
    void reset();
    void in(real_type x);
    real_type out() const;
    real_type tick(real_type x);

    FIR(FIR &&) = default;
    FIR &operator=(FIR &&) = default;

private:
    unsigned i_ = {};
    std::unique_ptr<real_type[]> x_;
    std::unique_ptr<real_type[], xsimd::aligned_delete> c_;

public:
    real_type impl_scalar() const;
    real_type impl_simd() const;
};

//------------------------------------------------------------------------------
// FIR filter with run-time length
//------------------------------------------------------------------------------
template <class V = xsimd::simd_type<float>> struct FIRg {
    typedef V vector_type;
    typedef typename V::value_type real_type;

    explicit FIRg(unsigned n);
    template <class Rc> void coefs(const Rc *coefs);
    void reset();
    void in(real_type x);
    real_type out() const;
    real_type tick(real_type x);

    FIRg(FIRg &&) = default;
    FIRg &operator=(FIRg &&) = default;

private:
    const unsigned n_{};
    unsigned i_{};
    std::unique_ptr<real_type[]> x_;
    std::unique_ptr<real_type[], xsimd::aligned_delete> c_;

public:
    real_type impl_scalar() const;
    real_type impl_simd() const;
};

//------------------------------------------------------------------------------
// IIR filter with compile-time length N
//------------------------------------------------------------------------------
template <unsigned N, class V = xsimd::simd_type<double>> struct IIR {
    static_assert(N >= 2, "invalid number of filter coefficients");

    typedef V vector_type;
    typedef typename V::value_type real_type;

    IIR();
    template <class Rc> void coefs(const Rc *bcf, const Rc *acf);
    void reset();
    real_type tick(real_type in);

    IIR(IIR &&) = default;
    IIR &operator=(IIR &&) = default;

private:
    unsigned i_ = {};
    std::unique_ptr<real_type[]> x_, y_;
    real_type b0_ = 1, a0_ = 1;
    std::unique_ptr<real_type[], xsimd::aligned_delete> b_, a_;

public:
    real_type impl_scalar(real_type in);
    real_type impl_simd(real_type in);
};

//------------------------------------------------------------------------------
// FIR filter with run-time length
//------------------------------------------------------------------------------
template <class V = xsimd::simd_type<double>> struct IIRg {
    typedef V vector_type;
    typedef typename V::value_type real_type;

    explicit IIRg(unsigned n);
    template <class Rc> void coefs(const Rc *bcf, const Rc *acf);
    void reset();
    real_type tick(real_type in);

    IIRg(IIRg &&) = default;
    IIRg &operator=(IIRg &&) = default;

private:
    const unsigned n_{};
    unsigned i_ = {};
    std::unique_ptr<real_type[]> x_, y_;
    real_type b0_ = 1, a0_ = 1;
    std::unique_ptr<real_type[], xsimd::aligned_delete> b_, a_;

public:
    real_type impl_scalar(real_type in);
    real_type impl_simd(real_type in);
};

//------------------------------------------------------------------------------
// Multirate FIR filter useful for upsampling
//  Pads every input sample with P-1 zeros, produces P results.
//------------------------------------------------------------------------------
template <unsigned N, unsigned P, class Ri = float> struct PolyFIR {
    static constexpr unsigned M = N / P;
    static_assert(M * P == N, "PolyFIR: N has to be divisible by P");

    PolyFIR();
    template <class Rc> void coefs(const Rc coefs[N]);
    void reset();
    void in(Ri in);
    std::array<Ri, P> out() const;
    std::array<Ri, P> tick(Ri x);

private:
    unsigned i_ = {};
    std::unique_ptr<Ri[]> x_, c_;
};

//------------------------------------------------------------------------------
// Hal Chamberlin's state variable filter with oversampling
//------------------------------------------------------------------------------
template <unsigned O, class R> struct ChamberlinFilter {
    void set_fq(R f, R q);

    void reset();
    void in(R x);
    R outl() const
    {
        return l_;
    };
    R outh() const
    {
        return h_;
    };
    R outb() const
    {
        return b_;
    };
    R outn() const
    {
        return n_;
    };

private:
    R l_{}, h_{}, b_{}, n_{};
    R d1_{}, d2_{};
    R q1_{}, f1_{};
};

}  // namespace dsp

#include "filter/fir.tcc"
#include "filter/firg.tcc"
#include "filter/iir.tcc"
#include "filter/iirg.tcc"
#include "filter/polyfir.tcc"
#include "filter/chamberlin.tcc"
