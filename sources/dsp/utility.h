//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include <cstdint>

namespace dsp {

template <class T> inline constexpr T square(T a)
{
    return a * a;
}
template <class T> inline constexpr T cube(T a)
{
    return a * a * a;
}

inline const uint32_t nextpow2(uint32_t n)
{
    uint32_t v = n - 1;
    for (unsigned i = 0; i <= 4; ++i)
        v |= v >> (1u << i);
    return v + 1;
}

}  // namespace dsp
