//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "design-fir.h"
#include "utility.h"
#include <memory>
#include <numeric>
#include <cmath>
#include <cassert>

namespace dsp {

namespace fir_detail {

    template <class R> void make_unity_gain(R *b, unsigned n)
    {
        R g = 1 / std::accumulate(b, b + n, R(0));
        for (unsigned i = 0; i < n; ++i)
            b[i] *= g;
    }

    template <class R, class Rr>
    void lowpass_windowed_sinc(Rr *coef, unsigned ncoef, R cutoff, WindowKind wink, R winp)
    {
        R step = cutoff * (R)(2 * M_PI);
        R start = -step * (ncoef - 1) / 2;
        for (unsigned i = 0; i < ncoef; ++i) {
            R phase = start + step * i;
            R sinc = (phase == 0) ? 1 : (std::sin(phase) / phase);
            R win = window(wink, i, ncoef, winp);
            coef[i] = sinc * win;
        }
    }

    template <class R> void spectral_inversion(R *b, unsigned n)
    {
        assert(n & 1);
        for (unsigned i = 0; i < n; ++i)
            b[i] = -b[i];
        b[n / 2] += 1;
    }

    template <class R> void spectral_reversal(R *b, unsigned n)
    {
        assert(n & 1);
        for (unsigned i = 0; i < n; i += 2)
            b[i] = -b[i];
    }

}  // namespace fir_detail

template <class R, class Rr>
Rr *fir_windowed_sinc(Rr *coef, unsigned ncoef, FIRKind kind, R w0, R w1,
                      WindowKind wink, R winp)
{
    assert(ncoef > 1);

    std::unique_ptr<Rr[]> coefalloc;
    if (!coef) {
        coefalloc.reset(new Rr[ncoef]);
        coef = coefalloc.get();
    }

    if (kind != FIR_Lowpass && ncoef % 2 == 0) {
        // can only design odd length filters
        coef[--ncoef] = 0;
    }

    switch (kind) {
    case FIR_Lowpass:
    case FIR_Highpass:
        fir_detail::lowpass_windowed_sinc(coef, ncoef, w0, wink, winp);
        fir_detail::make_unity_gain(coef, ncoef);
        if (kind == FIR_Highpass)
            // convert low-pass to high-pass
            fir_detail::spectral_inversion(coef, ncoef);
        break;
    case FIR_Bandpass:
    case FIR_Bandstop: {
        std::unique_ptr<R[]> lphp(new R[ncoef * 2]);
        R *lp = lphp.get(), *hp = lp + ncoef;
        // design low-pass
        fir_detail::lowpass_windowed_sinc(lp, ncoef, w0, wink, winp);
        fir_detail::make_unity_gain(lp, ncoef);
        // design high-pass
        fir_detail::lowpass_windowed_sinc(hp, ncoef, w1, wink, winp);
        fir_detail::make_unity_gain(hp, ncoef);
        fir_detail::spectral_inversion(hp, ncoef);
        // combine into band-stop
        for (unsigned i = 0; i < ncoef; ++i)
            coef[i] = lp[i] + hp[i];
        if (kind == FIR_Bandpass)
            // convert band-stop to band-pass
            fir_detail::spectral_inversion(coef, ncoef);
        break;
    }
    default:
        assert(false);
    }

    coefalloc.release();
    return coef;
}

}  // namespace dsp
