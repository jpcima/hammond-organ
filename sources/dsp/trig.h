//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once

namespace dsp {

template <class R> class FastSine {
public:
    FastSine(R phase, R step);

    R sin() const
    {
        return s_;
    }
    R cos() const
    {
        return c_;
    }
    void advance();

private:
    const R a_, b_;
    R s_, c_;
};

}  // namespace dsp

#include "trig.tcc"
