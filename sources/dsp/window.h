//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once

namespace dsp {

enum WindowKind {
    Window_Tukey,
    Window_Kaiser,
    Window_Hann,
    Window_Blackman,
    //
    WindowCount,
};

template <class R> R window(WindowKind kind, unsigned i, unsigned n, R a);
template <class R> R (*window_function(WindowKind kind))(unsigned, unsigned, R);

template <class R> R window_tukey(unsigned i, unsigned n, R a);
template <class R> R window_kaiser(unsigned i, unsigned n, R a);
template <class R> R window_hann(unsigned i, unsigned n, R);
template <class R> R window_blackman(unsigned i, unsigned n, R);

WindowKind window_of_name(const char *name);
const char *name_of_window(WindowKind kind);

}  // namespace dsp

#include "window.tcc"
