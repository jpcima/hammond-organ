//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include <memory>
#include <complex>

namespace dsp {

template <class R, bool Scaled = true> class FFT {
public:
    explicit FFT(unsigned n);

    std::complex<R> *forward(R *in);
    R *backward(std::complex<R> *in);

private:
    FFT(const FFT &) = delete;
    FFT &operator=(const FFT &) = delete;

    const unsigned n_;
    std::unique_ptr<R[]> wa_ch_;
    int ifac_[15];
};

}  // namespace dsp

#include "fft.tcc"
