//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "fft.h"
#include "fft/fftpack.h"

namespace dsp {

template <class R, bool Scaled>
FFT<R, Scaled>::FFT(unsigned n)
    : n_(n)
{
    wa_ch_.reset(new R[n * 2]);
    fftpack::rffti1<R>(n, wa_ch_.get(), ifac_);
}

template <class R, bool Scaled> std::complex<R> *FFT<R, Scaled>::forward(R *in)
{
    std::complex<R> *out = reinterpret_cast<std::complex<R> *>(in);
    const unsigned n = n_;
    const R *wa = &wa_ch_[0];
    R *ch = &wa_ch_[n];
    fftpack::rfftf1<R>(n, in, ch, wa, ifac_);
    if (Scaled)
        for (unsigned i = 0; i < n / 2; ++i)
            out[i] /= n;
    return out;
}

template <class R, bool Scaled> R *FFT<R, Scaled>::backward(std::complex<R> *in)
{
    R *out = reinterpret_cast<R *>(in);
    const unsigned n = n_;
    const R *wa = &wa_ch_[0];
    R *ch = &wa_ch_[n];
    fftpack::rfftb1<R>(n, out, ch, wa, ifac_);
    return out;
}

}  // namespace dsp
