//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once

namespace dsp {

enum class WaveId {
    Square,
    Triangle,
    Saw,
};

[[gnu::unused]] static const char *wave_id_name[] = {
    "Square",
    "Triangle",
    "Saw",
};

[[gnu::unused]] static constexpr unsigned wave_id_count =
    sizeof(wave_id_name) / sizeof(wave_id_name[0]);

template <class R> R wave_harmonic_ratio(WaveId wave, unsigned i);
template <class R> R wave_harmonic_gain(WaveId wave, unsigned i);
template <class R> R wave_output_gain(WaveId wave);

template <class R>
unsigned generate_wavetable(WaveId wave, unsigned harmonic_count, R hratio_max,
                            R *table, unsigned table_size, unsigned fft_size);

}  // namespace dsp

#include "wave.tcc"
