//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "wave.h"
#include "fft.h"
#include "utility.h"
#include <memory>
#include <complex>
#include <cmath>
#include <cassert>

namespace dsp {

template <class R> R wave_harmonic_ratio(WaveId wave, unsigned i)
{
    unsigned k = i + 1;
    switch (wave) {
    case WaveId::Square:
        return 2 * k - 1;
    case WaveId::Triangle:
        return 2 * i + 1;
    case WaveId::Saw:
        return k;
    }
    assert(false);
}

template <class R> R wave_harmonic_gain(WaveId wave, unsigned i)
{
    unsigned k = i + 1;
    switch (wave) {
    case WaveId::Square:
        return R(1) / (2 * k - 1);
    case WaveId::Triangle:
        return ((i & 1) ? R(-1) : R(1)) / square(2 * i + 1);
    case WaveId::Saw:
        return ((k & 1) ? R(-1) : R(1)) / k;
    }
    assert(false);
}

template <class R> R wave_output_gain(WaveId wave)
{
    switch (wave) {
    case WaveId::Square:
        return R(4) / M_PI;
    case WaveId::Triangle:
        return 8 / square((R)M_PI);
    case WaveId::Saw:
        return R(2) / M_PI;
    }
    assert(false);
}

template <class R>
unsigned generate_wavetable(WaveId wave, unsigned harmonic_count, R hratio_max,
                            R *table, unsigned table_size, unsigned fft_size)
{
    assert(fft_size % table_size == 0 &&
           "integer multiple desired for trivial downsampling");

    FFT<R> dft(fft_size);

    std::unique_ptr<std::complex<R>[]> cplx(new std::complex<R>[fft_size / 2]());

    unsigned h;
    for (h = 0; h < harmonic_count; ++h) {
        R amp = wave_harmonic_gain<R>(wave, h);
        R hratio = wave_harmonic_ratio<R>(wave, h);
        R phase = 0;
        unsigned bin_num = hratio;
        if (bin_num == 0)
            continue;
        if (bin_num >= fft_size / 2)
            break;
        if (hratio > hratio_max)
            break;
        cplx[bin_num] = std::polar(amp / 2, phase);
    }

    float *real = dft.backward(cplx.get());

    float outg = wave_output_gain<R>(wave);
    for (unsigned i = 0; i < table_size; ++i)
        table[i] = outg * real[i * fft_size / table_size];

    return h;
}

}  // namespace dsp
