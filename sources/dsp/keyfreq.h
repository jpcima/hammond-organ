//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include <type_traits>
#include <cmath>

namespace dsp {

// MIDI key/frequency conversion

template <class R>
typename std::enable_if<std::is_floating_point<R>::value, R>::type key2freq(R n)
{
    return 440 * std::exp2((n - (R)69) * ((R)1 / 12));
}

template <class R>
typename std::enable_if<std::is_floating_point<R>::value, R>::type semitone(R s)
{
    return std::exp2(s * ((R)1 / 12));
}

template <class R>
typename std::enable_if<std::is_floating_point<R>::value, R>::type cent(R c)
{
    R s = c * (R)1e-2;
    return semitone(s);
}

}  // namespace dsp
