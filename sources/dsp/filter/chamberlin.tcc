//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "../filter.h"
#include <cmath>

namespace dsp {

template <unsigned O, class R> inline void ChamberlinFilter<O, R>::reset()
{
    d1_ = 0;
    d2_ = 0;
}

template <unsigned O, class R> void ChamberlinFilter<O, R>::set_fq(R f, R q)
{
    f1_ = 2 * std::sin((R)M_PI * f / O);
    q1_ = 1 / q;
}

template <unsigned O, class R> inline void ChamberlinFilter<O, R>::in(R x)
{
    const R q1 = q1_;
    const R f1 = f1_;
    R d1 = d1_;
    R d2 = d2_;
    R l = d2 + f1 * d1;
    R h = x - l - q1 * d1;
    R b = f1 * h + d1;
    R n = h + l;
    d1 = b;
    d2 = l;
    l_ = l;
    h_ = h;
    b_ = b;
    n_ = n;
    for (unsigned i = 1; i < O; ++i) {
        l = d2 + f1 * d1;
        h = x - l - q1 * d1;
        b = f1 * h + d1;
        n = h + l;
        d1 = b;
        d2 = l;
    }
    d1_ = d1;
    d2_ = d2;
}

}  // namespace dsp
