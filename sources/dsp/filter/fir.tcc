//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "../filter.h"
#include <algorithm>

namespace dsp {

template <unsigned N, class V> FIR<N, V>::FIR()
{
    constexpr unsigned v = V::size;
    constexpr unsigned nn = (N + v - 1) & ~(v - 1);

    xsimd::aligned_allocator<real_type, XSIMD_DEFAULT_ALIGNMENT> alloc;
    c_.reset(alloc.allocate(nn));
    x_.reset(new real_type[2 * nn]());
}

template <unsigned N, class V>
template <class Rc>
void FIR<N, V>::coefs(const Rc *coefs)
{
    std::copy_n(coefs, N, c_.get());
}

template <unsigned N, class V> inline void FIR<N, V>::reset()
{
    constexpr unsigned v = V::size;
    constexpr unsigned nn = (N + v - 1) & ~(v - 1);

    std::fill_n(x_.get(), 2 * nn, 0);
}

template <unsigned N, class V> inline void FIR<N, V>::in(real_type x)
{
    constexpr unsigned v = V::size;
    constexpr unsigned nn = (N + v - 1) & ~(v - 1);

    unsigned i = i_ = (i_ + nn - 1) % nn;
    x_[i] = x_[i + nn] = x;
}

template <unsigned N, class V> inline auto FIR<N, V>::out() const -> real_type
{
    // return impl_scalar();
    return impl_simd();
}

template <unsigned N, class V>
inline auto FIR<N, V>::tick(real_type x) -> real_type
{
    in(x);
    return out();
}

template <unsigned N, class V>
inline auto FIR<N, V>::impl_scalar() const -> real_type
{
    const unsigned i = i_;
    const real_type *__restrict x = x_.get();
    const real_type *__restrict c = c_.get();

    real_type r = 0;
    for (unsigned j = 0; j < N; ++j)
        r += x[i + j] * c[j];
    return r;
}

template <unsigned N, class V>
inline auto FIR<N, V>::impl_simd() const -> real_type
{
    const unsigned i = i_;
    constexpr unsigned v = V::size;
    constexpr unsigned nn = (N + v - 1) & ~(v - 1);
    const real_type *__restrict x = x_.get();
    const real_type *__restrict c = c_.get();

    vector_type rs{};
    for (unsigned j = 0; j < nn; j += v) {
        vector_type cs(&c[j], xsimd::aligned_mode());
        vector_type xs(&x[i + j]);
        rs = xsimd::fma(xs, cs, rs);
    }
    return xsimd::hadd(rs);
}

}  // namespace dsp
