//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "../filter.h"
#include <algorithm>
#include <stdexcept>

namespace dsp {

template <class V>
inline IIRg<V>::IIRg(unsigned n)
    : n_(n)
{
    if (n < 2)
        throw std::runtime_error("invalid number of filter coefficients");

    constexpr unsigned v = V::size;
    const unsigned nn = (n + v - 2) & ~(v - 1);

    xsimd::aligned_allocator<real_type, XSIMD_DEFAULT_ALIGNMENT> alloc;
    b_.reset(alloc.allocate(nn));
    a_.reset(alloc.allocate(nn));
    x_.reset(new real_type[2 * nn]());
    y_.reset(new real_type[2 * nn]());
}

template <class V>
template <class Rc>
void IIRg<V>::coefs(const Rc *bcf, const Rc *acf)
{
    const unsigned n = n_;
    b0_ = bcf[0];
    a0_ = acf[0];
    std::copy_n(bcf + 1, n - 1, b_.get());
    std::copy_n(acf + 1, n - 1, a_.get());
}

template <class V> inline void IIRg<V>::reset()
{
    constexpr unsigned v = V::size;
    const unsigned nn = (n_ + v - 2) & ~(v - 1);

    std::fill_n(x_.get(), 2 * nn, 0);
    std::fill_n(y_.get(), 2 * nn, 0);
}

template <class V> inline auto IIRg<V>::tick(real_type in) -> real_type
{
    // return impl_scalar(in);
    return impl_simd(in);
}

template <class V> inline auto IIRg<V>::impl_scalar(real_type in) -> real_type
{
    constexpr unsigned v = V::size;
    const unsigned n = n_;
    const unsigned nn = (n + v - 2) & ~(v - 1);
    real_type *__restrict x = x_.get();
    real_type *__restrict y = y_.get();
    const real_type *__restrict bcf = b_.get();
    const real_type *__restrict acf = a_.get();

    real_type r = in * b0_;
    unsigned i = (i_ + nn - 1) % nn;
    x[i] = x[i + nn] = in;

    for (unsigned j = 0; j < n - 1; ++j)
        r += x[i + j + 1] * bcf[j] - y[i + j + 1] * acf[j];

    r /= a0_;
    y[i] = y[i + nn] = r;
    i_ = i;
    return r;
}

template <class V> inline auto IIRg<V>::impl_simd(real_type in) -> real_type
{
    constexpr unsigned v = V::size;
    const unsigned n = n_;
    const unsigned nn = (n + v - 2) & ~(v - 1);
    real_type *__restrict x = x_.get();
    real_type *__restrict y = y_.get();
    const real_type *__restrict bcf = b_.get();
    const real_type *__restrict acf = a_.get();

    real_type r = in * b0_;
    unsigned i = (i_ + nn - 1) % nn;
    x[i] = x[i + nn] = in;

    vector_type rs{};
    for (unsigned j = 0; j < nn; j += v) {
        vector_type bs(&bcf[j], xsimd::aligned_mode());
        vector_type xs(&x[i + j + 1]);
        vector_type as(&acf[j], xsimd::aligned_mode());
        vector_type ys(&y[i + j + 1]);
        rs = xsimd::fma(xs, bs, rs);
        rs = xsimd::fnma(ys, as, rs);
    }
    r += xsimd::hadd(rs);

    r /= a0_;
    y[i] = y[i + nn] = r;
    i_ = i;
    return r;
}

}  // namespace dsp
