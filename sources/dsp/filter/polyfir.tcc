//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "../filter.h"
#include <algorithm>

namespace dsp {

template <unsigned N, unsigned P, class Ri> PolyFIR<N, P, Ri>::PolyFIR()
{
    x_.reset(new Ri[2 * M]());
    c_.reset(new Ri[N]());
}

template <unsigned N, unsigned P, class Ri>
template <class Rc>
void PolyFIR<N, P, Ri>::coefs(const Rc coefs[N])
{
    std::copy_n(coefs, N, c_.get());
}

template <unsigned N, unsigned P, class Ri>
inline void PolyFIR<N, P, Ri>::reset()
{
    std::fill_n(x_.get(), 2 * M, 0);
}

template <unsigned N, unsigned P, class Ri>
inline void PolyFIR<N, P, Ri>::in(Ri in)
{
    unsigned i = i_ = (i_ + M - 1) % M;
    x_[i] = x_[i + M] = in;
}

template <unsigned N, unsigned P, class Ri>
inline std::array<Ri, P> PolyFIR<N, P, Ri>::out() const
{
    const unsigned i = i_;
    const Ri *__restrict x = x_.get();
    const Ri *__restrict c = c_.get();

    std::array<Ri, P> s{};
    for (unsigned p = 0; p < P; ++p) {
        for (unsigned j = 0; j < M; ++j)
            s[p] += x[i + j] * c[j * P + p];
    }
    return s;
}

template <unsigned N, unsigned P, class Ri>
inline std::array<Ri, P> PolyFIR<N, P, Ri>::tick(Ri x)
{
    in(x);
    return out();
}

}  // namespace dsp
