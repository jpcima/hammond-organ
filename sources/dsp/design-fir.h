//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "window.h"

namespace dsp {

enum FIRKind {
    FIR_Lowpass,
    FIR_Highpass,
    FIR_Bandpass,
    FIR_Bandstop,
};

template <class R, class Rr>
Rr *fir_windowed_sinc(Rr *coef, unsigned ncoef, FIRKind kind, R w0, R w1,
                      WindowKind wink, R winp);

}  // namespace dsp

#include "design-fir.tcc"
