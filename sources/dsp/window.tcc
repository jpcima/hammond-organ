//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "window.h"
#include "utility.h"
#include <cmath>
#include <cstring>
#include <cassert>

namespace dsp {

template <class R> struct window_traits {
    typedef R(function_type)(unsigned, unsigned, R);

    static constexpr function_type *functions[WindowCount] = {
        &window_tukey<R>,
        &window_kaiser<R>,
        &window_hann<R>,
        &window_blackman<R>,
    };
    static constexpr const char *names[WindowCount] = {
        "tukey",
        "kaiser",
        "hann",
        "blackman",
    };
};

template <class R>
constexpr typename window_traits<R>::function_type *window_traits<R>::functions[WindowCount];

template <class R> constexpr const char *window_traits<R>::names[WindowCount];

template <class R> R window(WindowKind kind, unsigned i, unsigned n, R a)
{
    return window_function<R>(kind)(i, n, a);
}

template <class R> R (*window_function(WindowKind kind))(unsigned, unsigned, R)
{
    assert(unsigned(kind) < WindowCount);
    return window_traits<R>::functions[kind];
}

template <class R> R window_tukey(unsigned i, unsigned n, R a)
{
    const R x = R(i) / (n - 1);
    R w = 1;
    if (x < a / 2)
        w = (1 + std::cos((R)M_PI * (2 * x / a - 1))) / 2;
    else if (x > 1 - a / 2)
        w = (1 + std::cos((R)M_PI * (2 * (x - 1) / a + 1))) / 2;
    return w;
}

namespace window_detail {

    template <class R> static R i0(R x)
    {
        const R ax = std::fabs(x);
        if (ax < R(3.75)) {
            const R y = square(x / R(3.75));
            return 1 + y * (R(3.5156229) +
                            y * (R(3.0899424) +
                                 y * (R(1.2067492) +
                                      y * (R(0.2659732) +
                                           y * (R(0.360768e-1) + y * R(0.45813e-2))))));
        }
        else {
            const R y = R(3.75) / ax;
            return (std::exp(ax) / std::sqrt(ax)) *
                   (R(0.39894228) +
                    y * (R(0.1328592e-1) +
                         y * (R(0.225319e-2) +
                              y * (R(-0.157565e-2) +
                                   y * (R(0.916281e-2) +
                                        y * (R(-0.2057706e-1) +
                                             y * (R(0.2635537e-1) +
                                                  y * (R(-0.1647633e-1) + y * R(0.392377e-2)))))))));
        }
    }

}  // namespace window_detail

template <class R> R window_kaiser(unsigned i, unsigned n, R a)
{
    const R b = (R)M_PI * a;
    const R x = R(i) / (n - 1);
    R arg = b * std::sqrt(1 - square(2 * x - 1));
    return window_detail::i0(arg) / window_detail::i0(b);
}

template <class R> R window_hann(unsigned i, unsigned n, R)
{
    const R r = R(i) / (n - 1);
    return (1 - std::cos(2 * R(M_PI) * r)) / 2;
}

template <class R> R window_blackman(unsigned i, unsigned n, R)
{
    const R a = 0.16;
    const R r = R(i) / (n - 1);
    return (1 - a) / 2 - std::cos(2 * R(M_PI) * r) / 2 +
           (a / 2) * std::cos(4 * R(M_PI) * r);
}

inline WindowKind window_of_name(const char *name)
{
    unsigned i = 0;
    while (i < WindowCount && std::strcmp(name, window_traits<float>::names[i]))
        ++i;
    return WindowKind(i);
}

inline const char *name_of_window(WindowKind kind)
{
    if (unsigned(kind) >= WindowCount)
        return {};
    return window_traits<float>::names[kind];
}

}  // namespace dsp
