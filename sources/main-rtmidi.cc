//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "main.h"
#include "utility/logging.h"
#include <RtMidi.h>
#include <vector>

#undef LOG_TAG
#define LOG_TAG "MIDI"

static RtMidiIn *midi_in = nullptr;

void setup_midi()
{
    void *callback_context = nullptr;

    LOGI("Setting up MIDI input");
    midi_in = new RtMidiIn(RtMidi::Api::UNSPECIFIED, PROJECT_DISPLAY_NAME);
    midi_in->setCallback(
        [](double time, std::vector<unsigned char> *msg, void *userdata) {
            midi_in_callback(time, msg->data(), msg->size());
        },
        callback_context);
    midi_in->openVirtualPort(PROJECT_DISPLAY_NAME " MIDI");
}

void shutdown_midi()
{
    if (midi_in) {
        LOGI("Shutting down MIDI input");
        delete midi_in;
        midi_in = nullptr;
    }
}
