//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "logging.h"

namespace utility {

template <class T> FILE *BasicLogSettings<T>::stream = stderr;
template <class T> LogLevel BasicLogSettings<T>::level = LogLevel::Info;

}  // namespace utility

#define UTILITY__LOGAT(l, c, ...)                 \
    do {                                          \
        if (::utility::LogSettings::level <= (l)) \
            UTILITY__LOG(c, ##__VA_ARGS__);       \
    } while (0);

#define UTILITY__LOG(c, fmt, ...)                                                          \
    do {                                                                                   \
        std::fprintf(::utility::LogSettings::stream, "[%s]", LOG_TAG);                     \
        for (ssize_t len = std::strlen(LOG_TAG), nsp = 20 - len; nsp > 0; --nsp)           \
            std::fputc(' ', ::utility::LogSettings::stream);                               \
        std::fprintf(::utility::LogSettings::stream, "%c: " fmt "\n", (c), ##__VA_ARGS__); \
    } while (0)
