//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once

namespace utility {

void enable_denormals();
void disable_denormals();

struct FPStateRestorer {
    FPStateRestorer();
    ~FPStateRestorer();

private:
    int state_{};
};

}  // namespace utility
