//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include <boost/preprocessor/cat.hpp>
#include <utility>

namespace utility {

#define scope(kw) SCOPE_GUARD__##kw()

template <class F> class scope_guard {
    F f;

public:
    scope_guard(scope_guard<F> &&o)
        : f(std::move(o.f))
    {
    }
    scope_guard(F f)
        : f(std::move(f))
    {
    }
    ~scope_guard()
    {
        f();
    }
    scope_guard(const scope_guard &) = delete;
    scope_guard &operator=(const scope_guard &) = delete;
};

#define SCOPE_GUARD_PP_UNIQUE(x) BOOST_PP_CAT(x, __LINE__)
#define SCOPE_GUARD__exit()                         \
    auto SCOPE_GUARD_PP_UNIQUE(_scope_exit_local) = \
        ::utility::scope_guard_detail::scope_guard_helper() << [&]

namespace scope_guard_detail {

    struct scope_guard_helper {
        template <class F> inline scope_guard<F> operator<<(F &&f)
        {
            return scope_guard<F>(std::forward<F>(f));
        }
    };

}  // namespace scope_guard_detail

}  // namespace utility
