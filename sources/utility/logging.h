//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include <cstdio>
#include <cstring>

namespace utility {

enum class LogLevel { Debug, Info, Warning, Error };

template <class> struct BasicLogSettings {
    static FILE *stream;
    static LogLevel level;
};
typedef BasicLogSettings<void> LogSettings;

}  // namespace utility

#define LOGD(...) \
    UTILITY__LOGAT(::utility::LogLevel::Debug, 'D', ##__VA_ARGS__);
#define LOGI(...) UTILITY__LOGAT(::utility::LogLevel::Info, 'I', ##__VA_ARGS__);
#define LOGW(...) \
    UTILITY__LOGAT(::utility::LogLevel::Warning, 'W', ##__VA_ARGS__);
#define LOGE(...) \
    UTILITY__LOGAT(::utility::LogLevel::Error, 'E', ##__VA_ARGS__);

#include "logging.tcc"
