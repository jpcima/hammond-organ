//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "parts/rotary-speaker.h"
#include "definitions.h"
#include "dsp/trig.h"
#include <complex>

RotarySpeaker::RotarySpeaker()
{
}

void RotarySpeaker::init(double sample_rate, unsigned buffer_size)
{
    sample_interval_ = 1.0 / sample_rate;
#ifdef RS_CHAMBERLIN_FILTER
    filter_.set_fq(filter_cutoff / sample_rate, filterq_);
#else
    hpf_.setup(3, sample_rate, filter_cutoff);
    lpf_.setup(3, sample_rate, filter_cutoff);
    temp_.reset(new float[2 * buffer_size]);
#endif
}

inline void RotarySpeaker::process_one(float mod, float hp, float lp,
                                       float *out_l, float *out_r)
{
    const float himod = mod * hiscale + hibias;
    const float lomod = -mod * loscale + lobias;

    float hioutput = hp;
    for (unsigned st = 0; st < hisdflen; ++st) {
        float x = hioutput;
        float y = himod * x + hix_[st] - himod * hiy_[st];
        hix_[st] = x;
        hiy_[st] = y;
        hioutput = y;
    }

    float looutput = lp;
    for (unsigned st = 0; st < losdflen; ++st) {
        float x = looutput;
        float y = lomod * x + lox_[st] - lomod * loy_[st];
        lox_[st] = x;
        loy_[st] = y;
        looutput = y;
    }

    *out_l = himod * hioutput;
    *out_r = lomod * looutput;
}

void RotarySpeaker::tick(const float *in, float *out, unsigned frame_count)
{
    float modphase = modphase_;
    const float modfreq = modfreq_ * sample_interval_;
    const float phasestep = modfreq * float(2 * M_PIl);

#ifndef RS_CHAMBERLIN_FILTER
    float *hpb = temp_.get();
    float *lpb = hpb + frame_count;
    std::copy(in, in + frame_count, hpb);
    std::copy(in, in + frame_count, lpb);
    hpf_.process(frame_count, &hpb);
    lpf_.process(frame_count, &lpb);
#endif

    dsp::FastSine<float> oscilmod(modphase, phasestep);
    for (unsigned i = 0; i < frame_count; ++i) {
        float mod = oscilmod.sin();
        oscilmod.advance();
        float l, r;

#ifdef RS_CHAMBERLIN_FILTER
        const float x = in[i];
        filter_.in(x);
        float hp = filter_.outh(), lp = filter_.outl();
#else
        float hp = hpb[i], lp = lpb[i];
#endif

        process_one(mod, hp, lp, &l, &r);
        out[i] = l + r;
    }

    modphase += frame_count * phasestep;
    int phasewrap = modphase / float(2 * M_PIl);
    modphase -= phasewrap * float(2 * M_PIl);
    modphase_ = modphase;
}

void RotarySpeaker::tick(const float *in, float *out_l, float *out_r, unsigned frame_count)
{
    float modphase = modphase_;
    const float modfreq = modfreq_ * sample_interval_;
    const float phasestep = modfreq * float(2 * M_PIl);

#ifndef RS_CHAMBERLIN_FILTER
    float *hpb = temp_.get();
    float *lpb = hpb + frame_count;
    std::copy(in, in + frame_count, hpb);
    std::copy(in, in + frame_count, lpb);
    hpf_.process(frame_count, &hpb);
    lpf_.process(frame_count, &lpb);
#endif

    dsp::FastSine<float> oscilmod(modphase, phasestep);
    for (unsigned i = 0; i < frame_count; ++i) {
        float mod = oscilmod.sin();
        oscilmod.advance();
        float l, r;

#ifdef RS_CHAMBERLIN_FILTER
        const float x = in[i];
        filter_.in(x);
        float hp = filter_.outh(), lp = filter_.outl();
#else
        float hp = hpb[i], lp = lpb[i];
#endif

        process_one(mod, hp, lp, &l, &r);
        // TODO test me
        out_l[i] = l + 0.7f * r;
        out_r[i] = r + 0.7f * l;
    }

    modphase += frame_count * phasestep;
    int phasewrap = modphase / float(2 * M_PIl);
    modphase -= phasewrap * float(2 * M_PIl);
    modphase_ = modphase;
}
