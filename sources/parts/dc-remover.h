//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once

class DcRemover {
public:
    float tick(float x);

private:
    float lastin_{};
    float lastout_{};
};

inline float DcRemover::tick(float x)
{
    float out = lastout_ = 0.999f * lastout_ + x - lastin_;
    lastin_ = x;
    return out;
}
