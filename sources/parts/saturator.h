//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "dsp/filter.h"
#include "dsp/utility.h"
#include <memory>
#include <cmath>

class Saturator {
public:
    static constexpr unsigned over_ratio = 8;
    static constexpr unsigned over_hlen = 64;

    static const std::unique_ptr<float[]> over_hcoef;

    Saturator();
    float tick(float x);

private:
    dsp::PolyFIR<over_hlen, over_ratio> upfilter_;
    dsp::FIR<over_hlen> downfilter_;
    float sat1(float x);
    float sat2(float x);
};

inline float Saturator::tick(float x)
{
    upfilter_.in(x);
    std::array<float, over_ratio> outs = upfilter_.out();

    downfilter_.in(sat2(outs[0]));
    float r = downfilter_.out();
    for (unsigned i = 1; i < over_ratio; ++i)
        downfilter_.in(outs[i]);
    return r;
}

inline float Saturator::sat1(float x)
{
    // from DAFX: symmetrical soft clipping
    float origx = x;
    x = std::fabs(x);
    if (x <= 1.0f / 3.0f) {
        x = 2.0f * x;
    }
    else if (x <= 2.0f / 3.0f) {
        x = (3.0f - dsp::square(2.0f - 3.0f * x)) / 3.0f;
    }
    else {
        x = 1.0f;
    }
    return copysign(x, origx);
}

inline float Saturator::sat2(float x)
{
    // from DAFX: exponential distortion
    return copysign(x, 1.0f - std::exp(-std::fabs(x)));
}
