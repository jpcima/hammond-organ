//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "parts/saturator.h"
#include "dsp/design-fir.h"
#include "dsp/window.h"

const std::unique_ptr<float[]>
Saturator::over_hcoef(dsp::fir_windowed_sinc<double>((float *)nullptr, over_hlen,
                                                     dsp::FIR_Lowpass, 0.5 / over_ratio,
                                                     0, dsp::Window_Kaiser, 2.0));

Saturator::Saturator()
{
    upfilter_.coefs(over_hcoef.get());
    downfilter_.coefs(over_hcoef.get());
}
