//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "dsp/utility.h"
#include <algorithm>
#include <memory>
#include <cstdint>
#include <cassert>

class DelayA {
public:
    DelayA(float delay, uint32_t maxdelay);

    static inline constexpr float minimum_delay()
    {
        return 0.5f;
    }
    inline float maximum_delay() const
    {
        return inputlen_ - 1;
    }

    void clear();

    inline float delay() const
    {
        return delay_;
    }
    void delay(float delay);

    float tick(float input);
    float next_out();

private:
    float lastframe_ = 0.0f;
    uint32_t inpoint_;  // range (0, inputlen-1)
    uint32_t outpoint_;  // range (0, inputlen-1)
    float delay_;
    float alpha_;
    float coeff_;
    float apinput_;
    float next_output_;
    bool do_next_out_;
    std::unique_ptr<float[]> inputs_;
    const uint32_t inputlen_;
};

inline DelayA::DelayA(float delay, uint32_t maxdelay)
    : inputlen_(dsp::nextpow2(maxdelay + 1))
{
    inputs_.reset(new float[inputlen_]());
    inpoint_ = 0;
    apinput_ = 0.0f;
    do_next_out_ = true;
    this->delay(delay);
}

inline void DelayA::clear()
{
    std::fill_n(inputs_.get(), inputlen_, 0.0f);
    lastframe_ = 0.0f;
    apinput_ = 0.0f;
}

inline void DelayA::delay(float delay)
{
    assert(delay >= minimum_delay() && delay <= maximum_delay());

    float outpointer = inpoint_ + inputlen_ + 1 - delay;  // outpoint chases inpoint
    outpointer -= int(outpointer / inputlen_) * inputlen_;  // modulo maximum length

    delay_ = delay;

    outpoint_ = outpointer;  // integer part
    outpoint_ &= inputlen_ - 1;
    alpha_ = 1 + outpoint_ - outpointer;  // fractional part

    if (alpha_ < 0.5f) {
        // The optimal range for alpha is about 0.5 - 1.5 in order to
        // achieve the flattest phase delay response.
        outpoint_ = (outpoint_ + 1) & (inputlen_ - 1);
        alpha_ += 1.0f;
    }

    coeff_ = (1.0 - alpha_) / (1.0 + alpha_);  // coefficient for allpass
}

inline float DelayA::tick(float input)
{
    inputs_[inpoint_] = input;

    // Increment input pointer modulo length.
    inpoint_ = (inpoint_ + 1) & (inputlen_ - 1);

    lastframe_ = next_out();
    do_next_out_ = true;

    // Save the allpass input and increment modulo length.
    apinput_ = inputs_[outpoint_];
    outpoint_ = (outpoint_ + 1) & (inputlen_ - 1);

    return lastframe_;
}

inline float DelayA::next_out()
{
    if (do_next_out_) {
        // Do allpass interpolation delay.
        next_output_ = -coeff_ * lastframe_;
        next_output_ += apinput_ + coeff_ * inputs_[outpoint_];
        do_next_out_ = false;
    }
    return next_output_;
}
