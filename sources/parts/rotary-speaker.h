//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#pragma once
#include "dsp/filter.h"
#include <DspFilters/Butterworth.h>
#include <memory>

#define RS_CHAMBERLIN_FILTER

/* from "Computationally Efficient Hammond Organ Synthesis"
   by Pekonen et al.
*/

// TODO try another rotary speaker from DAFX

class RotarySpeaker {
public:
    RotarySpeaker();
    void init(double sample_rate, unsigned buffer_size);

    void tick(const float *in, float *out, unsigned frame_count);
    void tick(const float *in, float *out_l, float *out_r, unsigned frame_count);

    inline float modulator_frequency() const
    {
        return modfreq_;
    }
    inline void modulator_frequency(float f)
    {
        modfreq_ = f;
    }

    // TODO the stereo version
    //  mix each AM's into the other with gain 0.7

private:
    void process_one(float mod, float hp, float lp, float *out_l, float *out_r);

    float sample_interval_ = 0;

    float modfreq_ = 3.0;
    float modphase_ = 0.0f;

    static constexpr double filter_cutoff = 800.0;

#ifdef RS_CHAMBERLIN_FILTER
    static constexpr float filterq_ = 0.8;
    dsp::ChamberlinFilter<8, float> filter_;
#else
    Dsp::SimpleFilter<Dsp::Butterworth::HighPass<4>, 1> hpf_;
    Dsp::SimpleFilter<Dsp::Butterworth::LowPass<4>, 1> lpf_;
    std::unique_ptr<float[]> temp_;
#endif

    static constexpr float hiscale = 0.2f;
    static constexpr float loscale = 0.04f;
    static constexpr float hibias = -0.75f;
    static constexpr float lobias = -0.92f;
    static constexpr unsigned hisdflen = 4;
    static constexpr unsigned losdflen = 3;

    float hix_[hisdflen]{};
    float hiy_[hisdflen]{};
    float lox_[losdflen]{};
    float loy_[losdflen]{};
};
