//          Copyright Jean Pierre Cimalando 2018.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "main.h"
#include "definitions.h"
#include "utility/logging.h"
#include "utility/scope_guard.h"
#include <RtAudio.h>
#include <algorithm>
#include <memory>
#include <cmath>

#undef LOG_TAG
#define LOG_TAG "Audio"

namespace {

RtAudio *client{};

}  // namespace

static int process(void *outputbuffer, void *, unsigned nframes, double,
                   RtAudioStreamStatus, void *userdata);
static void errorCallback(RtAudioError::Type type, const std::string &errorText);
///

void setup_audio()
{
    bool success = false;

    scope(exit)
    {
        if (!success)
            shutdown_audio();
    };

    LOGI("Setting up audio output");

    RtAudio *client = new RtAudio(RtAudio::Api::UNSPECIFIED);
    ::client = client;

    LOGI("Using audio system %d", client->getCurrentApi());

    unsigned num_audio_devices = client->getDeviceCount();
    if (num_audio_devices == 0)
        throw std::runtime_error("No audio devices are present for output.");

    unsigned outputDeviceId = client->getDefaultOutputDevice();
    RtAudio::DeviceInfo deviceInfo = client->getDeviceInfo(outputDeviceId);
    unsigned sampleRate = deviceInfo.preferredSampleRate;

    RtAudio::StreamParameters streamParam;
    streamParam.deviceId = outputDeviceId;
    streamParam.nChannels = num_output_channels;

    RtAudio::StreamOptions streamOpts;
    streamOpts.flags = RTAUDIO_NONINTERLEAVED | RTAUDIO_ALSA_USE_DEFAULT;
    streamOpts.streamName = PROJECT_DISPLAY_NAME;

    const double latency = 10e-3;
    unsigned bufferSize = std::ceil(latency * sampleRate);
    LOGI("Desired latency %f", latency);
    LOGI("Desired buffer size %u", bufferSize);

    client->openStream(&streamParam, nullptr, RTAUDIO_FLOAT32, sampleRate,
                       &bufferSize, &process, nullptr, &streamOpts, &errorCallback);

    LOGI("The sample rate is %u", sampleRate);
    LOGI("The buffer size is %u", bufferSize);

    // now buffer size and sample rate are both set
    audio_out_init_callback(sampleRate, bufferSize);

    client->startStream();

    success = true;
}

void shutdown_audio()
{
    LOGI("Shutting down Audio output");
    delete client;
    client = nullptr;
}

int process(void *outputbuffer, void *, unsigned nframes, double,
            RtAudioStreamStatus, void *userdata)
{
    float *outputs[num_output_channels];
    for (unsigned i = 0; i < num_output_channels; ++i)
        outputs[i] = (float *)outputbuffer + i * nframes;

    audio_out_process_callback(outputs, nframes);
    return 0;
}

void errorCallback(RtAudioError::Type type, const std::string &errorText)
{
    LOGE("Audio error: %s", errorText.c_str());
    if (type != RtAudioError::WARNING && type != RtAudioError::DEBUG_WARNING)
        throw RtAudioError(errorText, type);
}
