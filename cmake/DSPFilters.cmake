
set(DSPFilters_SOURCE_DIR
  "${PROJECT_SOURCE_DIR}/thirdparty/DSPFilters")
set(DSPFilters_INCLUDE_DIR
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/include")

set(DSPFilters_SOURCES
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/Bessel.cpp"
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/Biquad.cpp"
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/Butterworth.cpp"
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/Cascade.cpp"
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/ChebyshevI.cpp"
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/ChebyshevII.cpp"
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/Custom.cpp"
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/Design.cpp"
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/Documentation.cpp"
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/Elliptic.cpp"
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/Filter.cpp"
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/Legendre.cpp"
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/Param.cpp"
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/PoleFilter.cpp"
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/RBJ.cpp"
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/RootFinder.cpp"
  "${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/State.cpp")

add_library(DSPFilters STATIC ${DSPFilters_SOURCES})
target_include_directories(DSPFilters
  PUBLIC "${DSPFilters_INCLUDE_DIR}")
