set(CMAKE_SYSTEM_NAME "Linux")
set(CMAKE_SYSTEM_VERSION 1)
set(CMAKE_SYSTEM_PROCESSOR "arm")
set(ARM_SYSTEM_PROCESSOR "6h")

set(CMAKE_SYSROOT "/opt/raspberry")

set(CMAKE_C_COMPILER "/usr/local/x-tools-armv${ARM_SYSTEM_PROCESSOR}/x-tools${ARM_SYSTEM_PROCESSOR}/arm-unknown-linux-gnueabihf/bin/arm-unknown-linux-gnueabihf-gcc")
set(CMAKE_CXX_COMPILER "/usr/local/x-tools-armv${ARM_SYSTEM_PROCESSOR}/x-tools${ARM_SYSTEM_PROCESSOR}/arm-unknown-linux-gnueabihf/bin/arm-unknown-linux-gnueabihf-g++")

set(CMAKE_FIND_ROOT_PATH "/opt/raspberry")

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

set(ENV{PKG_CONFIG_LIBDIR} "/opt/raspberry/usr/lib/pkgconfig")
set(ENV{PKG_CONFIG_SYSROOT_DIR} "/opt/raspberry")
