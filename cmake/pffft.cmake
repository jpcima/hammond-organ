
set(pffft_SOURCE_DIR
  "${PROJECT_SOURCE_DIR}/thirdparty/pffft")

set(pffft_SOURCES
  "${pffft_SOURCE_DIR}/pffft.c")

add_library(pffft STATIC ${pffft_SOURCES})
target_include_directories(pffft
  PUBLIC "${pffft_SOURCE_DIR}")

set(PFFFT_SIMD_AVAILABLE ON)
if(CMAKE_SYSTEM_PROCESSOR MATCHES "^arm")
  try_compile(PFFFT_SIMD_AVAILABLE "${CMAKE_CURRENT_BINARY_DIR}"
    SOURCES "${PROJECT_SOURCE_DIR}/cmake/pffft-feature-arm-neon.c")
  message("PFFFT cannot use NEON instructions on this ARM target")
endif()

if(NOT PFFFT_SIMD_AVAILABLE)
  target_compile_definitions(pffft PRIVATE PFFFT_SIMD_DISABLE=1)
endif()
