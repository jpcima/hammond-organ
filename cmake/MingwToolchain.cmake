set(CMAKE_SYSTEM_NAME "Windows")
set(CMAKE_SYSTEM_VERSION 1)
set(CMAKE_SYSTEM_PROCESSOR "i686")

set(CMAKE_SYSROOT "/usr/i686-w64-mingw32")

set(CMAKE_C_COMPILER "i686-w64-mingw32-gcc")
set(CMAKE_CXX_COMPILER "i686-w64-mingw32-g++")

add_compile_options("-march=core2")

set(CMAKE_FIND_ROOT_PATH "/usr/i686-w64-mingw32")

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

set(ENV{PKG_CONFIG_LIBDIR} "/usr/i686-w64-mingw32/lib/pkgconfig")
set(ENV{PKG_CONFIG_SYSROOT_DIR} "/usr/i686-w64-mingw32")
