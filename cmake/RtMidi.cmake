include(FindThreads)

set(RtMidi_SOURCE_DIR
  "${PROJECT_SOURCE_DIR}/thirdparty/rtmidi-3.0.0")

set(RtMidi_SOURCES
  "${RtMidi_SOURCE_DIR}/RtMidi.cpp")

set(RTMIDI_APIS "")

add_library(RtMidi STATIC "${RtMidi_SOURCE_DIR}/RtMidi.cpp")
target_include_directories(RtMidi PUBLIC "${RtMidi_SOURCE_DIR}")
target_link_libraries(RtMidi PUBLIC ${CMAKE_THREAD_LIBS_INIT})
if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
  target_compile_definitions(RtMidi PUBLIC "__LINUX_ALSA__")
  target_link_libraries(RtMidi PUBLIC "asound")
  list(APPEND RTMIDI_APIS "alsa")
elseif(CMAKE_SYSTEM_NAME STREQUAL "Windows")
  target_compile_definitions(RtMidi PUBLIC "__WINDOWS_MM__")
  target_link_libraries(RtMidi PUBLIC "winmm")
  list(APPEND RTMIDI_APIS "winmm")
elseif(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
  target_compile_definitions(RtMidi PUBLIC "__MACOSX_CORE__")
  find_library(COREMIDI_LIBRARY "CoreMidi")
  target_link_libraries(RtMidi PUBLIC "${COREMIDI_LIBRARY}")
  list(APPEND RTMIDI_APIS "coremidi")
endif()

message(STATUS "API support in RtMidi: ${RTMIDI_APIS}")
