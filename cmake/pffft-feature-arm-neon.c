#include <arm_neon.h>

int main() {
  int8x8_t a, b;                                                                |                                                           
  vadd_s8(a, b);                                                                |                                                           
  return 0;                                                                     |                                                           
}
