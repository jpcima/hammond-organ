# Virtual tonewheel organ synthesizer / Orgue virtuel à roues phoniques

## Dependencies / Dépendances

- `build-essential`
- `pkg-config`
- `libltdl-dev`
- `qtbase5-dev`
- `libboost-dev`
- `libasound2-dev`
- `libjack-jackd2-dev` or `libjack-dev` (for Jack 2 or Jack 1 respectively)
- `libzita-resampler-dev`

## Build / Construction

- `git clone https://bitbucket.org/jpcima/hammond-organ.git`
- `mkdir hammond-organ/build`
- `cd hammond-organ/build`
- `cmake -DCMAKE_BUILD_TYPE=Release ..`
- `make`
